<?php
/**
 * Smarty plugin
 * @package Smarty
 * @subpackage PluginsModifier
 */

/**
 * Smarty replace modifier plugin
 *
 * Type:     modifier<br>
 * Name:     replace<br>
 * Purpose:  simple search/replace
 *
 * @link http://smarty.php.net/manual/en/language.modifier.replace.php replace (Smarty online manual)
 * @author Monte Ohrt <monte at ohrt dot com>
 * @author Uwe Tews
 * @param string $string  input string
 * @param string $search  text to search for
 * @param string $replace replacement text
 * @return string
 */
function smarty_modifier_replace($string, $search, $replace)
{
    $i = 0;
    if (Smarty::$_MBSTRING) {
        require_once(SMARTY_PLUGINS_DIR . 'shared.mb_str_replace.php');
        $ar = explode(",", $search);
        $br = explode(",", $replace);
        $cnt = count($ar);
        for($i=0; $i<$cnt;){
            $string = smarty_mb_str_replace($ar[$i], $br[$i], $string);
            $i++;
        }
        return $string;
    }
    return str_replace($ar[$i], $br[$i], $string);
}
