﻿/* SORTING */
if (jQuery('.fs_blog_module').size() > 0) {
	var $container = jQuery('.fs_blog_module');
} else if (jQuery('#list').size() > 0) {
	var $container = jQuery('#list');
} else {
	var $container = jQuery('.fs_grid_portfolio');
}

jQuery(function () {

    $container.isotope({
        itemSelector: '.element'
    });

    var $optionSets = jQuery('.optionset'),
        $optionLinks = $optionSets.find('a'),
        $showAll = jQuery('.show_all');

    $optionLinks.on('click', function () {
        var $this = jQuery(this);
        // don't proceed if already selected
        if ($this.parent('li').hasClass('selected')) {
            return false;
        }
        var $optionSet = $this.parents('.optionset');
        $optionSet.find('.selected').removeClass('selected');
        $this.parent('li').addClass('selected');
		if ($this.attr('data-option-value') == "*") {
			$container.removeClass('now_filtering');
		} else {
			$container.addClass('now_filtering');
		}

        // make option object dynamically, i.e. { filter: '.my-filter-class' }
        var options = {},
            key = $optionSet.attr('data-option-key'),
            value = $this.attr('data-option-value');
        // parse 'false' as false boolean
        value = value === 'false' ? false : value;
        options[key] = value;
        if (key === 'layoutMode' && typeof changeLayoutMode === 'function') {
            // changes in layout modes need extra logic
            changeLayoutMode($this, options)
        } else {
            // otherwise, apply new options
            $container.isotope(options);
        }
        return false;
    });

    if (jQuery('.fs_blog_module').size() > 0) {
        jQuery('.fs_blog_module').find('img').load(function () {
            $container.isotope('reLayout');
        });
    } else {
        $container.find('img').load(function () {
            $container.isotope('reLayout');
        });
    }
});

jQuery(window).load(function () {
    if (jQuery('.fs_blog_module').size() > 0) {
        jQuery('.fs_blog_module').isotope('reLayout');
        setTimeout("jQuery('.fs_blog_module').isotope('reLayout')", 500);
    } else {
        $container.isotope('reLayout');
        setTimeout("$container.isotope('reLayout')", 500);
    }
    jQuery('.blog_filter a').on('click', function () {
        setTimeout("jQuery('.fs_blog_module').isotope('reLayout')", 800);
    });
});
jQuery(window).resize(function () {
    if (jQuery('.fs_blog_module').size() > 0) {
        jQuery('.fs_blog_module').isotope('reLayout');
    } else {
        $container.isotope('reLayout');
    }
});

jQuery.fn.portfolio_addon = function (addon_options) {
    // Set Variables
    var addon_el = jQuery(this),
        addon_base = this,
        img_count = addon_options.items.length,
        img_per_load = addon_options.load_count,
        $newEls = '',
        loaded_object = '',
        $container = jQuery('.image-grid');

    jQuery('.load_more_works').on('click', function () {
        $newEls = '';
        loaded_object = '';
        var loaded_images = $container.find('.added').size();
        var now_load = '';
        if ((img_count - loaded_images) > img_per_load) {
            now_load = img_per_load;
        } else {
            now_load = img_count - loaded_images;
        }

        if ((loaded_images + now_load) == img_count) {
            jQuery(this).fadeOut();
        }

        var i_start = '';
        if (loaded_images < 1) {
            i_start = 1;
        } else {
            i_start = loaded_images + 1;
        }

        if (now_load > 0) {
            if (addon_options.type == 1) {
                // Portfolio 3-4 Columns
                for (var i = i_start-1; i < i_start+now_load-1; i++) {
                    loaded_object = loaded_object + '<div class="'+ addon_options.items[i].sortcategory +' element portfolio_item added"><div class="portfolio_item_block"><div class="portfolio_item_wrapper"><div class="port_img_block wrapped_img"><a href="' + addon_options.items[i].url + '"><img src="' + addon_options.items[i].src + '" alt=""></a></div><div class="portfolio_columns_info"><div class="portfolio_columns_title"><h5><a href="' + addon_options.items[i].url + '">' + addon_options.items[i].title + '</a></h5><div class="featured_item_content">' + addon_options.items[i].descr + '</div><div class="featured_items_category"><i class="fa fa-paperclip"></i> ' + addon_options.items[i].category + '</div></div></div></div></div></div>';
                }
            }
            if (addon_options.type == 2) {
                // Portfolio Grid (Masonry)
                for (var i = i_start-1; i < i_start+now_load-1; i++) {
                    loaded_object = loaded_object + '<div class="portfolio-listing-item portfolio-grid-item element '+ addon_options.items[i].sortcategory +' added"><div class="portfolio_item_wrapper"><div class="img_block wrapped_img"><a href="' + addon_options.items[i].url + '"><img class="img2preload" src="' + addon_options.items[i].src + '" alt=""><div class="portfolio_item_content"><div class="portfolio_content_wrapper"><h3>' + addon_options.items[i].title + '</h3><h6>' + addon_options.items[i].descr + '</h6><div class="portfolio_item_meta"><div class="meta_comments"><i class="fa fa-comment-o"></i><h6>' + addon_options.items[i].comments + ' comments</h6></div><div class="portfolio_likes gallery_likes_add"><i class="fa fa-heart-o"></i><span class="portfolio_likes_text">' + addon_options.items[i].likes + '</span> <h6>likes</h6></div></div></div></div><div class="portfolio_item_fadder"></div></a></div></div></div>';
                }
            }
            if (addon_options.type == 3) {
                // Portfolio Grid with Title
                for (var i = i_start-1; i < i_start+now_load-1; i++) {
                    loaded_object = loaded_object + '<div class="portfolio-listing-item element '+ addon_options.items[i].sortcategory +' added"><div class="portfolio_item_wrapper"><div class="img_block wrapped_img"><a href="' + addon_options.items[i].url + '"><img class="img2preload" src="' + addon_options.items[i].src + '" alt=""></a></div><div class="portfolio_columns_title portfolio_grid_title"><h5><a href="' + addon_options.items[i].url + '">' + addon_options.items[i].title + '</a></h5><div class="portfolio_likes gallery_likes_add"><i class="fa fa-heart-o"></i><span class="portfolio_likes_text">' + addon_options.items[i].likes + '</span></div></div></div></div>';
                }
            }

            $newEls = jQuery(loaded_object);
            $container.isotope('insert', $newEls, function () {
                $container.isotope('reLayout');
            });
        }

        return false;
    });
}





