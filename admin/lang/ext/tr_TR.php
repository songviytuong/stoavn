<?php
$lang['404description']='Sayfa Bulunamadı';
$lang['CSS']='Css';
$lang['about']='Hakkında';
$lang['accesskey']='Ulaşım Tuşu';
$lang['accountupdated']='Kullanıcı hesabı g&uuml;ncellendi.';
$lang['action']='Eylem';
$lang['actionstatus']='Eylem/Durum';
$lang['active']='Aktif';
$lang['add']='Ekle';
$lang['addbookmark']='Kısayol Ekle';
$lang['addcontent']='Yeni İ&ccedil;erik Ekle';
$lang['addcss']='Stilsayfası ekle';
$lang['addcssassociation']='Stilsayfası İlişkisi ekle';
$lang['added_css']='Stylesheet Eklendi';
$lang['addgroup']='Yeni Grup Ekle';
$lang['addhtmlblob']='Genel İ&ccedil;erik Bloğu Ekle';
$lang['additional_params']='Ek Parametreler';
$lang['additionaleditors']='Ek d&uuml;zenleyiciler';
$lang['addstylesheet']='Stilsayfası ekle';
$lang['addtemplate']='Yeni Şablon Ekle';
$lang['adduser']='Yeni Kullanıcı Ekle';
$lang['addusertag']='Kullanıcı tanımlı etiket ekle';
$lang['admin']='Site Y&ouml;neticisi';
$lang['admin_layout_legend']='Admin lay-out ayarları';
$lang['adminaccess']='Y&ouml;netici i&ccedil;in oturum a&ccedil;maya erişim';
$lang['admincallout']='Y&ouml;netim Kısayolları';
$lang['admindescription']='Site Y&ouml;netim işlevleri.';
$lang['adminhome']='Y&ouml;netim';
$lang['adminindent']='İ&ccedil;erik G&ouml;sterme';
$lang['adminlog']='Y&ouml;netici G&uuml;nl&uuml;ğ&uuml;';
$lang['adminlog_1day']='1 g&uuml;n';
$lang['adminlog_1month']='1 ay';
$lang['adminlog_1week']='1 hafta';
$lang['adminlog_2weeks']='2 hafta';
$lang['adminlog_3months']='3 ay';
$lang['adminlog_6months']='6 ay';
$lang['adminlog_manual']='Manuel silme';
$lang['adminlogcleared']='Y&ouml;netici G&uuml;nl&uuml;ğ&uuml; başarıyla silindi';
$lang['adminlogdescription']='Y&ouml;netimde kimin ne yaptığının ge&ccedil;mişini g&ouml;sterir.';
$lang['adminlogempty']='Y&ouml;netici G&uuml;nl&uuml;ğ&uuml; boş';
$lang['adminpaging']='Sayfa Listesinde sayfada g&ouml;sterilecek i&ccedil;erik &ouml;ğe sayısı';
$lang['adminpaneltitle']='CMS Made Simple Y&ouml;netici Paneli';
$lang['adminprefs']='Kullanıcı Tercihleri';
$lang['adminprefsdescription']='Site y&ouml;netimi i&ccedil;in kişisel tercihlerinizi burada ayarlayabilirsiniz.';
$lang['adminspecialgroup']='Uyarı: Bu grubun &uuml;yeleri otomatik olarak t&uuml;m izinlere sahip olacaktır';
$lang['adminsystemtitle']='CMS Y&ouml;netim Sistemi';
$lang['admintheme']='Y&ouml;netim Teması';
$lang['advanced']='Gelişmiş';
$lang['aliasalreadyused']='Takma ad zaten başka bir sayfada kullanılmış. &quot;Se&ccedil;enekler&quot; sekmesindeki &quot;Sayfa Takma adı&quot;&#039;nı başka bir şeye değiştiriniz.';
$lang['aliasmustbelettersandnumbers']='Takma ad t&uuml;m&uuml;yle harf ve sayılardan oluşmalıdır';
$lang['aliasnotaninteger']='Takma ad tamsayı olamaz';
$lang['all_groups']='T&uuml;m Gruplar';
$lang['allpagesmodified']='T&uuml;m sayfalar değişti!';
$lang['always']='Herzaman';
$lang['apply']='Uygula';
$lang['applydescription']='Değişiklikleri kaydet ve d&uuml;zenlemeye devam et';
$lang['assignmentchanged']='Grup atamaları g&uuml;ncellendi.';
$lang['assignments']='Kullanıcıları ata';
$lang['associationexists']='Bu ilişki zaten var';
$lang['attachstylesheet']='Bu Stilsayfasını iliştir';
$lang['attachstylesheets']='Tasarım şablonlarını bağla';
$lang['attachtemplate']='Bunu Şablona iliştir';
$lang['attachtotemplate']='Stilsayfasını şablona bağla';
$lang['author']='Yazar';
$lang['autoclearcache2']='Belirli g&uuml;nden &ouml;nceki &ouml;n bellek dosyalarını kaldır';
$lang['autoinstallupgrade']='Otomatik olarak kur veya g&uuml;ncelle';
$lang['back']='Men&uuml;ye Geri D&ouml;n';
$lang['backendwysiwygtouse']='Varsayılan wysiwyg edit&ouml;r&uuml; (yeni kullanıcılar i&ccedil;in)';
$lang['backtoplugins']='Eklenti Listesine Geri d&ouml;n';
$lang['basic_attributes']='Temel Ayarlar';
$lang['blobexists']='Genel İ&ccedil;erik Bloğu adı zaten var';
$lang['blobmanagement']='Genel İ&ccedil;erik Blok Y&ouml;netimi';
$lang['blobs']='Genel İ&ccedil;erik Blokları';
$lang['bookmarks']='Kısayollar';
$lang['browser_cache_settings']='Browser &Ouml;nbellek Ayarları';
$lang['bulk_success']='Toplu işlem başarıyla g&uuml;ncellendi.';
$lang['cachable']='Tamponlanabilir';
$lang['cachecleared']='&Ouml;nbellek temizlendi';
$lang['cachenotwritable']='Tampon klas&ouml;r yazılabilir değil. Tampon temizleme &ccedil;alışmayacaktır. L&uuml;tfen tmp/cache tampon klas&ouml;r&uuml;ne t&uuml;m okuma/yazma yetkilerini veriniz (chmod 777)';
$lang['cancel']='Vazge&ccedil;';
$lang['canceldescription']='Değişiklikleri G&ouml;zardı Et';
$lang['cantchmodfiles']='Bazı dosyaların yetkileri değiştirilemedi';
$lang['cantremove']='Kaldırılamıyor';
$lang['cantremovefiles']='Dosya silme sorunu (yetki?)';
$lang['caution']='Uyarı';
$lang['changehistory']='Tarih&ccedil;eyi Değiştir';
$lang['changeowner']='Sahibi Değiştir';
$lang['changepermissions']='Yetkileri Değiştir';
$lang['changepermissionsconfirm']='DİKKAT\n\nBu işlem eklentiyi oluşturan dosyaların web sunucu tarafından yazılabilir olduğundan emin olmayı deneyecektir.\nDevam etmek istediğinizden emin misiniz?';
$lang['checkversion']='Periyodik yeni versiyon kontrol&uuml;n&uuml; a&ccedil;';
$lang['clear']='Temizle';
$lang['clearadminlog']='Y&ouml;netici G&uuml;nl&uuml;ğ&uuml;n&uuml; Temizle';
$lang['clearcache']='&Ouml;nbelleği temizle';
$lang['clearcache_taskname']='Cache Dosyalarını Temizle';
$lang['close']='Kapat';
$lang['cms_install_information']='CMS Kurulum Bilgisi';
$lang['cms_version']='CMS Versiyonu';
$lang['code']='Kod';
$lang['config_information']='Config Bilgisi';
$lang['confirmcancel']='Yaptığınız değişiklikleri g&ouml;zardı etmek istediğinizden emin misiniz? T&uuml;m değişiklikleri g&ouml;zardı etmek i&ccedil;in Tamam&#039;ı tıklayınız. D&uuml;zenlemeye devam etmek i&ccedil;in Vazge&ccedil;&#039;i tıklayınız.';
$lang['confirmdefault']='Site\&#039;nin varsayılan sayfası olarak ayarlamak istediğinizden emin misiniz?';
$lang['confirmdeletedir']='Klas&ouml;r&uuml; ve i&ccedil;eriğini t&uuml;m&uuml;yle silmek istediğinizden emin misiniz?';
$lang['connection_failed']='Bağlantı hatası!';
$lang['content']='İ&ccedil;erik';
$lang['content_autocreate_flaturls']='Automatically created URL&#039;s are flat';
$lang['content_autocreate_urls']='Sayfa URL lerini otomatik oluştur';
$lang['content_editor_legend']='İ&ccedil;erik edit&ouml;r ayarları';
$lang['content_id']='İ&ccedil;erik ID';
$lang['content_imagefield_path']='Resim alanı i&ccedil;in yol';
$lang['content_mandatory_urls']='Sayfa URL leri gerekli';
$lang['content_thumbnailfield_path']='K&uuml;&ccedil;&uuml;k resim i&ccedil;in yol';
$lang['contentadded']='İ&ccedil;erik başarılı olarak veritabanına eklendi.';
$lang['contentdeleted']='İ&ccedil;erik başarılı olarak veritabanından kaldırıldı.';
$lang['contentdescription']='İ&ccedil;eriği burada ekleyip d&uuml;zenliyoruz.';
$lang['contentimage_path']='{content_image} etiketi i&ccedil;in yol';
$lang['contentmanagement']='İ&ccedil;erik Y&ouml;netimi';
$lang['contenttype']='İ&ccedil;erik Tipi';
$lang['contenttype_content']='İ&ccedil;erik';
$lang['contenttype_errorpage']='Hata Sayfası';
$lang['contenttype_pagelink']='İ&ccedil; Sayfa Linki';
$lang['contenttype_redirlink']='Bağlantı Y&ouml;nlendirme';
$lang['contenttype_sectionheader']='B&ouml;l&uuml;m Başlığı';
$lang['contenttype_separator']='Ayra&ccedil;';
$lang['contentupdated']='İ&ccedil;erik başarılı olarak g&uuml;ncellendi.';
$lang['contract']='B&ouml;l&uuml;m&uuml; Daralt';
$lang['contractall']='T&uuml;m B&ouml;l&uuml;mleri Daralt';
$lang['copy']='Kopyala';
$lang['copy_from']='Buradan Kopyala';
$lang['copy_to']='Buraya Kopyala';
$lang['copycontent']='İ&ccedil;eriği Kopyala';
$lang['copystylesheet']='Stilsayfasınu kopyala';
$lang['copytemplate']='Şablonu Kopyala';
$lang['core']='&Ccedil;ekirdek';
$lang['create']='Yarat';
$lang['createnewfolder']='Yeni Klas&ouml;r Yarat';
$lang['cron_120m']='2 Saat';
$lang['cron_12h']='12 Saat';
$lang['cron_15m']='15 Dakika';
$lang['cron_24h']='24 Saat';
$lang['cron_30m']='30 Dakika';
$lang['cron_3h']='3 Saat';
$lang['cron_60m']='1 Saat';
$lang['cron_6h']='6 Saat';
$lang['cron_request']='Herbir istek';
$lang['cssalreadyused']='CSS adı zaten kullanılmış';
$lang['cssmanagement']='CSS Y&ouml;netimi';
$lang['currentassociations']='Y&uuml;r&uuml;rl&uuml;kteki İlişkiler';
$lang['currentdirectory']='Y&uuml;r&uuml;rl&uuml;kteki Klas&ouml;r';
$lang['currentgroups']='Y&uuml;r&uuml;rl&uuml;kteki Gruplar';
$lang['currentpages']='Y&uuml;r&uuml;rl&uuml;kteki Sayfalar';
$lang['currenttemplates']='Y&uuml;r&uuml;rl&uuml;kteki Şablonlar';
$lang['currentusers']='Y&uuml;r&uuml;rl&uuml;kteki Kullanıcılar';
$lang['custom404']='Uyarlanmış 404 Hata Mesajı';
$lang['dashboard']='&Ouml;zet G&ouml;r&uuml;nt&uuml;le';
$lang['database']='Veritabanı';
$lang['databaseprefix']='Veritabanı &Ouml;neki';
$lang['databasetype']='Veritabanı Tipi';
$lang['date']='Tarih';
$lang['date_format_string']='Tarih Bi&ccedil;imi Dizilişi';
$lang['date_format_string_help']='<em>strftime</em> formatlanmış tarih format ifadesi';
$lang['day']='g&uuml;n';
$lang['days']='g&uuml;nler';
$lang['default']='Varsayılan';
$lang['default_contenttype']='Varsayılan İ&ccedil;erik Tipi';
$lang['defaultpagecontent']='&Ouml;ntanımlı Sayfa İ&ccedil;eriği';
$lang['defaultparentpage']='&Ouml;ntanımlı &Uuml;st Sayfa';
$lang['delete']='Sil';
$lang['deleteassociationconfirm']='- %s - e ilişkiyi silmek istediğine emin misin?';
$lang['deleteconfirm']='Silmek istediğinizden emin misiniz?';
$lang['deletecontent']='İ&ccedil;eriği Sil';
$lang['deletecss']='CSS&#039;yi sil';
$lang['deletepages']='Bu sayfalar silinsin mi?';
$lang['deletetemplate']='Tasarım şablonlarını sil';
$lang['deletetemplates']='Şablonları Sil';
$lang['dependencies']='Bağımlılıklar';
$lang['depsformodule']='%s Eklentisi i&ccedil;in bağımlılıklar';
$lang['description']='A&ccedil;ıklama';
$lang['destination_page']='Gidilecek Sayfa';
$lang['destinationnotfound']='Se&ccedil;ilen sayfa bulunamadı veya ge&ccedil;erli değil';
$lang['directoryabove']='&Uuml;st seviyedeki klas&ouml;r';
$lang['directoryexists']='Bu klas&ouml;r zaten var.';
$lang['disable_wysiwyg']='Bu sayfada WYSIWYG editor&uuml; kapat (regardless of template or user settings)';
$lang['disablesafemodewarning']='Y&ouml;netici g&uuml;venli modu uyarısını kapat';
$lang['disallowed_contenttypes']='İ&ccedil;erik Tipine izin verilmiyor';
$lang['documentation']='D&ouml;k&uuml;mantasyon';
$lang['down']='Aşağı';
$lang['download']='İndir';
$lang['ecommerce']='E-Ticaret';
$lang['ecommerce_desc']='E-Ticaret desteği olan mod&uuml;ller';
$lang['edit']='D&uuml;zenle';
$lang['editbookmark']='Kısayolu D&uuml;zenle';
$lang['editconfiguration']='Ayarları D&uuml;zenle';
$lang['editcontent']='İ&ccedil;erik D&uuml;zenle';
$lang['editcontent_settings']='İ&ccedil;erik D&uuml;zenleme Ayarları';
$lang['editcss']='Stilsayfasını d&uuml;zenle';
$lang['editcsssuccess']='Stilsayfası g&uuml;ncellendi';
$lang['editeventhandler']='Olay İşleyicisini D&uuml;zenle';
$lang['editgroup']='Grup D&uuml;zenle';
$lang['edithtmlblob']='Genel İ&ccedil;erik Bloğunu D&uuml;zenle';
$lang['edithtmlblobsuccess']='Genel i&ccedil;erik bloğu g&uuml;ncellendi';
$lang['editpage']='Sayfa D&uuml;zenle';
$lang['editstylesheet']='Stilsayfası d&uuml;zenle';
$lang['edittemplate']='Şablon D&uuml;zenle';
$lang['edittemplatesuccess']='Şablon g&uuml;ncellendi';
$lang['edituser']='Kullanıcı D&uuml;zenle';
$lang['editusertag']='Kullanıcı Tanımlı Etiket D&uuml;zenle';
$lang['email']='E-Posta Adresi';
$lang['enablecustom404']='Uyarlanmış 404 Mesajını etkinleştir';
$lang['enablenotifications']='Admin b&ouml;l&uuml;m&uuml;nde kullanıcı uyarılarını aktifleştir';
$lang['enablesitedown']='Site &Ccedil;alışmıyor Mesajını Etkinleştir';
$lang['enablewysiwyg']='Site &Ccedil;alışmıyor Mesajında WYSIWYG edit&ouml;r&uuml; a&ccedil;';
$lang['encoding']='Kodlama';
$lang['error']='Hata';
$lang['error_delete_default_parent']='Varsayılan sayfanın bağlı olduğu sayfayı silemezsiniz.';
$lang['error_no_default_content_block']='No default content block was detected in this template.  Please ensure that you have a {content} tag in the page template.';
$lang['error_nofileuploaded']='Y&uuml;klenen Dosya Yok';
$lang['error_nograntall_found']='Could not find a suitable &quot;GRANT ALL&quot; permission.  This may mean you could have problems installing or removing modules.  Or even adding and deleting items, including pages';
$lang['error_type']='Hata Tipi';
$lang['error_udt_name_whitespace']='Hata: Kullanıcı Tanımlı Etiketler isimlerinde boşluk bulunduramazlar.';
$lang['errorattempteddowngrade']='Bu eklentinin kurulması s&uuml;r&uuml;m d&uuml;ş&uuml;rmeye yol a&ccedil;abilirdi. İşlem durduruldu';
$lang['errorcantcreatefile']='Dosya yaratılamadı (yetki sorunu mu?)';
$lang['errorchildcontent']='İ&ccedil;eriğe bağlı i&ccedil;erikler var. L&uuml;tfen &ouml;nce onları kaldırınız.';
$lang['errorcopyingstylesheet']='Stilsayfası kopyalama hatası';
$lang['errorcopyingtemplate']='Şablon Kopyalama Hatası';
$lang['errorcouldnotparsexml']='XML dosyası &ccedil;&ouml;z&uuml;mleme hatası. L&uuml;tfen bir  .tar.gz veya zip dosyası değil .xml dosyası y&uuml;klediğinizden emin olunuz.';
$lang['errorcreatingassociation']='İlişki yaratma hatası';
$lang['errorcssinuse']='Bu stilsayfası hala şablon veya sayfalar tarafından kullanılmakta. L&uuml;tfen &ouml;ncelikle bu  ilişkileri kaldırınız.';
$lang['errordefaultpage']='Y&uuml;r&uuml;rl&uuml;kteki varsayılan sayfa silinemez. &Ouml;nce başka bir sayfayı ayarlayın, l&uuml;tfen.';
$lang['errordeletingassociation']='İlişki silme hatası';
$lang['errordeletingcontent']='İ&ccedil;erik silme hatası (Bu sayfa alt sayfalar barındırıyor veya varsayılan olarak se&ccedil;ilmiş sayfa olabilir.)';
$lang['errordeletingcss']='CSS silme hatası ';
$lang['errordeletingdirectory']='Klas&ouml;r silinemedi. Yetki sorunu mu?';
$lang['errordeletingfile']='Dosya silinemedi. Yetki sorunu mu?';
$lang['errordirectorynotwritable']='Klas&ouml;re yazma yetkisi yok';
$lang['errordtdmismatch']='XML dosyasında DTD S&uuml;r&uuml;m&uuml; yok veya uyumsuz';
$lang['errorgettingcssname']='Stilsayfası adını alma hatası';
$lang['errorgettingtemplatename']='Şablon adı alma hatası';
$lang['errorincompletexml']='XML Dosyası tam değil veya ge&ccedil;ersiz';
$lang['errorinsertingblob']='Genel İ&ccedil;erik Blok ekleme hatası';
$lang['errorinsertingcss']='Stilsayfası ekleme hatası';
$lang['errorinsertinggroup']='Grup ekleme hatası';
$lang['errorinsertingtag']='Kullanıcı etiketi ekleme hatası';
$lang['errorinsertingtemplate']='Şablon ekleme hatası';
$lang['errorinsertinguser']='Kullanıcı ekleme hatası';
$lang['errorinstallfailed']='Eklenti kurulamadı';
$lang['errormodulenotfound']='İ&ccedil; hata, eklentinin başlatılmış hali bulunamadı';
$lang['errormodulenotloaded']='İ&ccedil; hata, eklenti başlatılamadı';
$lang['errormoduleversionincompatible']='Eklenti CMS&#039;nin bu s&uuml;r&uuml;m&uuml; ile uyumsuz';
$lang['errormodulewontload']='Varolan eklentinin başlatılmasında hata';
$lang['errornofilesexported']='Dosyaları xml&#039;e verme hatası';
$lang['errorpagealreadyinuse']='Hata kodu Zaten Kullanımda';
$lang['errorretrievingcss']='Stilsayfası alma hatası';
$lang['errorretrievingtemplate']='Şablon alma hatası';
$lang['errorsendingemail']='Email g&ouml;nderiminde hata oluştu. Y&ouml;netici ile iletişime ge&ccedil;iniz.';
$lang['errortemplateinuse']='Bu şablon bir sayfada kullanılıyor. L&uuml;tfen &ouml;nce sayfayı kaldırınız.';
$lang['errorupdatetemplateallpages']='Şablun şu anda aktif';
$lang['errorupdatingcss']='Stilsayfası g&uuml;ncelleme hatası';
$lang['errorupdatinggroup']='Grup g&uuml;ncelleme hatası';
$lang['errorupdatingpages']='Sayfa g&uuml;ncelleme hatası';
$lang['errorupdatingtemplate']='Şablon g&uuml;ncelleme hatası';
$lang['errorupdatinguser']='Kullanıcı g&uuml;ncelleme hatası';
$lang['errorupdatingusertag']='Kullanıcı etiketi gğncelleme hatası';
$lang['erroruserinuse']='Kullanıcı i&ccedil;erik sayfalarına sahip. Silmeden &ouml;nce sayfaların sahibini değiştiriniz.';
$lang['event']='Olay';
$lang['event_desc_addglobalcontentpost']='Yeni genel i&ccedil;erik bloğu yaratıldıktan sonra g&ouml;nderildi';
$lang['event_desc_addglobalcontentpre']='Yeni bir genel i&ccedil;erik bloğu yaratılmadan &ouml;nce g&ouml;nderildi';
$lang['event_desc_addgrouppost']='Yeni grup yaratıldıktan sonra g&ouml;nderildi';
$lang['event_desc_addgrouppre']='Yeni grup yaratılmadan &ouml;nce g&ouml;nderildi';
$lang['event_desc_addstylesheetpost']='Yeni Stilsayfası yaratıldıktan sonra g&ouml;nderildi';
$lang['event_desc_addstylesheetpre']='Yeni Stilsayfası yaratılmadan &ouml;nce g&ouml;nderildi';
$lang['event_desc_addtemplatepost']='Yeni şablon yaratıldıktan sonra g&ouml;nderildi';
$lang['event_desc_addtemplatepre']='Yeni şablon yaratılmadan &ouml;nce g&ouml;nderildi';
$lang['event_desc_adduserdefinedtagpost']='Kullanıcı tanımlı etiket eklendikten sonra g&ouml;nderildi';
$lang['event_desc_adduserdefinedtagpre']='Kullanıcı tanımlı etiket eklenmeden &ouml;nce g&ouml;nderildi';
$lang['event_desc_adduserpost']='Yeni kullanıcı yaratıldıktan sonra g&ouml;nderildi';
$lang['event_desc_adduserpre']='Yeni kullanıcı yaratılmadan &ouml;nce g&ouml;nderildi';
$lang['event_desc_changegroupassignpost']='Grup atamalarından sonra kaydedilmiş';
$lang['event_desc_changegroupassignpre']='Grup atamalarından &ouml;nce kaydedilmiş';
$lang['event_desc_contentdeletepost']='İ&ccedil;erik sistemden silindikten sonra g&ouml;nderildi';
$lang['event_desc_contentdeletepre']='İ&ccedil;erik sistemden silinmeden &ouml;nce g&ouml;nderildi';
$lang['event_desc_contenteditpost']='İ&ccedil;erikteki değişiklikler kaydedildikten sonra g&ouml;nderildi';
$lang['event_desc_contenteditpre']='İ&ccedil;erikteki değişiklikler kaydedilmeden &ouml;nce g&ouml;nderildi';
$lang['event_desc_contentpostcompile']='İ&ccedil;erik smarty tarafından işlendikten sonra g&ouml;nderildi';
$lang['event_desc_contentpostrender']='Birleştirilmiş html gezgine g&ouml;nderilmeden &ouml;nce g&ouml;nderildi';
$lang['event_desc_contentprecompile']='İ&ccedil;erik smarty&#039;ye işlenmek &uuml;zere g&ouml;nderilmeden &ouml;nce g&ouml;nderildi';
$lang['event_desc_contentstylesheet']='Stilsayfası gezgine g&ouml;nderilmeden &ouml;nce g&ouml;nderildi';
$lang['event_desc_deleteglobalcontentpost']='Genel i&ccedil;erik bloğu sistemden silindikten sonra g&ouml;nderildi';
$lang['event_desc_deleteglobalcontentpre']='Genel i&ccedil;erik bloğu sistemden silinmeden &ouml;nce g&ouml;nderildi';
$lang['event_desc_deletegrouppost']='Grup sistemden silindikten sonra g&ouml;nderildi';
$lang['event_desc_deletegrouppre']='Grup sistemden silinmeden &ouml;nce g&ouml;nderildi';
$lang['event_desc_deletestylesheetpost']='Stilsayfası sistemden silindikten sonra g&ouml;nderildi';
$lang['event_desc_deletestylesheetpre']='Stilsayfası sistemden silinmeden &ouml;nce g&ouml;nderildi';
$lang['event_desc_deletetemplatepost']='Şablon sistemden silindikten sonra g&ouml;nderildi';
$lang['event_desc_deletetemplatepre']='Şablon sistemden silinmeden &ouml;nce g&ouml;nderildi';
$lang['event_desc_deleteuserdefinedtagpost']='Kullanıcı tanımlı etiket silindikten sonra g&ouml;nderildi';
$lang['event_desc_deleteuserdefinedtagpre']='Kullanıcı tanımlı etiket silinmeden &ouml;nce g&ouml;nderildi';
$lang['event_desc_deleteuserpost']='Kullanıcı sistemden silindikten sonra g&ouml;nderildi';
$lang['event_desc_deleteuserpre']='Kullanıcı sistemden silinmeden &ouml;nce g&ouml;nderildi';
$lang['event_desc_editglobalcontentpost']='Genel i&ccedil;erik bloğundaki değişiklikler kaydedildikten sonra g&ouml;nderildi';
$lang['event_desc_editglobalcontentpre']='Genel i&ccedil;erik bloğundaki değişiklikler kaydedilmeden &ouml;nce g&ouml;nderildi';
$lang['event_desc_editgrouppost']='Gruptaki değişiklikler kaydedildikten sonra g&ouml;nderildi';
$lang['event_desc_editgrouppre']='Gruptaki değişiklikler kaydedilmeden &ouml;nce g&ouml;nderildi';
$lang['event_desc_editstylesheetpost']='Stilsayfasındaki değişiklikler kaydedildikten sonra g&ouml;nderildi';
$lang['event_desc_editstylesheetpre']='Stilsayfasındaki değişiklikler kaydedilmeden &ouml;nce g&ouml;nderildi';
$lang['event_desc_edittemplatepost']='Şablondaki değişiklikler kaydedildikten sonra g&ouml;nderildi';
$lang['event_desc_edittemplatepre']='Şablondaki değişiklikler kaydedilmeden &ouml;nce g&ouml;nderildi';
$lang['event_desc_edituserdefinedtagpost']='Kullanıcı tanımlı etiket g&uuml;ncellendikten sonra g&ouml;nderildi';
$lang['event_desc_edituserdefinedtagpre']='Kullanıcı tanımlı etiket g&uuml;ncellenmeden &ouml;nce g&ouml;nderildi';
$lang['event_desc_edituserpost']='Kullanıcıdaki değişiklikler kaydedildikten sonra g&ouml;nderildi';
$lang['event_desc_edituserpre']='Kullanıcıdaki değişiklikler kaydedilmeden &ouml;nce g&ouml;nderildi';
$lang['event_desc_globalcontentpostcompile']='Genel i&ccedil;erik bloğu smarty tarafından işlendikten sonra g&ouml;nderildi';
$lang['event_desc_globalcontentprecompile']='Genel i&ccedil;erik bloğu smarty&#039;ye işlenmek &uuml;zere g&ouml;nderilmeden &ouml;nce g&ouml;nderildi';
$lang['event_desc_loginpost']='Kullanıcı y&ouml;netim paneline girdikten sonra g&ouml;nderildi';
$lang['event_desc_logoutpost']='Kullanıcı y&ouml;netim panelinden &ccedil;ıktıktan sonra g&ouml;nderildi';
$lang['event_desc_moduleinstalled']='Eklenti kurulduktan sonra g&ouml;nderildi';
$lang['event_desc_moduleuninstalled']='Eklenti kaldırıldıktan sonra g&ouml;nderildi';
$lang['event_desc_moduleupgraded']='Eklenti g&uuml;ncellendikten sonra g&ouml;nderildi';
$lang['event_desc_smartypostcompile']='Smarty i&ccedil;in ayrılmış i&ccedil;erik işlendikten sonra g&ouml;nderildi';
$lang['event_desc_smartyprecompile']='Smarty i&ccedil;in ayrılmış i&ccedil;erik işlenmek &uuml;zere g&ouml;nderilmeden &ouml;nce g&ouml;nderildi';
$lang['event_desc_templatepostcompile']='Şablon smarty tarafından işlendikten sonra g&ouml;nderildi';
$lang['event_desc_templateprecompile']='Şablon smarty&#039;ye işlenmek &uuml;zere g&ouml;nderilmeden &ouml;nce g&ouml;nderildi';
$lang['event_description']='Olay A&ccedil;ıklaması';
$lang['event_help_addglobalcontentpost']='<p>Yeni genel i&ccedil;erik bloğu yaratıldıktan sonra g&ouml;nderildi.</p>
<h4>Parametreler</h4>
<ul>
<li>&#039;global_content&#039; - Etkilenen genel i&ccedil;erik &ouml;beği nesnesine ilgi.</li>
</ul>
';
$lang['event_help_addglobalcontentpre']='<p>Yeni bir genel i&ccedil;erik bloğu yaratılmadan &ouml;nce g&ouml;nderildi.</p>
<h4>Parametreler</h4>
<ul>
<li>&#039;global_content&#039; - Etkilenen genel i&ccedil;erik &ouml;beği nesnesine ilgi.</li>
</ul>
';
$lang['event_help_addgrouppost']='<p>Yeni grup yaratıldıktan sonra g&ouml;nderildi.</p>
<h4>Parametreler</h4>
<ul>
<li>&#039;group&#039; - Etkilenen grup nesnesine ilgi.</li>
</ul>
';
$lang['event_help_addgrouppre']='<p>Yeni grup yaratılmadan &ouml;nce g&ouml;nderildi.</p>
<h4>Parametreler</h4>
<ul>
<li>&#039;group&#039; - Etkilenen grup nesnesine ilgi.</li>
</ul>
';
$lang['event_help_addstylesheetpost']='<p>Yeni Stilsayfası yaratıldıktan sonra g&ouml;nderildi.</p>
<h4>Parametreler</h4>
<ul>
<li>&#039;stylesheet&#039; - Etkilenen Stilsayfası nesnesine ilgi.</li>
</ul>
';
$lang['event_help_addstylesheetpre']='<p>Yeni Stilsayfası yaratılmadan &ouml;nce g&ouml;nderildi.</p>
<h4>Parametreler</h4>
<ul>
<li>&#039;stylesheet&#039; - Etkilenen Stilsayfası nesnesine ilgi.</li>
</ul>
';
$lang['event_help_addtemplatepost']='<p>Yeni şablon yaratıldıktan sonra g&ouml;nderildi.</p>
<h4>Parametreler</h4>
<ul>
<li>&#039;template&#039; - Etkilenen şablon nesnesine ilgi.</li>
</ul>
';
$lang['event_help_addtemplatepre']='<p>Yeni şablon yaratılmadan &ouml;nce g&ouml;nderildi.</p>
<h4>Parametreler</h4>
<ul>
<li>&#039;template&#039; - Etkilenen şablon nesnesine ilgi.</li>
</ul>
';
$lang['event_help_adduserpost']='<p>Yeni kullanıcı yaratıldıktan sonra g&ouml;nderildi.</p>
<h4>Parametreler</h4>
<ul>
<li>&#039;user&#039; - Etkilenen kullanıcı nesnesine ilgi.</li>
</ul>
';
$lang['event_help_adduserpre']='<p>Yeni kullanıcı yaratılmadan &ouml;nce g&ouml;nderildi.</p>
<h4>Parametreler</h4>
<ul>
<li>&#039;user&#039; - Etkilenen kullanıcı nesnesine ilgi.</li>
</ul>
';
$lang['event_help_changegroupassignpost']='<p>Sent after group assignments are saved.</p>
<h4>Parameters></h4>
<ul>
<li>&#039;group&#039; - Reference to the affected group object.</li>
<li>&#039;users&#039; - Array of references to user objects now belonging to the affected group.</li>
';
$lang['event_help_changegroupassignpre']='<p>Sent before group assignments are saved.</p>
<h4>Parameters></h4>
<ul>
<li>&#039;group&#039; - Reference to the group object.</li>
<li>&#039;users&#039; - Array of references to user objects belonging to the group.</li>
';
$lang['event_help_contentdeletepost']='<p>İ&ccedil;erik sistemden silindikten sonra g&ouml;nderildi.</p>
<h4>Parametreler</h4>
<ul>
<li>&#039;content&#039; - Etkilenen i&ccedil;erik nesnesine ilgi.</li>
</ul>
';
$lang['event_help_contentdeletepre']='<p>İ&ccedil;erik sistemden silinmeden &ouml;nce g&ouml;nderildi.</p>
<h4>Parametreler</h4>
<ul>
<li>&#039;content&#039; - Etkilenen i&ccedil;erik nesnesine ilgi.</li>
</ul>
';
$lang['event_help_contenteditpost']='<p>İ&ccedil;erikteki değişiklikler kaydedildikten sonra g&ouml;nderildi.</p>
<h4>Parametreler</h4>
<ul>
<li>&#039;content&#039; - Etkilenen i&ccedil;erik nesnesine ilgi.</li>
</ul>
';
$lang['event_help_contenteditpre']='<p>İ&ccedil;erikteki değişiklikler kaydedilmeden &ouml;nce g&ouml;nderildi.</p>
<h4>Parametreler</h4>
<ul>
<li>&#039;global_content&#039; - Etkilenen i&ccedil;erik nesnesine ilgi.</li>
</ul>
';
$lang['event_help_contentpostcompile']='<p>İ&ccedil;erik smarty tarafından işlendikten sonra g&ouml;nderildi.</p>
<h4>Parametreler</h4>
<ul>
<li>&#039;content&#039; - Etkilenen i&ccedil;erik metnine ilgi.</li>
</ul>
';
$lang['event_help_contentpostrender']='<p>Birleştirilmiş html gezgine g&ouml;nderilmeden &ouml;nce g&ouml;nderildi.</p>
<h4>Parametreler</h4>
<ul>
<li>&#039;content&#039; - Html metnine ilgi.</li>
</ul>
';
$lang['event_help_contentprecompile']='<p>İ&ccedil;erik smarty&#039;ye işlenmek &uuml;zere g&ouml;nderilmeden &ouml;nce g&ouml;nderildi.</p>
<h4>Parametreler</h4>
<ul>
<li>&#039;content&#039; - Etkilenen i&ccedil;erik metnine ilgi.</li>
</ul>
';
$lang['event_help_contentstylesheet']='<p>Stilsayfası gezgine g&ouml;nderilmeden &ouml;nce g&ouml;nderildi.</p>
<h4>Parametreler</h4>
<ul>
<li>&#039;content&#039; - Etkilenen Stilsayfası metnine ilgi.</li>
</ul>
';
$lang['event_help_deleteglobalcontentpost']='<p>Genel i&ccedil;erik bloğu sistemden silindikten sonra g&ouml;nderildi.</p>
<h4>Parametreler</h4>
<ul>
<li>&#039;global_content&#039; - Etkilenen genel i&ccedil;erik &ouml;beği nesnesine ilgi.</li>
</ul>
';
$lang['event_help_deleteglobalcontentpre']='<p>Genel i&ccedil;erik bloğu sistemden silinmeden &ouml;nce g&ouml;nderildi.</p>
<h4>Parametreler</h4>
<ul>
<li>&#039;global_content&#039; - Etkilenen genel i&ccedil;erik &ouml;beği nesnesine ilgi.</li>
</ul>
';
$lang['event_help_deletegrouppost']='<p>Grup sistemden silindikten sonra g&ouml;nderildi.</p>
<h4>Parametreler</h4>
<ul>
<li>&#039;group&#039; - Etkilenen grup nesnesine ilgi.</li>
</ul>
';
$lang['event_help_deletegrouppre']='<p>Grup sistemden silinmeden &ouml;nce g&ouml;nderildi.</p>
<h4>Parametreler</h4>
<ul>
<li>&#039;group&#039; - Etkilenen grup nesnesine ilgi.</li>
</ul>
';
$lang['event_help_deletestylesheetpost']='<p>Stilsayfası sistemden silindikten sonra g&ouml;nderildi.</p>
<h4>Parametreler</h4>
<ul>
<li>&#039;stylesheet&#039; - Etkilenen Stilsayfası nesnesine ilgi.</li>
</ul>
';
$lang['event_help_deletestylesheetpre']='<p>Stilsayfası sistemden silinmeden &ouml;nce g&ouml;nderildi.</p>
<h4>Parametreler</h4>
<ul>
<li>&#039;stylesheet&#039; - Etkilenen Stilsayfası nesnesine ilgi.</li>
</ul>
';
$lang['event_help_deletetemplatepost']='<p>Şablon sistemden silindikten sonra g&ouml;nderildi.</p>
<h4>Parametreler</h4>
<ul>
<li>&#039;template&#039; - Etkilenen şablon nesnesine ilgi.</li>
</ul>
';
$lang['event_help_deletetemplatepre']='<p>Şablon sistemden silinmeden &ouml;nce g&ouml;nderildi.</p>
<h4>Parametreler</h4>
<ul>
<li>&#039;template&#039; - Etkilenen şablon nesnesine ilgi.</li>
</ul>
';
$lang['event_help_deleteuserpost']='<p>Kullanıcı sistemden silindikten sonra g&ouml;nderildi.</p>
<h4>Parametreler</h4>
<ul>
<li>&#039;user&#039; - Etkilenen kullanıcı nesnesine ilgi.</li>
</ul>
';
$lang['event_help_deleteuserpre']='<p>Kullanıcı sistemden silinmeden &ouml;nce g&ouml;nderildi.</p>
<h4>Parametreler</h4>
<ul>
<li>&#039;user&#039; - Etkilenen kullanıcı nesnesine ilgi.</li>
</ul>
';
$lang['event_help_editglobalcontentpost']='<p>Genel i&ccedil;erik bloğundeki değişiklikler kaydedildikten sonra g&ouml;nderildi.</p>
<h4>Parametreler</h4>
<ul>
<li>&#039;global_content&#039; - Etkilenen genel i&ccedil;erik &ouml;beği nesnesine ilgi.</li>
</ul>
';
$lang['event_help_editglobalcontentpre']='<p>Genel i&ccedil;erik bloğundeki değişiklikler kaydedilmeden &ouml;nce g&ouml;nderildi.</p>
<h4>Parametreler</h4>
<ul>
<li>&#039;global_content&#039; - Etkilenen genel i&ccedil;erik &ouml;beği nesnesine ilgi.</li>
</ul>
';
$lang['event_help_editgrouppost']='<p>Sent after edits to a group are saved.</p>
<h4>Parametreler</h4>
<ul>
<li>&#039;group&#039; - Etkilenen grup nesnesine ilgi.</li>
</ul>
';
$lang['event_help_editgrouppre']='<p>Gruptaki değişiklikler kaydedilmeden &ouml;nce g&ouml;nderildi.</p>
<h4>Parametreler</h4>
<ul>
<li>&#039;group&#039; - Etkilenen grup nesnesine ilgi.</li>
</ul>
';
$lang['event_help_editstylesheetpost']='<p>Stilsayfasındaki değişiklikler kaydedildikten sonra g&ouml;nderildi.</p>
<h4>Parametreler</h4>
<ul>
<li>&#039;stylesheet&#039; - Etkilenen Stilsayfası nesnesine ilgi.</li>
</ul>
';
$lang['event_help_editstylesheetpre']='<p>Stilsayfasındaki değişiklikler kaydedilmeden &ouml;nce g&ouml;nderildi.</p>
<h4>Parametreler</h4>
<ul>
<li>&#039;stylesheet&#039; - Etkilenen Stilsayfası nesnesine ilgi.</li>
</ul>
';
$lang['event_help_edittemplatepost']='<p>Şablondaki değişiklikler kaydedildikten sonra g&ouml;nderildi.</p>
<h4>Parametreler</h4>
<ul>
<li>&#039;template&#039; - Etkilenen şablon nesnesine ilgi.</li>
</ul>
';
$lang['event_help_edittemplatepre']='<p>Şablondaki değişiklikler kaydedilmeden &ouml;nce g&ouml;nderildi.</p>
<h4>Parametreler</h4>
<ul>
<li>&#039;template&#039; - Etkilenen şablon nesnesine ilgi.</li>
</ul>
';
$lang['event_help_edituserpost']='<p>Kullanıcıdaki değişiklikler kaydedildikten sonra g&ouml;nderildi.</p>
<h4>Parametreler</h4>
<ul>
<li>&#039;user&#039; - Etkilenen kullanıcı nesnesine ilgi.</li>
</ul>
';
$lang['event_help_edituserpre']='<p>Kullanıcıdaki değişiklikler kaydedilmeden &ouml;nce g&ouml;nderildi.</p>
<h4>Parametreler</h4>
<ul>
<li>&#039;user&#039; - Etkilenen kullanıcı nesnesine ilgi.</li>
</ul>
';
$lang['event_help_globalcontentpostcompile']='<p>Genel i&ccedil;erik bloğu smarty tarafından işlendikten sonra g&ouml;nderildi.</p>
<h4>Parametreler</h4>
<ul>
<li>&#039;global_content&#039; - Etkilenen genel i&ccedil;erik &ouml;beği metnine ilgi.</li>
</ul>
';
$lang['event_help_globalcontentprecompile']='<p>Genel i&ccedil;erik bloğu smarty&#039;ye işlenmek &uuml;zere g&ouml;nderilmeden &ouml;nce g&ouml;nderildi.</p>
<h4>Parametreler</h4>
<ul>
<li>&#039;global_content&#039; - Etkilenen genel i&ccedil;erik &ouml;beği metnine ilgi.</li>
</ul>
';
$lang['event_help_loginpost']='<p>Kullanıcı y&ouml;netim paneline girdikten sonra g&ouml;nderildi.</p>
<h4>Parametreler</h4>
<ul>
<li>&#039;user&#039; - Etkilenen kullanıcı nesnesine ilgi.</li>
</ul>
';
$lang['event_help_logoutpost']='<p>Kullanıcı y&ouml;netim panelinden &ccedil;ıktıktan sonra g&ouml;nderildi.</p>
<h4>Parametreler</h4>
<ul>
<li>&#039;user&#039; - Etkilenen kullanıcı nesnesine ilgi.</li>
</ul>
';
$lang['event_help_smartypostcompile']='<p>Smarty i&ccedil;in ayrılmış i&ccedil;erik işlendikten sonra g&ouml;nderildi.</p>
<h4>Parametreler</h4>
<ul>
<li>&#039;content&#039; - Etkilenen metne ilgi.</li>
</ul>
';
$lang['event_help_smartyprecompile']='<p>Smarty i&ccedil;in ayrılmış i&ccedil;erik işlenmek &uuml;zere g&ouml;nderilmeden &ouml;nce g&ouml;nderildi.</p>
<h4>Parametreler</h4>
<ul>
<li>&#039;content&#039; - Etkilenen metne ilgi.</li>
</ul>
';
$lang['event_help_templatepostcompile']='<p>Şablon smarty tarafından işlendikten sonra g&ouml;nderildi.</p>
<h4>Parametreler</h4>
<ul>
<li>&#039;template&#039; - Etkilenen şablon metnine ilgi.</li>
</ul>
';
$lang['event_help_templateprecompile']='<p>Şablon smarty&#039;ye işlenmek &uuml;zere g&ouml;nderilmeden &ouml;nce g&ouml;nderildi.</p>
<h4>Parametreler</h4>
<ul>
<li>&#039;template&#039; - Etkilenen şablon metnine ilgi.</li>
</ul>
';
$lang['event_name']='Olay Adı';
$lang['eventhandlerdescription']='Kullanıcı etiketlerini olaylarla ilişkilendir';
$lang['eventhandlers']='Olaylar';
$lang['execute']='&Ccedil;alıştır';
$lang['expand']='B&ouml;l&uuml;m&uuml; Genişlet';
$lang['expandall']='T&uuml;m B&ouml;l&uuml;mleri Genişlet';
$lang['export']='Ver';
$lang['extensions']='Ekler';
$lang['extensionsdescription']='Eklenti, etiket, ve diğer eğlenceli şeyler.';
$lang['extra1']='Ektra Sayfa &Ouml;zellği 1';
$lang['extra2']='Ektra Sayfa &Ouml;zellği 2';
$lang['extra3']='Ektra Sayfa &Ouml;zellği 3';
$lang['failure']='Hata';
$lang['false']='Yanlış';
$lang['file_uploads']='Dosya Y&uuml;klemeleri';
$lang['file_url']='Dosyaya kısayol (URL yerine)';
$lang['filecreatedirnodoubledot']='Klas&ouml;r &#039;..&#039; i&ccedil;eremez.';
$lang['filecreatedirnoname']='Adı olmayan klas&ouml;r yaratılamaz.';
$lang['filecreatedirnoslash']='Klas&ouml;r &#039;/&#039; veya &#039;\&#039; i&ccedil;eremez.';
$lang['filemanagement']='Dosya Y&ouml;netimi';
$lang['filemanager']='Dosya Y&ouml;neticisi';
$lang['filemanagerdescription']='Dosya y&uuml;kleme ve y&ouml;netme.';
$lang['filename']='Dosya adı';
$lang['filenotuploaded']='Dosya y&uuml;klenemedi. Yetki sorunu mu?';
$lang['files']='Dosyalar';
$lang['files_not_found']='Dosyalar Bulunamadı';
$lang['filesize']='Dosya Boyutu';
$lang['filterapplied']='Şimdiki Filtre';
$lang['filterapply']='Filtreleri uygula';
$lang['filterbymodule']='Eklentiye g&ouml;re s&uuml;z';
$lang['filterreset']='Filtreleri sıfırla';
$lang['filters']='Filtreler';
$lang['filteruser']='Kullanıcı Adı';
$lang['first']='İlk';
$lang['firstname']='Ad';
$lang['forgotpwprompt']='Y&ouml;netici kullanıcı adını giriniz. An email will then be sent to the email address associated with that username with new login information';
$lang['forums']='Forumlar';
$lang['frontendlang']='&Ouml;ny&uuml;z i&ccedil;in varsayılan dil';
$lang['frontendwysiwygtouse']='&Ouml;n u&ccedil; wysiwyg';
$lang['gcb_wysiwyg']='Genel İ&ccedil;erik Bloğu i&ccedil;in (GCB) WYSIWYG&#039;yi etkinleştir';
$lang['gcb_wysiwyg_help']='Genel İ&ccedil;erik Bloğu d&uuml;zenlemesi srasında WYSIWYG d&uuml;zenleyiciyi etkinleştir';
$lang['gd_version']='GD versiyonu';
$lang['general_operation_settings']='Genel Operasyon Ayarları';
$lang['general_settings']='Genel Ayarlar';
$lang['global_umask']='Dosya Yaratma Maskesi (umask)';
$lang['globalconfig']='Genel Ayarlar';
$lang['globalmetadata']='Evrensel Metadata';
$lang['goto']='Geri d&ouml;n:';
$lang['group']='Grup';
$lang['groupassignmentdescription']='Burada kullanıcıları gruplara atayabilirsiniz.';
$lang['groupassignments']='Grup Atamaları';
$lang['groupmanagement']='Grup Y&ouml;netimi';
$lang['groupmembers']='Grup Atamaları';
$lang['groupname']='Grup Adı';
$lang['grouppermissions']='Grup Yetkileri';
$lang['groupperms']='Grup Yetkileri';
$lang['grouppermsdescription']='Grup yetki ve ulaşım seviyelerini ayarla';
$lang['groups']='Gruplar';
$lang['groupsdescription']='Gruplar burada y&ouml;netiliyor.';
$lang['handle_404']='Varsayılan 404 Kullanımı';
$lang['handler']='İşleyici (kullanıcı tanımlı etiket)';
$lang['hasdependents']='Bağlılıkları var';
$lang['headtags']='Ana Etiketler';
$lang['help']='Yardım';
$lang['help_systeminformation']='The information displayed below is collected from a variety of locations, and summarized here so that you may be able to conveniently find some of the information required when trying to diagnose a problem or request help with your CMS Made Simple installation.';
$lang['helpaddtemplate']='<p>Şablon sitenizin i&ccedil;eriğinin nasıl g&ouml;r&uuml;neceğini kontrol eden şeydir.</p><p>&Ouml;ğelerinizin g&ouml;r&uuml;n&uuml;mlerini kontrol etmek i&ccedil;in, burada yerleşimi yaratıp Stilsayfası b&ouml;l&uuml;m&uuml;nde de CSS&#039;lerinizi ekleyiniz.</p>';
$lang['helplisttemplate']='<p>Bu sayfa şablon d&uuml;zenleme, silme ve yaratma işlemlerini sağlar.</p><p>Yeni bir şablon yaratmak i&ccedil;in <u>Yeni Şablon Ekle</u> d&uuml;ğmesini tıklayınız.</p><p>Eğer t&uuml;m i&ccedil;erik sayfalarının aynı şablonu kullanmasını isterseniz, <u>T&uuml;m İ&ccedil;eriğe Ayarla</u> bağını tıklayınız.</p><p>Eğer bir şablonu kopyalamak isterseniz, <u>Kopyala</u> simgesini tıklayınız; yeni kopyalanan şablon i&ccedil;in bir ad girmeniz istenecektir.</p>';
$lang['helpwithsection']='%s Yardım';
$lang['hide_help_links']='Yardım bağlarını gizle';
$lang['hide_help_links_help']='Wiki ve sayfa başlıklarındaki eklenti yardım bağlarını pasifleştirmek i&ccedil;in bu kutucuğu işaretleyiniz.';
$lang['hidefrommenu']='Men&uuml;de G&ouml;sterme';
$lang['home']='Index';
$lang['homepage']='Anasayfa';
$lang['hostname']='Sunucu adı';
$lang['hour']='saat';
$lang['hours']='saatler';
$lang['htmlblobdescription']='Genel İ&ccedil;erik Blokları sayfa veya şablonlarınıza koyabileceğiniz i&ccedil;erik par&ccedil;alarıdır.';
$lang['htmlblobs']='Genel İ&ccedil;erik Blokları';
$lang['idnotvalid']='Girilen kimlik ge&ccedil;erli değil';
$lang['ignorenotificationsfrommodules']='Bu mod&uuml;ller ile ilgili uyarıları yoksay';
$lang['illegalcharacters']='%s. alanında ge&ccedil;ersiz karakterler';
$lang['image']='Resim';
$lang['imagemanagement']='Resim Y&ouml;neticisi';
$lang['imagemanager']='Resim Y&ouml;netimi';
$lang['imagemanagerdescription']='Resimleri Y&uuml;kle/d&uuml;zenle ve kaldır.';
$lang['images']='Resim Y&ouml;neticisi';
$lang['inactive']='Pasif';
$lang['indent']='Hiyerarşiyi vurgulamak i&ccedil;in Sayfa listesini i&ccedil;e kaydır';
$lang['info_autoalias']='Eğer bu alan boş brakılmışsa, takma isim otomatik olarak yaratılacaktır.';
$lang['info_edeprecated_failed']='If E_DEPRECATED is enabled in your error reporting users will see alot of warning messages that could effect the display and functionalty';
$lang['info_edituser_password']='Kullanıcının parolasını değiştirmek i&ccedil;in bu alanı değiştiriniz';
$lang['info_edituser_passwordagain']='Kullanıcının parolasını değiştirmek i&ccedil;in bu alanı değiştiriniz';
$lang['info_pagealias']='Bu sayfa i&ccedil;in benzersiz bir sayfa takma adı belirleyiniz.';
$lang['info_sitedownexcludes']='This parameter allows listing a comma separated list of ip addresses or networks that should not be subject to the sitedown mechanism.  This allows administrators to work on a site whilst anonymous visitors receive a sitedown message.<br/><br/>Addresses can be specified in the following formats:<br/>
1. xxx.xxx.xxx.xxx -- (exact IP address)<br/>
2. xxx.xxx.xxx.[yyy-zzz] -- (IP address range)<br/>
3. xxx.xxx.xxx.xxx/nn -- (nnn = number of bits, cisco style.  i.e:  192.168.0.100/24 = entire 192.168.0 class C subnet)';
$lang['info_validation']='This function will compare the checksums found in the uploaded file with the files on the current installation.  It can assist in finding problems with uploads, or exactly what files were modified if your system has been hacked.  A checksum file is generated for each release of CMS Made simple from version 1.4 on.';
$lang['informationmissing']='Bilgi eksik';
$lang['insecure']='Normal (HTTP)';
$lang['install']='Kur';
$lang['installdirwarning']='<em><strong>Uyarı:</strong></em> kur klas&ouml;r&uuml; hala duruyor. L&uuml;tfen t&uuml;m&uuml;yle siliniz.';
$lang['installed']='Kuruldu';
$lang['installed_modules']='Y&uuml;kl&uuml; Mod&uuml;ller';
$lang['invalid']='Ge&ccedil;ersiz';
$lang['invalidcode']='Ge&ccedil;ersiz kod girildi.';
$lang['invalidcode_brace_missing']='Parantez sayıları dengesiz';
$lang['invalidemail']='Email adresi yanlış girildi';
$lang['invalidtemplate']='Şablon ge&ccedil;ersiz';
$lang['ip_addr']='IP Adresi';
$lang['irc']='IRC.';
$lang['itemid']='&Ouml;ğe Kimliği';
$lang['itemname']='&Ouml;ğe Adı';
$lang['itsbeensincelogin']='It has been %s since you last login';
$lang['jsdisabled']='&Uuml;zg&uuml;n&uuml;m, bu işlev Javascript&#039;in etkinleştirilmiş olmasını gerektiriyor.';
$lang['lang_settings_legend']='Dil Ayarları';
$lang['langparam']='Parametre &ouml;ny&uuml;zde hangi dilin kullanılacağını belirler. Her eklenti bunu desteklemez veya kullanmaz.';
$lang['language']='Dil';
$lang['last']='Son';
$lang['last_modified_at']='En son yenilenme zamanı';
$lang['last_modified_by']='En son yenileyen';
$lang['lastname']='Soyadı';
$lang['layout']='Yerleşim';
$lang['layoutdescription']='Site tasarım se&ccedil;enekleri.';
$lang['lctitle_active']='İ&ccedil;erik &ouml;ğesi aktif olup olmadığını g&ouml;sterir. Inaktif &ouml;ğeler g&ouml;r&uuml;nt&uuml;lenemiyor.';
$lang['lctitle_move']='İ&ccedil;erik hiyerarşi d&uuml;zenlemesine izin ver';
$lang['lctitle_multiselect']='Hepsini Se&ccedil;/Se&ccedil;me';
$lang['listcontent_settings']='İ&ccedil;erik Listesi Ayarları';
$lang['listcontent_showalias']='&quot;Alias&quot; kolonunu g&ouml;ster';
$lang['listcontent_showtitle']='Sayfa Başlığını yada Menu Yazısını g&ouml;ster';
$lang['listcontent_showurl']='&quot;URL&quot; kolonunu g&ouml;ster';
$lang['listgcbs_pagelimit']='Genel İ&ccedil;erik Blokları g&ouml;steriminde satır sayısı';
$lang['liststylesheets']='Tasarım Şablonları';
$lang['liststylesheets_pagelimit']='Stil şablonları g&ouml;steriminde satır sayısı';
$lang['listtemplates_pagelimit']='Şablonları g&ouml;steriminde satır sayısı';
$lang['login_info']='Y&ouml;netim b&ouml;l&uuml;m&uuml;n&uuml;n d&uuml;zg&uuml;n &ccedil;alışabilmesi i&ccedil;in';
$lang['login_info_params']='<ol> 
  <li>Tarayıcınız &Ccedil;erezler izin vermeli</li> 
  <li>Tarayıcınızda Javascript etkin olmalı</li> 
  <li>Popup pencerelere aşağıdaki adres i&ccedil;in izin verilmiş olmalıdır:</li> 
</ol>';
$lang['login_info_title']='Bilgi';
$lang['loginprompt']='Y&ouml;netim Paneline girmek i&ccedil;in ge&ccedil;erli bir kullanıcı parolası giriniz.';
$lang['logintitle']='CMS Made Simple Y&ouml;netici Girişi';
$lang['logout']='&Ccedil;ık';
$lang['lostpw']='Şifrenizi mi Unuttunuz?';
$lang['lostpwemail']='You are recieving this e-mail because a request has been made to change the (%s) password associated with this user account (%s).  If you would like to reset the password for this account simply click on the link below or paste it into the url field on your favorite browser:
%s

If you feel this is incorrect or made in error, simply ignore the email and nothing will change.';
$lang['lostpwemailsubject']='[%s] Şifre Geri Alma';
$lang['main']='Ana';
$lang['mainmenu']='Ana Men&uuml;';
$lang['managebookmarks']='Kısayolları Y&ouml;net';
$lang['managebookmarksdescription']='Y&ouml;netim kısayollarınızı burada d&uuml;zenleyebilirsiniz.';
$lang['master_admintheme']='Varsayılan Y&ouml;netim Şablonu (for the login page and new user accounts)';
$lang['maximumversion']='En y&uuml;ksek s&uuml;r&uuml;m';
$lang['maximumversionsupported']='Desteklenen en y&uuml;ksek CMSMS S&uuml;r&uuml;m&uuml;';
$lang['md5_function']='md5 fonksiyonu';
$lang['mediatype']='Ortam Tipi';
$lang['mediatype_']='Hi&ccedil;biri ayarlanmamış : heryeri etkileyecektir';
$lang['mediatype_all']='all : T&uuml;m aygıtlar i&ccedil;in uygundur.';
$lang['mediatype_aural']='işitsel : konuşma analiz makineleri i&ccedil;in';
$lang['mediatype_braille']='kabartma : kabartma yazı geribildirim cihazları i&ccedil;in';
$lang['mediatype_embossed']='kabartılmış : kabartma yazı yazıcılarına i&ccedil;in';
$lang['mediatype_handheld']='avu&ccedil;i&ccedil;i : Avu&ccedil;i&ccedil;i cihazlar i&ccedil;in';
$lang['mediatype_print']='&ccedil;ıktı : Baskı &ouml;nizleme modu (sayfa ya da opak malzeme ve d&ouml;k&uuml;man) i&ccedil;in';
$lang['mediatype_projection']='projeksiyon : Yansıtılan sunumlar i&ccedil;in, &ouml;rneğin projekt&ouml;r ya da asetat';
$lang['mediatype_screen']='ekran : &Ouml;ncelikle renkli bilgisayar ekranları i&ccedil;in';
$lang['mediatype_tty']='tty : İşitme engelliler i&ccedil;in &ouml;zel bir ara&ccedil; olan tty i&ccedil;in';
$lang['mediatype_tv']='tv : Televizyon-tipli cihazlar i&ccedil;in (işitme engelliler i&ccedil;in i&ccedil;in)';
$lang['menu_bookmarks']='[+].';
$lang['menutext']='Men&uuml; Yazısı';
$lang['metadata']='Metadata (bilgi hakkında bilgi)';
$lang['minimumversion']='En d&uuml;ş&uuml;k s&uuml;r&uuml;m';
$lang['minimumversionrequired']='İstenen en d&uuml;ş&uuml;k CMSMS S&uuml;r&uuml;m&uuml;';
$lang['minute']='dakika';
$lang['minutes']='dakikalar';
$lang['missingdependency']='Eksik Bağlılık';
$lang['missingparams']='Bazı parametreler eksik veya ge&ccedil;ersiz';
$lang['modifygroupassignments']='Grup Atamalarını D&uuml;zenle';
$lang['module']='Eklenti';
$lang['module_help']='Eklenti Yardım';
$lang['module_name']='Eklenti Adı';
$lang['moduleabout']='%s eklentisi hakkında';
$lang['moduledecides']='Modul Karar Versin';
$lang['moduledescription']='Eklentiler CMS Made Simple&#039;yi geliştirerek her &ccedil;eşit uyarlanmış işlevi sunmasını sağlar.';
$lang['moduleerrormessage']='%s Eklentisi i&ccedil;in Hata Mesajı';
$lang['modulehelp']='%s eklentisi i&ccedil;in yardım';
$lang['modulehelp_english']='İngilizce g&ouml;r&uuml;nt&uuml;le';
$lang['modulehelp_yourlang']='Kendi dilinizde g&ouml;r&uuml;nt&uuml;leyin';
$lang['moduleinstalled']='Eklenti zaten kurulu';
$lang['moduleinstallmessage']='%s Eklentisi i&ccedil;in Mesaj kur';
$lang['moduleinterface']='%s Arabirimi';
$lang['modules']='Eklentiler';
$lang['modulesnotwritable']='Eklenti (modules) klas&ouml;r&uuml; yazılabilir değil, eğer eklentileri XML dosyası y&uuml;kleme yoluyla kurmak istiyorsanız modules klas&ouml;r&uuml;ne t&uuml;m okuma/yazma/&ccedil;alıştırma yetkilerini vermelisiniz (chmod 777).';
$lang['moduleuninstallmessage']='%s Eklentisi i&ccedil;in Mesaj kaldır';
$lang['moduleupgraded']='Upgrade Başarılı';
$lang['moduleupgradeerror']='Eklenti y&uuml;kseltmede hata.';
$lang['move']='Taşı';
$lang['movecontent']='Sayfaları Taşı';
$lang['msg_defaultcontent']='T&uuml;m yeni sayfalarda &ouml;ntanımlı i&ccedil;erik olarak g&ouml;r&uuml;nmesi gereken kodu buraya ekleyin';
$lang['msg_defaultmetadata']='T&uuml;m yeni sayfalarda metadata b&ouml;l&uuml;m&uuml;nde g&ouml;r&uuml;nmesi gereken kodu buraya ekleyin';
$lang['myaccount']='Hesabım';
$lang['myaccountdescription']='Kişisel hesabınızın ayrıntılarını burada d&uuml;zenleyebilirsiniz.';
$lang['myprefs']='Tercihlerim';
$lang['myprefsdescription']='Site y&ouml;netim alanının istediğiniz gibi &ccedil;alışmasının uyarlamalarını burada yapabilirsiniz.';
$lang['name']='Ad';
$lang['needpermissionto']='Bu işlemi yapmak i&ccedil;in &#039;%s&#039; yetkisine sahip olmalısınız.';
$lang['needupgrade']='G&uuml;ncelleme gerekiyor';
$lang['never']='Asla';
$lang['new_version_available']='<em>Notice:</em> A new version of CMS Made Simple is available.  Please notify your administrator.';
$lang['new_window']='yeni pencere';
$lang['newstylesheetname']='Yeni Stilsayfası adı';
$lang['newtemplatename']='Yeni Şablon Adı';
$lang['next']='Sonraki';
$lang['no']='Hayır';
$lang['no_bulk_performed']='Toplu işlem yapılamadı.';
$lang['no_file_url']='Hi&ccedil;biri (Yukarıdaki URL&#039;i kullan)';
$lang['no_orders_changed']='Sayfaları yeniden sıralamayı se&ccedil;tiniz, ancak hi&ccedil;bir sayfanın sırasını değiştirmediniz. Sayfalar yeniden sıralanmadı.';
$lang['no_permission']='Bu &ouml;zelliği kullanma yetkiniz bulunmamaktadır.';
$lang['noaccessto']='%s&#039;e ulaşılamıyor';
$lang['nocss']='Stilsayfası yok';
$lang['nodefault']='Varsayılan Se&ccedil;ilmemiş';
$lang['noentries']='Girdi yok';
$lang['nofieldgiven']='%s girilmemiş!';
$lang['nofiles']='Dosya yok';
$lang['nogcbwysiwyg']='Genel İ&ccedil;erik Bloklarında WYSIWYG edit&ouml;r&uuml; devre dışı bırak';
$lang['noncachable']='Tamponlanamaz';
$lang['none']='Hi&ccedil;biri';
$lang['nopaging']='T&uuml;m &Ouml;ğeleri G&ouml;ster';
$lang['nopasswordforrecovery']='Bu kullanıcı ile ilgili mail adresi tanımlanmamış.  Şifre geri alma işlemi m&uuml;mk&uuml;n değil.  Y&ouml;netici ile iletişime ge&ccedil;iniz.';
$lang['nopasswordmatch']='Parolalar aynı değil';
$lang['norealdirectory']='Ger&ccedil;ek bir klas&ouml;r girilmemiş';
$lang['norealfile']='Ger&ccedil;ek bir dosya girilmemiş';
$lang['notification_to_handle']='Değerlendirme bekleyen<b>%d</b> uyarı var';
$lang['notifications']='Uyarılar';
$lang['notifications_to_handle']='Değerlendirme bekleyen<b>%d</b> uyarı var';
$lang['notinstalled']='Y&uuml;klenmedi';
$lang['noxmlfileuploaded']='Hi&ccedil;bir dosya y&uuml;klenmedi. Eklentiyi XML yoluyla kurmak i&ccedil;in bilgisayarınızdaki bir .xml eklenti dosyasını se&ccedil;ip y&uuml;klemelisiniz.';
$lang['of']='-in(ın)';
$lang['off']='Kapalı';
$lang['on']='A&ccedil;ık';
$lang['open']='A&ccedil;';
$lang['options']='Se&ccedil;enekler';
$lang['order']='Sırala';
$lang['order_too_large']='Bir sayfanın sırası aynı seviyedeki sayfa sayısından b&uuml;y&uuml;k olamaz. Sayfalar yeniden sıralanmadı.';
$lang['order_too_small']='Bir sayfanın sırası sıfır olamaz. Sayfalar yeniden sıralanmadı.';
$lang['originator']='&Ccedil;ıkaran';
$lang['other']='Diğer';
$lang['overwritemodule']='Varolan eklentilerin &uuml;st&uuml;ne yaz';
$lang['owner']='Sahip';
$lang['page']='Sayfa';
$lang['page_metadata']='Spesifik Sayfa Metadata';
$lang['page_reordered']='Sayfa başarılı olarak yeniden sıralandı.';
$lang['page_url']='Sayfa URL';
$lang['pagealias']='Sayfa Takma Adları';
$lang['pagedefaults']='Sayfa &Ouml;ntanımlıları';
$lang['pagedefaultsdescription']='Yeni sayfalar i&ccedil;in &ouml;ntanımlı değerleri uygula';
$lang['pagedefaultsupdated']='Sayfa varsayılan ayarları g&uuml;ncellendi';
$lang['pages']='Sayfalar';
$lang['pages_reordered']='Sayfaları başarılı olarak yeniden sıralandı';
$lang['pagesdescription']='Sayfa ve diğer i&ccedil;erik ekleme ve d&uuml;zenlemesi burada yapılıyor.';
$lang['parameters']='Parametreler';
$lang['parent']='&Uuml;st';
$lang['password']='Parola';
$lang['passwordagain']='Parola (yeniden)';
$lang['passwordchange']='L&uuml;tfen yeni şifre veriniz';
$lang['passwordchangedlogin']='Şifre değiştirildi.  L&uuml;tfen yeni bilgiler ile giriş yapınız.';
$lang['permission']='Yeki';
$lang['permission_information']='İzin Bilgisi';
$lang['permissions']='Yetkiler';
$lang['permissionschanged']='Yetkiler g&uuml;ncellendi.';
$lang['php_information']='PHP Bilgisi';
$lang['phpversion']='Şu anki PHP Versiyonu';
$lang['pluginabout']='%s etiketi hakkında';
$lang['pluginhelp']='%s etiketi i&ccedil;in yardım';
$lang['pluginmanagement']='Eklenti Y&ouml;netimi';
$lang['plugins']='Eklentiler';
$lang['preferences']='Tercihler';
$lang['preferencesdescription']='Site genelindeki tercihlerinizi burada ayarlarsınız.';
$lang['prefsupdated']='Tercihler g&uuml;ncellendi.';
$lang['preview']='&Ouml;nizle';
$lang['previewdescription']='Değişiklikler &ouml;nizle';
$lang['previous']='&Ouml;nceki';
$lang['prompt_use_smartycaching']='Smarty &Ouml;nbellekleme A&ccedil;ık';
$lang['read']='Oku';
$lang['recentpages']='Ge&ccedil;miş Sayfalar';
$lang['recoveryemailsent']='Email kayıtlı adrese g&ouml;nderildi. L&uuml;tfen g&ouml;nderilen a&ccedil;ıklamalar i&ccedil;in gelen kutusunu kontrol ediniz.';
$lang['remote_connection_timeout']='Bağlantı zamanaşımına uğradı!';
$lang['remove']='Kaldır';
$lang['removeconfirm']='Bu hareket eklentiyi oluşturan dosyaları geri d&ouml;n&uuml;lmez bir şekilde silecektir.\nDevam etmek istediğinizden emin misiniz?';
$lang['removecssassociation']='Stilsayfası ilişkisini kaldır';
$lang['reorder']='Yeniden Sırala';
$lang['reorderpages']='Sayfaları Yeniden Sırala';
$lang['results']='Sonu&ccedil;lar';
$lang['revert']='T&uuml;m değişiklikleri geri al';
$lang['run']='&Ccedil;alıştır';
$lang['run_udt']='Kullanıcı tanımlı etiketi &ccedil;alıştır';
$lang['runuserplugin']='Kullanıcı Pluginini &ccedil;alıştır';
$lang['safe_mode']='PHP G&uuml;venli Mod (Safe Mod)';
$lang['saveconfig']='Ayarları Kaydet';
$lang['search_module']='Arama mod&uuml;l&uuml;';
$lang['search_string_find']='Bağlantı ok!';
$lang['searchable']='Bu sayfa aranabilir';
$lang['secure']='G&uuml;venli (HTTPS)';
$lang['secure_page']='Bu sayfa i&ccedil;in HTTPS kullan';
$lang['selectall']='T&uuml;m&uuml;n&uuml; Se&ccedil;';
$lang['selecteditems']='Se&ccedil;ilenle';
$lang['selectgroup']='Grup Se&ccedil;';
$lang['send']='G&ouml;nder';
$lang['server_api']='Sunucu API';
$lang['server_cache_settings']='Server &Ouml;nbellek Ayarları';
$lang['server_db_grants']='Veritabanı erişim seviyesini kontrol et';
$lang['server_db_type']='Sunucu Veritabanı';
$lang['server_db_version']='Sunucu Veritabanı Versiyonu';
$lang['server_information']='Sunucu Bilgisi';
$lang['server_os']='Sunucu İşletim Sistemi';
$lang['server_software']='Sunucu Yazılımı';
$lang['setallcontent']='T&uuml;m Sayfaları Ayarla';
$lang['setallcontentconfirm']='T&uuml;m sayfalarda bu şablonu kullanmak istediğinizden emin misiniz?';
$lang['setfalse']='Yanlış yap';
$lang['settemplate']='Şablon Belirle';
$lang['settrue']='Doğru Yap';
$lang['setup']='Gelişmiş Ayarlar';
$lang['showall']='T&uuml;m&uuml;n&uuml; G&ouml;ster';
$lang['showbookmarks']='Y&ouml;netici Kısayollarını G&ouml;ster';
$lang['showfilters']='Filtre d&uuml;zenle';
$lang['showinmenu']='Men&uuml;de G&ouml;ster';
$lang['showrecent']='Ge&ccedil;mişte kullanılan sayfaları g&ouml;ster';
$lang['showsite']='Siteyi G&ouml;ster';
$lang['sibling_duplicate_order']='Aynı seviyedeki iki sayfanın sırası aynı olamaz. Sayfalar yeniden sıralanmadı.';
$lang['siteadmin']='Site Y&ouml;neticisi';
$lang['sitedown_settings']='Site Kapalı Ayarları';
$lang['sitedownexcludes']='Bu adresleri Site Kapalı mesajından &ccedil;ıkar.';
$lang['sitedownmessage']='Site &Ccedil;alışmıyor Mesajı';
$lang['sitedownwarning']='<strong>Uyarı:</strong> Siteniz şu anda &quot;Site bakım nedeniyle &ccedil;alışmıyor&quot; mesajı g&ouml;steriyor.  Bunu &ccedil;&ouml;zmek i&ccedil;in %s dosyasını siliniz.';
$lang['sitename']='Site Adı';
$lang['siteprefs']='Genel Ayarlar';
$lang['siteprefsupdated']='Genel Ayarlar g&uuml;ncellendi';
$lang['smarty_settings']='Smarty Ayarları';
$lang['start_upgrade_process']='G&uuml;ncelleme S&uuml;recini Başlat';
$lang['status']='Durum';
$lang['stylesheet']='Stilsayfası';
$lang['stylesheetcopied']='Stil Kopyalandı';
$lang['stylesheetexists']='Stilsayfası var';
$lang['stylesheetnotfound']='Stylesheet %d bulunamadı';
$lang['stylesheets']='Tasarım şablonları';
$lang['stylesheetsdescription']='Stilsayfası y&ouml;netimi ardışık tasarım şablonlarını (CSS) ele almanın gelişmiş bir yoludur.';
$lang['stylesheetstodelete']='Bu tasarım şablonları silinecek';
$lang['subitems']='Alt &ouml;ğeler';
$lang['submit']='G&ouml;nder';
$lang['submitdescription']='Değişiklikleri kaydet';
$lang['success']='Başarı';
$lang['syntaxhighlightertouse']='Kullanmak i&ccedil;in s&ouml;zdizim vurgulayıcısını se&ccedil;';
$lang['sysmain_cache_status']='&Ouml;nbellek durumu';
$lang['sysmain_confirmclearcache']='&Ouml;nbelleği silmek istediğinize emin misiniz?';
$lang['sysmain_content_status']='İ&ccedil;erik durumu';
$lang['sysmain_database_status']='Veritabanı durumu';
$lang['sysmain_fixtypes']='standart i&ccedil;erik sayfasına &ccedil;evir';
$lang['sysmain_nocontenterrors']='İ&ccedil;erik hatası yok';
$lang['sysmain_optimize']='Optimize Et';
$lang['sysmain_optimizetables']='Tabloları optimize et';
$lang['sysmain_pagesfound']='sayfa bulundu';
$lang['sysmain_repair']='Tamir et';
$lang['sysmain_repairtables']='Tabloları tamir et';
$lang['sysmain_tablesoptimized']='Tablolar optimize edildi';
$lang['sysmain_tablesrepaired']='Tablolar tamir edildi';
$lang['sysmain_typesfixed']='sayfa i&ccedil;erik tipleri onarıldı';
$lang['sysmain_update']='G&uuml;ncelle';
$lang['sysmaintab_changelog']='Değişim logları';
$lang['sysmaintab_content']='&Ouml;nbellek ve İ&ccedil;erik';
$lang['sysmaintab_database']='Veritabanı';
$lang['systeminfo']='Sistem Bilgisi';
$lang['systemmaintenance']='Sistem Bakımı';
$lang['tabindex']='Sekme Dizini';
$lang['tagdescription']='Etketler i&ccedil;erik ve/veya şablonlarınıza ekleyebileceğiniz k&uuml;&ccedil;&uuml;k işlevlerdir.';
$lang['tags']='Etiketler';
$lang['tagtousegcb']='Bu bloğu kullanmak i&ccedil;in işaretle';
$lang['target']='Hedef';
$lang['team']='Takım';
$lang['template']='Şablon';
$lang['templatecopied']='Şablon Kopyalandı';
$lang['templatecss']='Şablonları Stilsayfasına ata';
$lang['templateexists']='Şablon adı zaten var';
$lang['templatemanagement']='Şablon Y&ouml;netimi';
$lang['templates']='Şablonlar';
$lang['templatesdescription']='Şablon ekleme ve d&uuml;zenleme burada yapılır. Şablonlar sitenizin g&ouml;r&uuml;nt&uuml;s&uuml;n&uuml; tanımlar.';
$lang['templatestodelete']='Bu şablonlar silinecek';
$lang['test']='Deneme';
$lang['text_settemplate']='Se&ccedil;ilen sayfa i&ccedil;in farklı bir şablon belirle';
$lang['thumbnail']='K&uuml;&ccedil;&uuml;k Resim';
$lang['thumbnail_height']='K&uuml;&ccedil;&uuml;k Resim Y&uuml;kseklik';
$lang['thumbnail_width']='K&uuml;&ccedil;&uuml;k Resim Genişlik';
$lang['title']='Başlık';
$lang['titleattribute']='A&ccedil;ıklama (başlık &ouml;zelliği)';
$lang['tools']='Ara&ccedil;lar';
$lang['troubleshooting']='(Sorun giderme)';
$lang['true']='Doğru';
$lang['type']='Tip';
$lang['typenotvalid']='Tip ge&ccedil;erli değil';
$lang['uninstall']='Kaldır';
$lang['uninstallconfirm']='Eklentiyi kaldırmak istediğinizden emin misiniz? Adı:';
$lang['unknown']='Bilinmeyen';
$lang['unlimited']='Limitsiz';
$lang['untested']='Denenmemiş';
$lang['up']='Yukarı';
$lang['updateperm']='Yetkileri G&uuml;ncelle';
$lang['upgrade']='G&uuml;ncelle';
$lang['upgradeconfirm']='Bunu g&uuml;ncellemek istediğinizden emin misiniz?';
$lang['uploadfile']='Dosya Y&uuml;kle';
$lang['uploadxmlfile']='XML dosyası ile eklenti kur';
$lang['url']='URL.';
$lang['use_wysiwyg']='WYSIWYG edit&ouml;r&uuml; kullan';
$lang['useadvancedcss']='Gelişmiş Stilsayfası Y&ouml;netimini kullan';
$lang['user']='Kullanıcı';
$lang['user_created']='Uyarlanmış Kısayollar';
$lang['user_login']='Kullanıcı Girişi';
$lang['user_logout']='Kullanıcı &Ccedil;ıkışı';
$lang['user_tag']='Kullanıcı Etiketi';
$lang['useraccount']='Kullanıcı Hesabı';
$lang['userdefinedtags']='Kullanıcı Tanımlı Etiketler';
$lang['usermanagement']='Kullanıcı Y&ouml;netimi';
$lang['username']='Kullanıcı adı';
$lang['usernameincorrect']='Kullanıcı adı veya parolası ge&ccedil;ersiz';
$lang['usernotfound']='Kullanıcı Bulunamadı';
$lang['userprefs']='Kullanıcı Tercihleri';
$lang['users']='Kullanıcılar';
$lang['usersassignedtogroup']='Kullanıcılar %s Grubuna atandı';
$lang['usersdescription']='Kullanıcıları burada y&ouml;netirsiniz.';
$lang['usersgroups']='Kullanıcılar &amp; Gruplar';
$lang['usersgroupsdescription']='Kullanıcı ve Grup ilintili &ouml;ğeler.';
$lang['usertagadded']='Kullanıcı Tanımlı Etiket başarılı olarak eklendi.';
$lang['usertagdeleted']='Kullanıcı Tanımlı Etiket başarılı olarak kaldırıldı.';
$lang['usertagdescription']='Gezgininizden kendinizin yaratıp d&uuml;zenleyebileceği etiketler.';
$lang['usertagexists']='Bu adda bir etiket zaten var. L&uuml;tfen başkasını se&ccedil;iniz.';
$lang['usertags']='Kullanıcı Tanımlı Etiketler';
$lang['usertagupdated']='Kullanıcı Tanımlı Etiket başarılı olarak g&uuml;ncellendi.';
$lang['usewysiwyg']='İ&ccedil;erik i&ccedil;in WYSIWYG d&uuml;zenleyiciyi kullan';
$lang['version']='S&uuml;r&uuml;m';
$lang['view']='İzle';
$lang['view_page']='Bu sayfayı yeni pencerede g&ouml;r&uuml;nt&uuml;le';
$lang['viewsite']='Siteyi İzle';
$lang['warn_admin_ipandcookies']='Uyarı: Admin aktiviteleri cookieleri kullanır ve IP adresini izlemektedir.';
$lang['warning_safe_mode']='<strong><em>WARNING:</em></strong> PHP G&uuml;venli modu aktive edildi.  Bu durum web tarayıcıyla y&uuml;klenen, resim, tema ve XML mod&uuml;l paketlerini de i&ccedil;eren bazı belgelerde g&uuml;&ccedil;l&uuml;klere sebep olacaktır. Site y&ouml;neticinizle g&uuml;venli modu kapaması konusunda g&ouml;r&uuml;şmeniz &ouml;nerilmektedir.';
$lang['warning_upgrade']='<em><strong>Uyarı:</strong></em> CMSMS sisteminin g&uuml;ncellenmesi gerekmektedir.';
$lang['warning_upgrade_info2']='L&uuml;tfen linki tıklayınız: %s.';
$lang['welcome_user']='Hoşgeldiniz';
$lang['welcomemsg']='Hoşgeldin %s';
$lang['wiki']='Wiki.';
$lang['wikihelp']='Topluluk Yardımı';
$lang['wontdeletetemplateinuse']='Bu şablonlar kullanıldığı i&ccedil;in silinmeyecek';
$lang['write']='Yaz';
$lang['wysiwygtouse']='Kullanacağınız WYSIWYG&#039;i se&ccedil;iniz';
$lang['xml']='XML.';
$lang['xmlmodulerepository']='Mod&uuml;lHavuzu web sunucusunun URL&#039;i';
$lang['yes']='Evet';
$lang['your_ipaddress']='IP Adresiniz';
?>