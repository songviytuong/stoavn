<?php
if (!function_exists('cmsms')) exit;

if (!$this->VisibleToAdminUser()) {
  echo $this->Lang("accessdenied");
  return;
}


$db = cmsms()->GetDb();
$admintheme = cms_utils::get_theme_object();


$tab="";
if (isset($params["tab"])) {
	$tab=$params["tab"];
}



$smarty->assign('accessTabTemplates', $this->CheckPermission('Modify Templates'));
$smarty->assign('accessTabShowcss', $this->CheckPermission('Modify Stylesheets'));

$smarty->assign('currentTabQuotes', 'quotes' == $tab);
$smarty->assign('currentTabGroups', 'groups' == $tab);
$smarty->assign('currentTabTemplates', 'templates' == $tab);
$smarty->assign('currentTabSettings', 'settings' == $tab);
$smarty->assign('currentTabCss', 'csstab' == $tab);

$smarty->setTemplateDir(array_merge($smarty->getTemplateDir(), array($this->GetModulePath().'/templates/')));

$quotes = QuotesDAO::GetQuoteEntries();
$countQ = count($quotes);
$maxPerPage = 20;
$start = 0;
if(isset($params['s']) && preg_match("/[0-9](.*)/", $params['s']) && $countQ >= $params['s']){
	$start = $params['s'];
}
if($countQ > 1){
	$quotes = array_slice($quotes, $start, $maxPerPage);
}
$countQ = ceil($countQ / $maxPerPage);
$urlPager = $this->create_url($id, 'defaultadmin', $returnid);

$smarty->assign('urlPager', $urlPager);
$smarty->assign('nbPages', $countQ);
$smarty->assign('maxPerPage', $maxPerPage);


$showquotes = array();
if (TRUE == empty($quotes)) {
	$smarty->assign('noquotestext', $this->Lang("noquotes"));
} else {
	foreach ($quotes as $quote) {
		$onerow = new stdClass();

	    switch($quote["type"]) {
	    	case "1" : $quote["content"]=substr($quote["content"],0,200); break;
	    }
		$onerow->content = $this->CreateLink($id, 'editquote', $returnid, strip_tags($quote["content"]), 
				array('quoteid'=>$quote["id"],"todo"=>"edit","type"=>$quote["type"]));
		$onerow->id = $quote["id"];
		$onerow->author = "";
		$onerow->reference = "";
		if (isset($quote["author"])) $onerow->author = $quote["author"];
		if (isset($quote["reference"])) $onerow->reference = $quote["reference"];
		$onerow->exposure="0";
		if (isset($quote["exposures"])) $onerow->exposure=$quote["exposures"];
		$onerow->quotetype = $this->_GetTypeName($quote["type"]);

		$onerow->editlink = $this->CreateLink($id, 'editquote', $returnid,
			$admintheme->DisplayImage('icons/system/edit.gif', $this->Lang("editquote"),'','','systemicon'),
			array('quoteid' => $quote["id"],"todo"=>"edit","type"=>$quote["type"]));
		
		$onerow->deletelink = $this->CreateLink($id, 'editquote', $returnid,
			$admintheme->DisplayImage('icons/system/delete.gif', $this->Lang("deletequote"),'','','systemicon'),
			array('quoteid' => $quote["id"],"todo"=>"delete","type"=>$quote["type"]), $this->Lang("confirmdeletequote"));

		array_push($showquotes, $onerow);
	}
	
}
$smarty->assign('quotetype', $this->Lang("quotetype"));
$smarty->assign('quotes', $this->Lang("quotes"));
$smarty->assign('quoteexposures', $this->Lang("quoteexposures"));
$smarty->assign('quoteauthor', $this->Lang("quoteauthor"));
$smarty->assign('quotereference', $this->Lang("quotereference"));
$smarty->assign('actions', $this->Lang("actions"));
$smarty->assign_by_ref('itemsQuotes', $showquotes);

$smarty->assign('itemcountQuotes', count($showquotes));

$form=$this->CreateFormStart($id,"editquote",$returnid,"post","",false,"",array("todo"=>"add"));
$form.=$this->CreateInputDropdown($id,"type",$this->_GetTypes());
$form.=$this->CreateInputSubmit($id,"submitQuotes",$this->Lang("addquote"));
$form.= $this->CreateFormEnd();


$smarty->assign('addformQuotes', $form);



/************************************************************/


$groups=QuotesDAO::GetGroups();
$showgroups = array();
if (TRUE == empty($groups)) {
	$smarty->assign('nogroupstext', $this->Lang("nogroups"));
} else {
	foreach ($groups as $group) {
		$onerow = new stdClass();
		
		$onerow->code = "";
		if (!empty($group["textid"])) {
			$onerow->code = $this->CreateLink($id, 'editgroup', $returnid, $group["textid"], array('groupid'=>$group["id"],"todo"=>"edit"));
		}
		
		$onerow->desc = "";
		if (!empty($group["description"])) {
			$onerow->desc = $group["description"];
		}
		$onerow->id = $group["id"];			
		$onerow->editlink = $this->CreateLink($id, 'editgroup', $returnid,
				$admintheme->DisplayImage('icons/system/edit.gif', $this->Lang("editgroup"),'','','systemicon'),
				array('groupid' => $group["id"],"todo"=>"edit"));
		
		$onerow->deletelink = $this->CreateLink($id, 'editgroup', $returnid,
				$admintheme->DisplayImage('icons/system/delete.gif', $this->Lang("deletegroup"),'','','systemicon'),
				array('groupid' => $group["id"],"todo"=>"delete"), $this->Lang("confirmdeletegroup"));

		array_push($showgroups, $onerow);
	}
	
}

$smarty->assign_by_ref('groups', $this->Lang("groups"));
$smarty->assign_by_ref('actionsGroups', $this->Lang("actions"));
$smarty->assign_by_ref('itemsGroups', $showgroups);
$smarty->assign('itemcountGroups', count($showgroups));

$link=$this->CreateLink($id, 'editgroup', 0, $admintheme->DisplayImage('icons/system/newobject.gif', $this->Lang("addgroup"),'','','systemicon'), 
			array(), '', false, false, '') .' '. 
	  $this->CreateLink($id, 'editgroup', $returnid, $this->lang("addgroup"), 
	  		array("todo"=>"add"), '', false, false, 'class="pageoptions"');

$smarty->assign('addlinkGroups', $link);


/************************************************************/


if( $this->CheckPermission('Modify Templates') ) {


	$templates=QuotesDAO::GetTemplates();

	$showtemplates = array();
	if (TRUE == empty($templates)) {
		$smarty->assign('notemplatestext', $this->Lang("notemplates"));
	} else {
		foreach ($templates as $template) {
			//print_r($template);

			$onerow = new stdClass();
			$onerow->name = $this->CreateLink($id, 'edittemplate', $returnid, $template["name"], 
					array('templateid'=>$template["id"],"todo"=>"edit"));
			$onerow->id = $template["id"];
			 				
			//echo $onerow->id;
			$onerow->editlink = $this->CreateLink($id, 'edittemplate', $returnid,
			$admintheme->DisplayImage('icons/system/edit.gif', $this->Lang("edittemplate"),'','','systemicon'),
					array('templateid' => $template["id"],"todo"=>"edit"));
					$onerow->copylink = $this->CreateLink($id, 'edittemplate', $returnid,
			$admintheme->DisplayImage('icons/system/copy.gif', $this->Lang("copytemplate"),'','','systemicon'),
					array('templateid' => $template["id"],"todo"=>"copy"));
			if ($template["name"]!="default") {
					$onerow->deletelink = $this->CreateLink($id, 'edittemplate', $returnid,
			$admintheme->DisplayImage('icons/system/delete.gif', $this->Lang("deletetemplate"),'','','systemicon'),
					array('templateid' => $template["id"],"todo"=>"delete"), $this->Lang("confirmdeletetemplate"));
			}
			array_push($showtemplates, $onerow);
		}	
	}

	$smarty->assign_by_ref('templates', $this->Lang("templates"));
	$smarty->assign_by_ref('actionsTemplates', $this->Lang("actions"));
	$smarty->assign_by_ref('itemsTemplates', $showtemplates);
	$smarty->assign('itemcountTemplates', count($showtemplates));

	$link=$this->CreateLink($id, 'edittemplate', 0, $admintheme->DisplayImage('icons/system/newobject.gif', $this->Lang("addtemplate"),'','','systemicon'), 
			array(), '', false, false, '') .' '. 
	      $this->CreateLink($id, 'edittemplate', $returnid, $this->Lang("addtemplate"), 
	      	array("todo"=>"add"), '', false, false, 'class="pageoptions"');

	$smarty->assign('addlinkTemplates', $link);

}

/************************************************************/


$smarty->assign('formstartSettings',$this->CreateFormStart($id,"savesettings",$returnid));
$smarty->assign('formend',$this->CreateFormEnd());

$smarty->assign('allowwysiwygtext', $this->Lang("allowwysiwyg"));
$smarty->assign('allowwysiwyginput', $this->CreateInputCheckbox($id,"allowwysiwyg",'1',$this->GetPreference("allowwysiwyg",'0')));

$smarty->assign('submitSettings', $this->CreateInputSubmit($id,"submit",$this->Lang("savesettings")));//"Gem indstillinger"));

/************************************************************/

echo $this->ProcessTemplate('admin.tpl');

?>