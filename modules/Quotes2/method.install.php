<?php
if (!function_exists('cmsms')) exit;

$db = cmsms()->GetDb();

$db_prefix = cms_db_prefix();
$dict = NewDataDictionary($db);
$flds= "
		id I,
    textid C(32),
		description C(255)
	";

$taboptarray = array('mysql' => 'TYPE=MyISAM');
$sqlarray = $dict->CreateTableSQL(cms_db_prefix().'module_quotegroups', $flds, $taboptarray);
$dict->ExecuteSQLArray($sqlarray);
$db->CreateSequence( cms_db_prefix()."module_quotegroups_seq" );

$flds= "
		id I,
    type I		
	";

$taboptarray = array('mysql' => 'TYPE=MyISAM');
$sqlarray = $dict->CreateTableSQL(cms_db_prefix().'module_quotes', $flds, $taboptarray);
$dict->ExecuteSQLArray($sqlarray);
$db->CreateSequence( cms_db_prefix()."module_quotes_seq" );

$flds= "
		quoteid I,
    name C(80),
		value X
	";
$taboptarray = array('mysql' => 'TYPE=MyISAM');
$sqlarray = $dict->CreateTableSQL(cms_db_prefix().'module_quoteprops', $flds, $taboptarray);
$dict->ExecuteSQLArray($sqlarray);


$flds= "
		id I,
    name C(80),
		isdefault I,
		content X
	";
$taboptarray = array('mysql' => 'TYPE=MyISAM');
$sqlarray = $dict->CreateTableSQL(cms_db_prefix().'module_quotetemplates', $flds, $taboptarray);
$dict->ExecuteSQLArray($sqlarray);
$db->CreateSequence( cms_db_prefix()."module_quotetemplates_seq" );

$flds= "
		quoteid I,
    groupid I		
	";
$taboptarray = array('mysql' => 'TYPE=MyISAM');
$sqlarray = $dict->CreateTableSQL(cms_db_prefix().'module_quoteconnections', $flds, $taboptarray);
$dict->ExecuteSQLArray($sqlarray);

QuotesDAO::AddTemplate("default",file_get_contents("../modules/Quotes2/templates/default.tpl"));

$this->CreatePermission('managequotes', $this->Lang('permission'));
$config = cmsms()->GetConfig();

$txt = file_get_contents($config['root_path'] . '/modules/' . $this->GetName() . '/css/stylesheet.css');
$css = new CmsLayoutStylesheet;
$css->set_name('Quotes Made Simple');
$css->set_description('The stylesheet for the module Quotes Made Simple (Reloaded)');
$css->set_content($txt);
$css->save();

?>