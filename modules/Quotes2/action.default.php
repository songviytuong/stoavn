<?php
if (!function_exists('cmsms')) exit;

$quotes=QuotesDAO::SelectQuotes($params);

if(count($quotes['quotes']) == 0){

	$quotes['quotes'][] = array(
		'content' => $this->lang("nomatchingquotes"),
		'author' => "Roy Batty",
		'reference' => "from the BladeRunner movie",
		'quoteexposures' => "0");
}

$template="default";
if (isset($params["template"])) $template=$params["template"];

$template=QuotesDAO::GetTemplate("",$template);

foreach ($quotes['quotes'] as $quote) {
    foreach ($quote as $propname=>$value) {
        $smarty->assign("quote".$propname, $value);
    }
    echo $this->ProcessTemplateFromData($template["content"]);
}
?>