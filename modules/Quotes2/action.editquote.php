<?php
if (!function_exists('cmsms')) exit;

if (!isset($params["todo"])) exit;


$inerror=false;
//print_r($params);die();
$params["tab"]="quotes";
$quoteid="";
if ($params["todo"]!="add" && !isset($params["quoteid"])) {
	echo "Internal error"; exit;
} else {
	if (isset($params["quoteid"])) $quoteid=$params["quoteid"];
}
$type="";
if (isset($params["type"])) $type=$params["type"];

switch ($params["todo"]) {
	case "delete" : {
		//echo $locationid;
		QuotesDAO::DeleteQuote($quoteid);
		$params["module_message"]=$this->Lang("quotedeleted");
		$this->Redirect($id, 'defaultadmin', $returnid,$params);
		break;
	}
	case "update" :
	case "save" : {
		if (!isset($params["type"]) || $params["type"]=="") {
			echo $this->ShowErrors($this->Lang("missingtype"));
			$inerror=true;
			break;
		}
		if (!isset($params["content"]) || $params["content"]=="") {
			echo $this->ShowErrors($this->Lang("missingcontent"));
			$inerror=true;
			break;
		}
		if ($params["todo"]=="save") {
			$quoteid=QuotesDAO::AddQuote($type);
		}
		QuotesDAO::SetQuoteProp($quoteid,"content",$params["content"]);
		QuotesDAO::SetQuoteProp($quoteid,"textid",$params["textid"]);
		switch ($type) {
			case "1" : {
				QuotesDAO::SetQuoteProp($quoteid,"author",$params["author"]);
				QuotesDAO::SetQuoteProp($quoteid,"reference",$params["reference"]);
				break;
			}
			case "2" : {
				
			}
		}

		$groups=QuotesDAO::GetGroups();
		if (!empty($groups)) {
			QuotesDAO::ClearConnections($quoteid);
			foreach ($groups as $group) {
				if (isset($params["conn".$group["id"]]) && $params["conn".$group["id"]]=="1") {
					QuotesDAO::SetConnection($quoteid,$group["id"]);
				}
			}
		}


		if ($params["todo"]=="save") {
			$params["module_message"]=$this->Lang("quoteadded");
		} else {
			$params["module_message"]=$this->Lang("quoteupdated");
		}
		$this->Redirect($id, 'defaultadmin', $returnid,$params);
		break;
	}
}


$content="";
$textid="";
$author="";
$reference="";

if ($params["todo"]=="edit" || $params["todo"]=="copy") {
	$thisquote=QuotesDAO::GetQuote("",$quoteid);
	$content=$thisquote["content"];
	$textid=$thisquote["textid"];
	switch ($type) {
		case 1 : {
			$author=$thisquote["author"];
			$reference=$thisquote["reference"];
			break;
		}
		case 2 : {
			$rssparsing=$thisquote["rssparsing"];
			break;
		}
	}

}

if (isset($params["content"])) $content=$params["content"];
if (isset($params["textid"])) $textid=$params["textid"];
switch($type) {
	case 1 : {
		if (isset($params["author"])) $author=$params["author"];
		if (isset($params["reference"])) $reference=$params["reference"];
		break;
	}
	case 2 : {
		if (isset($params["rssparsing"])) $rssparsing=$params["rssparsing"];
		break;
	}
}


$newtodo="";
if ($inerror) {
	$newtodo=$params["todo"];
} else {
	if ($params["todo"]=="edit") $newtodo="update";
	if ($params["todo"]=="add" || $params["todo"]=="copy") $newtodo="save";
}


$smarty->assign('formstart',$this->CreateFormStart($id,"editquote",$returnid,"post","",false,"",array("todo"=>$newtodo,"quoteid"=>$quoteid,"type"=>$type)));
$smarty->assign('formend',$this->CreateFormEnd());

$smarty->assign('quotetextid',$this->Lang("quotetextid"));
$smarty->assign('quotetextidhelp',$this->Lang("quotetextidhelp"));
$smarty->assign('textidinput',$this->CreateInputText($id,"textid",$textid,32,40));

switch ($type) {
	case 1 : {
		$smarty->assign('quotecontent',$this->Lang("quotecontent"));
		$usewysiwyg=($this->GetPreference("allowwysiwyg","0")=="1");
		$smarty->assign('contentinput',$this->CreateTextArea($usewysiwyg,$id,$content,"content",'pagesmalltextarea','','','',80,6));

		$smarty->assign('quoteauthor',$this->Lang("quoteauthor"));
		$smarty->assign('authorinput',$this->CreateInputText($id,"author",$author,40,100));

		$smarty->assign('quotereference',$this->Lang("quotereference"));
		$smarty->assign('referenceinput',$this->CreateInputText($id,"reference",$reference,40,100));
		break;
	}
	case 2 : {
		$smarty->assign('rssurltext',$this->Lang("rssquotecontent"));
		$smarty->assign('rssurlinput',$this->CreateInputText($id,"content",$content,80,255));

		$smarty->assign('rssparsingtext',$this->Lang("rssparsingtext"));
		$smarty->assign('rssparsinginput',$this->CreateInputText($id,"rssparsing",$rssparsing,80,255));
		$smarty->assign('rssparsinghelp',$this->Lang("rssparsinghelp"));
		break;

	}
}


$smarty->assign('type',$type);

$groups=QuotesDAO::GetGroups();

$groupsinput="";
if (TRUE == empty($groups)) {
	$smarty->assign('groupsinput', $this->Lang("nogroups"));
} else {
	foreach ($groups as $group) {
		$name=$group["description"];
		if ($name=="") $name=$group["textid"];
		$groupsinput.=$this->CreateInputCheckbox($id,"conn".$group["id"],"1",QuotesDAO::GetConnection($quoteid,$group["id"]));
		$groupsinput.=$this->CreateLabelForInput($id,"conn".$group["id"],$name);
		$groupsinput.="<br/>";
	}
	$smarty->assign('groupsinput', $groupsinput);
}
$smarty->assign('groupstext', $this->Lang("groups"));

$titleaction="";
switch($params["todo"]) {
	case "add" : $titleaction=$this->Lang("addingquote"); break;
	case "copy" : $titleaction=$this->Lang("copyingquote"); break;
	case "edit" : $titleaction=$this->Lang("editingquote"); break;
}

$smarty->assign('titleaction', $titleaction);

if ($params["todo"]=="edit") {
	$smarty->assign('submit', $this->CreateInputSubmit($id,"submit",$this->Lang("updatequote")));
} else {
	$smarty->assign('submit', $this->CreateInputSubmit($id,"submit",$this->Lang("addquote")));
}

$smarty->assign('backlink', $this->CreateLink($id,"defaultadmin",$returnid,$this->Lang("cancel"),array("tab"=>"quotes"),'',false,false,'class="pageback"'));

echo $this->ProcessTemplate('admineditquote.tpl');




?>