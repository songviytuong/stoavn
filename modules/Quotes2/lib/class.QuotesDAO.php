<?php

class QuotesDAO{

	private function __construct(){
	}
	
	function GetRSSQuotes($info) {
		$quoteinfo=QuotesDAO::GetQuote("",$info["id"]);
		$rss=new Rss($quoteinfo["content"]);
		if ($rss->inerror!="")  {
			echo $rss->inerror;
		  return false;		  
		}
		$rssquotes=$rss->parseItems();
		
		return $rssquotes;
	}

	function GetQuoteProps($quoteid) {
		
		$q="SELECT name,value FROM ".cms_db_prefix()."module_quoteprops WHERE quoteid=?";
		$p=array($quoteid);
		$result=cmsms()->GetDb()->Execute($q,$p);
		if (!$result || ($result->NumRows()==0)) {
			return false;
		}
		$output=array();
		while($row=$result->FetchRow()) {
			$output[$row["name"]]=$row["value"];
		}
		return $output;
	}

	function GetQuoteProp($quoteid,$name,$default=false) {
		$props=QuotesDAO::GetQuoteProps($quoteid);
		if (!$props) return $default;
		if (!isset($props[$name])) return $default;
		return $props[$name];
	}


	function SetQuoteProp($quoteid,$name,$value) {
		if ($quoteid=="") return false;
		
		$q="";

		if (QuotesDAO::GetQuoteProp($quoteid,$name)!==false) {
			$q="UPDATE ".cms_db_prefix()."module_quoteprops SET value=? WHERE name=? AND quoteid=?";
		} else {
			$q="INSERT INTO ".cms_db_prefix()."module_quoteprops (value, name, quoteid) VALUES (?,?,?)";
		}
		$p=array($value,$name,$quoteid);
		$result=cmsms()->GetDb()->Execute($q,$p);
		return ($result==true);
	}

	function RemoveQuoteProp($quoteid,$name="") {
		
		$q="DELETE FROM ".cms_db_prefix()."module_quoteprops WHERE id=?";
		if ($name!="") $q.="AND name=?";
		$p=array();
		if ($name!="") $p=array($name,$quoteid); else $p=array($quoteid);
		$result=cmsms()->GetDb()->Execute($q,$p);
		return ($result==true);
	}

	function AddGroup($textid,$description) {
		
		$newid=cmsms()->GetDb()->GenID(cms_db_prefix()."module_quotegroups_seq");
		$sql="INSERT INTO ".cms_db_prefix()."module_quotegroups (id,textid,description) VALUES (?,?,?)";
		$values=array($newid,$textid,$description);
		$result=cmsms()->GetDb()->Execute($sql,$values);
		return $newid;
	}

	function UpdateGroup($id,$textid,$description) {
		
		$q="UPDATE ".cms_db_prefix()."module_quotegroups SET textid=?,description=? WHERE id=?";
		$p=array($textid,$description,$id);
		$result=cmsms()->GetDb()->Execute($q,$p);
		return true;
	}

	function GetGroup($textid="",$id="") {
		
		$q="";$p="";
		if ($textid!="") {
			$q="SELECT * FROM ".cms_db_prefix()."module_quotegroups WHERE textid=?";
			$p=array($textid);
		} else {
			$q="SELECT * FROM ".cms_db_prefix()."module_quotegroups WHERE id=?";
			$p=array($id);
		}
		$result=cmsms()->GetDb()->Execute($q,$p);
		if (!$result || $result->RecordCount()==0) return false;
		$row=$result->FetchRow();
		return $row;
	}

	function GetGroups() {
		
		$q="SELECT * FROM ".cms_db_prefix()."module_quotegroups";

		$result=cmsms()->GetDb()->Execute($q);
		if (!$result || ($result->NumRows()==0)) {
			return false;
		}
		$output=array();
		while($row=$result->FetchRow()) {
			$output[]=$row;
		}
		return $output;
	}

	function AddQuote($type) {		
		
		$newid=cmsms()->GetDb()->GenID(cms_db_prefix()."module_quotes_seq");
		$sql="INSERT INTO ".cms_db_prefix()."module_quotes (id,type) VALUES (?,?)";
		$values=array($newid,$type);
		$result=cmsms()->GetDb()->Execute($sql,$values);
		return $newid;
	}
	
	function GetQuoteEntries() {
		
		$q="SELECT * FROM ".cms_db_prefix()."module_quotes";

		$result=cmsms()->GetDb()->Execute($q);
		if (!$result || ($result->NumRows()==0)) {
			return false;
		}
		$output=array();
		while($row=$result->FetchRow()) {
			$props=QuotesDAO::GetQuoteProps($row["id"]);
			$row=array_merge($row,$props);
			$output[]=$row;			
		}
		return $output;
	}
	

	function GetQuotes() {
		
		$q="SELECT * FROM ".cms_db_prefix()."module_quotes";

		$result=cmsms()->GetDb()->Execute($q);
		if (!$result || ($result->NumRows()==0)) {
			return false;
		}
		$output=array();
		while($row=$result->FetchRow()) {
			$props=QuotesDAO::GetQuoteProps($row["id"]);
			switch($row["type"]) {
				case 1 : {
					$row=array_merge($row,$props);
					$output[]=$row;
					break;
				}
				case 2 : {
					$rssquotes=QuotesDAO::GetRSSQuotes($row);
					if (count($rssquotes)>0) {						
						foreach($rssquotes as $rssquote) {
							$output[]=$rssquote;
						}
					}
				}
			}
			
			
		}
		return $output;
	}

	function GetQuote($textid="",$id="") {
		
		$q="";$p="";
		if ($textid!="") {
			$q="SELECT * FROM ".cms_db_prefix()."module_quotes WHERE textid=?";
			$p=array($textid);
		} else {
			$q="SELECT * FROM ".cms_db_prefix()."module_quotes WHERE id=?";
			$p=array($id);
		}
		$result=cmsms()->GetDb()->Execute($q,$p);
		if (!$result || $result->RecordCount()==0) return false;
		$row=$result->FetchRow();
		$props=QuotesDAO::GetQuoteProps($row["id"]);
		$row=array_merge($row,$props);
		return $row;
	}

	function DeleteGroup($groupid) {
		QuotesDAO::ClearConnections("",$groupid);
		
		$sql="DELETE FROM ".cms_db_prefix()."module_quotegroups WHERE id=?";
		$values=array($groupid);
		$result=cmsms()->GetDb()->Execute($sql,$values);
		return $result;
	}

	function DeleteQuote($quoteid) {
		QuotesDAO::ClearConnections($quoteid);
		QuotesDAO::RemoveQuoteProp($quoteid);
		
		$sql="DELETE FROM ".cms_db_prefix()."module_quotes WHERE id=?";
		$values=array($quoteid);
		$result=cmsms()->GetDb()->Execute($sql,$values);
		return $result;
	}

	function ClearConnections($quoteid="",$groupid="") {
		
		$result=false;
		if ($quoteid!="") {
			$q="DELETE FROM ".cms_db_prefix()."module_quoteconnections WHERE quoteid=?";
			$dbresult=cmsms()->GetDb()->Execute($q,array($quoteid));
			$result=($dbresult!=false);
		}
		if ($groupid!="") {
			$q="DELETE FROM ".cms_db_prefix()."module_quoteconnections WHERE groupid=?";
			$dbresult=cmsms()->GetDb()->Execute($q,array($groupid));
			$result=($dbresult!=false);
		}
		return $result;
	}

	function GetConnection($quoteid, $groupid) {
		
		$q="SELECT * FROM ".cms_db_prefix()."module_quoteconnections WHERE quoteid=? AND groupid=?";
		$result=cmsms()->GetDb()->Execute($q,array($quoteid,$groupid));
		if (!$result || ($result->NumRows()==0)) {
			return false;
		}
		return true;
	}

	function SetConnection($quoteid,$groupid) {
		if (QuotesDAO::GetConnection($quoteid, $groupid)) return true;
		
		$q="INSERT INTO ".cms_db_prefix()."module_quoteconnections (quoteid,groupid) VALUES (?,?)";
		$result=cmsms()->GetDb()->Execute($q,array($quoteid,$groupid));
		if (!$result) return "0";
		return "1";
	}

	function GetTemplates() {
		
		$q="SELECT * FROM ".cms_db_prefix()."module_quotetemplates";

		$result=cmsms()->GetDb()->Execute($q);
		if (!$result || ($result->NumRows()==0)) {
			return false;
		}
		$output=array();
		while($row=$result->FetchRow()) {
			$output[]=$row;
		}
		return $output;
	}

	function AddTemplate($name,$content) {
		
		$newid=cmsms()->GetDb()->GenID(cms_db_prefix()."module_quotetemplates_seq");
		$sql="INSERT INTO ".cms_db_prefix()."module_quotetemplates (id,name,content,isdefault) VALUES (?,?,?,0)";
		$values=array($newid,$name,$content);
		$result=cmsms()->GetDb()->Execute($sql,$values);
		return $newid;
	}

	function UpdateTemplate($id,$name,$content) {
		
		$q="UPDATE ".cms_db_prefix()."module_quotetemplates SET name=?,content=? WHERE id=?";
		$p=array($name,$content,$id);
		$result=cmsms()->GetDb()->Execute($q,$p);
		return true;
	}

	function SetDefaultTemplate($id) {
		
		$q="UPDATE ".cms_db_prefix()."module_quotetemplates SET isdefault='0'";
		$result=cmsms()->GetDb()->Execute($q);
		$q="UPDATE ".cms_db_prefix()."module_quotetemplates SET isdefault='1' WHERE id=?";
		$p=array($id);
		$result=cmsms()->GetDb()->Execute($q,$p);
		return true;
	}

	function DeleteTemplate($id) {
		
		$q="DELETE FROM ".cms_db_prefix()."module_quotetemplates WHERE id=?";
		$p=array($id);
		$result=cmsms()->GetDb()->Execute($q,array($id));
		return true;
	}

	function GetTemplate($id="",$name="") {
		
		$q="";$p=array();
		if ($id!="") {
			$q="SELECT * FROM ".cms_db_prefix()."module_quotetemplates WHERE id=?";
			$p=array($id);
		} else {
			$q="SELECT * FROM ".cms_db_prefix()."module_quotetemplates WHERE name=?";
			$p=array($name);
		}
		$result=cmsms()->GetDb()->Execute($q,$p);
		if (!$result || ($result->NumRows()==0)) {
			return false;
		}
		$row=$result->FetchRow();
		return $row;
	}

	function IncreaseExposure($quoteid) {
		$exposures=QuotesDAO::GetQuoteProp($quoteid,"exposures");
		if (!$exposures) {
			QuotesDAO::SetQuoteProp($quoteid,"exposures",1);
		} else {
			QuotesDAO::SetQuoteProp($quoteid,"exposures",$exposures+1);
		}
	}

	function SelectRandom($quotes) {
		$quote=array();
		$count=count($quotes);
		$picked=rand(0,$count-1);
		$quote=$quotes[$picked];
		QuotesDAO::IncreaseExposure($quote["id"]);
		return $quote;
	}

	function SelectEqual($quotes) {
		$hits=999999; $picked=array();
		foreach($quotes as $quote) {
			if ($quote["exposures"]<$hits) {
				$hits=$quote["exposures"];
				$picked=$quote;
			}
		}

		QuotesDAO::IncreaseExposure($picked["id"]);
		return $picked;
	}
	
	function SelectAll($quotes) {
	    return $quotes;
	}

	function SelectQuoteoftheday($quotes) {
		$quote=array();
		$lasttime=QuotesDAO::GetPreference("lastdaypick",-1);
		$lastid=QuotesDAO::GetPreference("lastidpick",-1);
		if ($lasttime==-1) {
				
			$quote=QuotesDAO::SelectRandom($quotes);
			QuotesDAO::SetPreference("lastdaypick",time());
			QuotesDAO::SetPreference("lastidpick",$quote["id"]);
		} else {
			if ($lasttime<(time()-(24*60*60))) {
				QuotesDAO::SetPreference("lastdaypick",-1);
				QuotesDAO::SetPreference("lastidpick",-1);
				$quote=QuotesDAO::SelectQuoteoftheday($quotes);
			} else {
				
				foreach ($quotes as $thisquote) {
					if ($thisquote["id"]==$lastid) {
						$quote=$thisquote;
						break;
					}
				}
				if (count($quote)==0) {
					//Quote must have been deleted

					cmsms()->SetPreference("lastdaypick",-1);
					cmsms()->SetPreference("lastidpick",-1);
					$quote=QuotesDAO::SelectQuoteoftheday($quotes);
				}

			}
		}
		return $quote;
	}


	function SelectQuotes($params) {
		$output['quotes']=array();
		$availablequotes=QuotesDAO::GetQuotes();
		$quotes=array();
		 
		if (isset($params["quote"]) && trim($params["quote"])!="") {
			$selectedquotes=explode(",",$params["quote"]);
			foreach($availablequotes as $quote) {
				foreach($selectedquotes as $textid) {
					if ($quote["textid"]==$textid) {
						$quotes[]=$quote;
					}
				}
			}
		} elseif (isset($params["group"]) && trim($params["group"])!="") {
			$selectedgroups=explode(",",$params["group"]);
			foreach($availablequotes as $quote) {
				foreach($selectedgroups as $group) {
					$group=QuotesDAO::GetGroup($group);
					if (QuotesDAO::GetConnection($quote["id"],$group["id"])) {
						$quotes[]=$quote;
					}
				}
			}
		} else {
			$quotes=$availablequotes;
		}
		unset($availablequotes); //free a little memory?
		 
		if (!empty($quotes)) {
			$pickedby="random";
			if (isset($params["pickedby"])) $pickedby=$params["pickedby"];
			$quotecount=count($quotes);
			$chosenindex=-1;
			switch($pickedby) {
				case "equal" : $output['quotes'][]=QuotesDAO::SelectEqual($quotes); break;
				case "day" : $output['quotes'][]=QuotesDAO::SelectQuoteoftheday($quotes); break;
				case "random" : $output['quotes'][]=QuotesDAO::SelectRandom($quotes); break;
				case "all" : $output['quotes']=QuotesDAO::SelectAll($quotes); break;
				default: $output['quotes'][]=QuotesDAO::SelectRandom($quotes);
			}
		}
		return $output;
	}
}

?>