<?php

class Rss {
	public $Count=0;
	public $Template;
	public $Items=array();
	public $CashPath="tmp/";
	public $inerror=false;

	function rss($url){
  	
		if(!$url) {$this->inerror="RSS: url don't exists";return false;}
		$content = file_get_contents($url);
		preg_match_all("/<item>(.+)<\/item>/Uis",$content,$Items1,PREG_SET_ORDER);
		foreach($Items1 as $indx=>$var){
			$this->Items[$indx]=$var[1];
		}
	}

	function parseItems(){
		$parsedarray=array();
		foreach($this->Items as $item) {
			preg_match_all("/<(title|link|description)>(.+)<\/(\\1)>/is",$item,$ParsedItem,PREG_SET_ORDER);
			$newquote=array("author"=>htmlspecialchars($ParsedItem[0][2],ENT_QUOTES),
											"content"=>strip_tags(html_entity_decode($ParsedItem[1][2])),
											"reference"=>strip_tags(htmlspecialchars($ParsedItem[2][2],ENT_QUOTES)));
			
			$parsedarray[]=$newquote;
		
		}
		return $parsedarray;
	}
}

?>