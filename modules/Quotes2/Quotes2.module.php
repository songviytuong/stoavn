<?php


class Quotes2 extends CMSModule {
	function GetName()  {
		return 'Quotes2';
	}

	function GetAdminSection() {
		return "content";
	}

	function GetFriendlyName()  {
		return $this->Lang("friendlyname");
	}

	function IsPluginModule() {
		return true;
	}

	function HasAdmin() {
		return true;
	}

	function GetVersion() {
		return '1.1.0';
	}

	function InstallPostMessage() {
		return $this->Lang("installpostmessage");
	}

	function UnInstallPostMessage() {
		return $this->Lang('uninstallpostmessage');
	}

	function VisibleToAdminUser() {
		return $this->CheckPermission('managequotes');
	}

	function GetAuthor() {
		return 'Kevin Danezis (Aka Bess) from the great work of Morten Poulsen';
	}

	function GetAuthorEmail() {
		return 'contact [at] furie [dot] be';
	}

	function GetChangeLog() {
		return $this->ProcessTemplate('changelog.tpl');
	}

	function GetHelp($lang='en_US') {
		return $this->Lang("help");
	}

	function InitializeFrontend() {
		$this->RegisterModulePlugin(true, false); 

		$this->RestrictUnknownParams();

		$this->SetParameterType('pickedby',CLEAN_STRING);
		$this->SetParameterType('template',CLEAN_STRING);
		$this->SetParameterType('group',CLEAN_STRING);
		$this->SetParameterType('quote',CLEAN_STRING);
	}

	function InitializeAdmin() {

		$this->CreateParameter('pickedby', 'random', $this->lang('parampickedbyhelp'));
		$this->CreateParameter('template', '', $this->lang('paramtemplatehelp'));
		$this->CreateParameter('group', '', $this->lang('paramgroupshelp'));
		$this->CreateParameter('quote', '', $this->lang('paramquoteshelp'));
	}

	function RegisterEvents() {

	}

	function _GetTypes() {
		return array(
		$this->Lang("plainquote")=>"1",
		$this->Lang("rssquote")=>"2",
		);
	}

	function _GetTypeName($type) {
		switch ($type) {
			case "1" : return $this->Lang("plainquote");
			case "2" : return $this->Lang("rssquote");
			default : return "unknown type";
		}
	}

}