
{if $itemcountQuotes > 0}

<div class="pageoptions">
	<p class="pageoptions">{$addformQuotes}</p>
</div>

<table cellspacing="0" class="pagetable">
	<thead>
		<tr>
			<th class="pageicon">&nbsp;</th>
			<th>{$quotes}</th>
			<th>{$quotetype}</th>
			<th>{$quoteexposures}</th>
			<th>{$quoteauthor}</th>
			<th>{$quotereference}</th>
			<th class="pageicon">{$actions}</th>
		</tr>
	</thead>
	<tbody>
{foreach from=$itemsQuotes item=entry}
{cycle values="row1,row2" assign=rowclass}
		<tr class="{$rowclass}" onmouseover="this.className='{$rowclass}hover';" onmouseout="this.className='{$rowclass}';">
			<td>&nbsp;</td>
			<td>{$entry->content}</td>
			<td>{$entry->quotetype}</td>
			<td>{$entry->exposure}</td>
			<td>{if isset($entry->author)}{$entry->author}{/if}</td>
			<td>{if isset($entry->reference)}{$entry->reference}{/if}</td>
			<td style="text-align:center">{$entry->editlink} {$entry->deletelink}</td>
		</tr>
		{*$entry|print_r*}
{/foreach}
	</tbody>
</table>
{if $nbPages > 1}
<div class='pager'>
	{for $page=1 to $nbPages}
		{if $page != 1} - {/if}
		<a href='{$urlPager}&amp;{$actionid}s={($page - 1) * $maxPerPage}'>{$page}</a>
	{/for}
</div>
{/if}

{else}
<h4>{$noquotestext}</h4>
{/if}

<div class="pageoptions">
	<p class="pageoptions">{$addformQuotes}</p>
</div>
