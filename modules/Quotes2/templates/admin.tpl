<style type="text/css">
	
.pager{
  text-align: center;
}

</style>

{tab_header name='quotes' label=$mod->Lang('quotestab') active=$currentTabQuotes}
{tab_header name='groups' label=$mod->Lang('grouptab') active=$currentTabGroups}
{if $accessTabTemplates}{tab_header name='templates' label=$mod->Lang('templatetab') active=$currentTabTemplates}{/if}
{if $accessTabShowcss}{tab_header name='css' label=$mod->Lang('csstab') active=$currentTabCss}{/if}
{tab_header name='settings' label=$mod->Lang('settings') active=$currentTabSettings}


	{tab_start name='quotes'}
	{include file='adminquotes.tpl'}


	{tab_start name='groups'}
	{include file='admingroups.tpl'}


	{if $accessTabTemplates}
		{tab_start name='templates'}
		{include file='admintemplates.tpl'}
	{/if}

	{if $accessTabShowcss}
		{tab_start name='css'}
		{*include file='adminedittext.tpl'*}
		The Stylesheet is now managed from the CmsMadeSimple Design system
	{/if}

	{tab_start name='settings'}
	{include file='adminsettings.tpl'}


{tab_end}

