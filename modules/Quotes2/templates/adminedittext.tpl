{$formstartText}
	{if isset($namehidden)}{$namehidden}{/if}
	{if isset($name) && $name != ''}
		<div class="pageoverflow">
		<p class="pagetext">{$name}</p>
		<p class="pageinput">{$nameinput}</p>
		</div>
	{/if}

	<div class="pageoverflow">
	<p class="pagetext">{$content}:</p>
	<p class="pageinput">{$contentinput}</p>
	<p class="pageinput">{if isset($templatehelp)}{$templatehelp}{/if}</p>
	</div>

	<div class="pageoverflow">
	<p class="pagetext">&nbsp;</p>
	<p class="pageinput">{$submitText}
		{if isset($applyText)}{$applyText}{/if}
		{if isset($resetText)}{$resetText}{/if}
		{if isset($backlinkText)}{$backlinkText}{/if}</p>
	</div>
{$formend}
