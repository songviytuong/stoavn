<ul>
	<li>
		<p><strong>1.1.0</strong></p>
		<p>Fix for cmsmadesimple 2.x series</p>
	</li>
	<li>
		<p><strong>1.0.3.2</strong></p>
		<p>fix warn in case of "no quote" (again)</p>
	</li>
	<li>
		<p><strong>1.0.3.1</strong></p>
		<p>fix warn in case of "no quote"</p>
	</li>
	<li>
		<p><strong>1.0.3</strong></p>
		<p>fix bug [#10026] Simple admin updates</p>
		<p>add feat [#9622] Paging + move "Add quote" function to top</p>
		<p>add a clear "Cancel" button on evey form</p>
		<p>refactor a lot of code</p>
		<p>remove all "echo" code</p>
	</li>
	<li>
		<p><strong>1.0.2</strong></p>
		<p>fix bug [#8956] Help page error</p>
		<p>fix bug [#8957] Filter by groupname problem - needs to be group ID</p>
		<p>add new option : pickedby='all'</p>
	</li>
	<li>
		<p><strong>1.0.1</strong></p>
		<p>fix bug [#8429] {literal}{Quotes2}{/literal} returns 0</p>
	</li>
	<li>
		<p><strong>1.0.0</strong></p>
		<p>Fork of the project, now Work with cmsmadesimple 1.10.x under the name of {literal}{Quotes2}{/literal}</p>
	</li>
	<li>
		<p><strong>0.1.2</strong></p>
		<p>Fixed strange little bug, potentially breaking qotd</p>
		<p>Removed a couple of debug echo's</p>
	</li>
	<li>
		<p><strong>0.1.1</strong></p>
		<p>Added rss-quotes support. Not perfect, but it's there.</p>
		<p>Fixed a bug rendereing deletion of quotes impossible</p>
	</li>
	<li>
		<strong>0.1.0</strong> - initial release
	</li>
</ul>