{if $itemcountTemplates > 0}
<table cellspacing="0" class="pagetable">
	<thead>
		<tr>
			<th class="pageicon">&nbsp;</th>
			<th>{$templates}</th>
			<th class="pageicon">{$actionsTemplates}</th>
		</tr>
	</thead>
	<tbody>
		{foreach from=$itemsTemplates item=entry}
		{cycle values="row1,row2" assign=rowclass}
				<tr class="{$rowclass}" onmouseover="this.className='{$rowclass}hover';" onmouseout="this.className='{$rowclass}';">
					<td>&nbsp;</td>
					<td>{$entry->name}</td>
					<td style="text-align:center">
						{if isset($entry->editlink)}{$entry->editlink}{/if}
						{if isset($entry->copylink)}{$entry->copylink}{/if}
						{if isset($entry->deletelink)}{$entry->deletelink}{/if}
					</td>
				</tr>
		{/foreach}
	</tbody>
</table>
{else}
<h4>{$notemplatestext}</h4>
{/if}

<div class="pageoptions">
	<p class="pageoptions">{$addlinkTemplates}</p>
</div>
