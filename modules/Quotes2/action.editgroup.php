<?php
if (!function_exists('cmsms')) exit;

if (!isset($params["todo"])) exit;


$inerror=false;

$params["tab"]="groups";
$groupid="";
if ($params["todo"]!="add" && !isset($params["groupid"])) {
	echo "Internal error"; exit;
} else {
	if (isset($params["groupid"])) $groupid=$params["groupid"];
}
if (isset($params["apply"])) $params["todo"]="apply";

switch ($params["todo"]) {
	case "delete" : {
		QuotesDAO::DeleteGroup($groupid);
		$params["module_message"]=$this->Lang("groupdeleted");		
		$this->Redirect($id, 'defaultadmin', $returnid,$params);
		break;
	}
	case "save" : {
		if (!isset($params["textid"])) {
			echo $this->ShowErrors($this->Lang("missingtextid"));
			$inerror=true;
			break;
		}
		$group=QuotesDAO::GetGroup($params["textid"]);
	  if ($group!=false && $group["id"]!=$groupid) {
			echo $this->ShowErrors($this->Lang("textidinuse"));
			$inerror=true;
			break;
		}
		if ($params["description"]=="") $params["decription"]=$params["textid"];
		QuotesDAO::AddGroup($params["textid"],$params["description"]);
		
		$params["module_message"]=$this->Lang("groupadded");
		$this->Redirect($id, 'defaultadmin', $returnid,$params);
		break;
	}
	case "apply" : {
		if (!isset($params["textid"])) {
			echo $this->ShowErrors($this->Lang("missingtextid"));
			break;
		}
		$group=QuotesDAO::GetGroup($params["textid"]);
		if ($group!=false && $group["id"]!=$groupid) {
			echo $this->ShowErrors($this->Lang("textidinuse"));
			$inerror=true;
			break;
		}
		if ($params["description"]=="") $params["decription"]=$params["textid"];
		QuotesDAO::UpdateGroup($groupid,$params["textid"],$params["description"]);
		$params["module_message"]=$this->Lang("groupupdated");
		$params["todo"]="edit";		
		unset($params["apply"]);
		$this->Redirect($id, 'edittemplate', $returnid,$params);
		break;
	}
	case "update" : {
		if (!isset($params["textid"])) {
			echo $this->ShowErrors($this->Lang("missingtextid"));
			break;
		}
		$group=QuotesDAO::GetGroup($params["textid"]);
		if ($group!=false && $group["id"]!=$groupid) {
			echo $this->ShowErrors($this->Lang("textidinuse"));
			$inerror=true;
			break;
		}
		if ($params["description"]=="") $params["decription"]=$params["textid"];
		QuotesDAO::UpdateGroup($groupid,$params["textid"],$params["description"]);
		$params["module_message"]=$this->Lang("groupupdated");
		$this->Redirect($id, 'defaultadmin', $returnid,$params);
		break;
	}
	case "add" : {
		$textid="";
	}
}

$name="";

$description="";

if ($params["todo"]=="edit") {
	$group=QuotesDAO::GetGroup("",$groupid);	
	$textid=$group["textid"];
	$description=$group["description"];
}
if (isset($params["textid"])) $textid=$params["textid"];
if (isset($params["description"])) $description=$params["description"];


$newtodo="";
if ($inerror) {
	$newtodo=$params["todo"];
} else {
  if ($params["todo"]=="edit") $newtodo="update";
  if ($params["todo"]=="add") $newtodo="save";
}
$smarty->assign('formstart',$this->CreateFormStart($id,"editgroup",$returnid,"post","",false,"",array("todo"=>$newtodo,"groupid"=>$groupid)));
$smarty->assign('formend',$this->CreateFormEnd());

$smarty->assign('textid',$this->lang("textid"));
$smarty->assign('textidinput',$this->CreateInputText($id,"textid",$textid,20,32));


$smarty->assign('description',$this->lang("groupdescription"));
$smarty->assign('descriptioninput',$this->CreateInputText($id,"description",$description,80,100));

if ($params["todo"]=="edit") {
	$smarty->assign('submit', $this->CreateInputSubmit($id,"submit",$this->Lang("savegroup")));
	$smarty->assign('apply', $this->CreateInputSubmit($id,"apply",$this->Lang("applychanges")));
} else {
	$smarty->assign('submit', $this->CreateInputSubmit($id,"submit",$this->Lang("addgroup")));
}

$smarty->assign('backlink', $this->CreateLink($id,"defaultadmin",$returnid,$this->Lang("cancel"),array("tab"=>"templates"),'',false,false,'class="pageback"'));

echo $this->ProcessTemplate('admineditgroup.tpl');

?>

