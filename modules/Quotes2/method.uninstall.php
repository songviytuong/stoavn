<?php
if (!function_exists('cmsms')) exit;

$db = cmsms()->GetDb();
$dict = NewDataDictionary($db);


$sqlarray = $dict->DropTableSQL(cms_db_prefix()."module_quotegroups");
$dict->ExecuteSQLArray($sqlarray);
$db->DropSequence(cms_db_prefix()."module_quotegroups_seq");

$sqlarray = $dict->DropTableSQL(cms_db_prefix()."module_quotes");
$dict->ExecuteSQLArray($sqlarray);
$db->DropSequence(cms_db_prefix()."module_quotes_seq");

$sqlarray = $dict->DropTableSQL(cms_db_prefix()."module_quotetemplates");
$dict->ExecuteSQLArray($sqlarray);
$db->DropSequence(cms_db_prefix()."module_quotetemplates_seq");


$sqlarray = $dict->DropTableSQL(cms_db_prefix()."module_quoteconnections");
$dict->ExecuteSQLArray($sqlarray);

$sqlarray = $dict->DropTableSQL(cms_db_prefix()."module_quoteprops");
$dict->ExecuteSQLArray($sqlarray);

$this->RemovePermission("managequotes");

$css = new CmsLayoutStylesheet;
$css = $css->load('Quotes Made Simple');
$css->delete();


?>