<?php
$lang['confirm_change_pwd'] = 'Waarschuwing: Na het aanpassen van het wachtwoord moet u deze aanpassen in de SimpleSiteMgr module!!';
$lang['friendlyname'] = 'Simple Site Info';
$lang['moddescription'] = 'SimpleSiteInfo verzorgt website informatie, te gebruiken voor de SimpleSiteMgr beheer module.';
$lang['create_new_pwd'] = 'Genereer een nieuw wachtwoord';
$lang['postinstall'] = 'De SimpleSiteInfo module is geïnstalleerd';
$lang['postuninstall'] = 'De SimpleSiteInfo module is verwijderd';
$lang['postupgrade'] = 'De SimpleSiteInfo module is bijgewerkt';
$lang['prompt_current_pwd'] = 'Huidig wachtwoord';
$lang['pwdchanged'] = 'Wachtwoord gewijzigd.<br /><b>Vergeet niet deze aan te passen in de SimpleSiteMgr module in de master website!</b>';
?>