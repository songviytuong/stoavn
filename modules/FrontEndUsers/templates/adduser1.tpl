<h3>{$title}</h3>
{if isset($message) }
  {if isset($error) }
    <p><font color="red">{$message}</font></p>
  {else}
    <p>{$message}</p>
  {/if}
{/if}
{$startform}
  <div class="c_full">
    <div class="grid_3 text-right">{$prompt_username}:</div>
    <div class="grid_9">
      {$input_username}
      {if isset($username_readonly)}&nbsp;{$mod->Lang('msg_username_readonly')}{/if}
    </div>
  </div>
  <div class="c_full">
    <div class="grid_3 text-right">{$prompt_password}:</div>
    <div class="grid_9">
       {$input_password}&nbsp;{$info_password|default:''}
    </div>
  </div>
  <div class="c_full">
    <div class="grid_3 text-right">{$prompt_repeatpassword}:</div>
    <div class="grid_9">
      {$input_repeatpassword}&nbsp;{$info_repeatpassword|default:''}
    </div>
  </div>
  <div class="c_full">
    <p class="grid_3 text-right">{$mod->Lang('prompt_disabled')}</p>
    <p class="grid_9">
      {cge_yesno_options prefix=$actionid name=input_disabled selected=$origparams.input_disabled|default:''}
    </p>
  </div>
  <div class="c_full">
    <p class="grid_3 text-right">{$mod->Lang('prompt_force_chsettings')}:</p>
    <p class="grid_9">
      {cge_yesno_options prefix=$actionid name=input_force_chsettings selected=$origparams.input_force_chsettings|default:''}
    </p>
    <div class="clearb"></div>
  </div>
  <div class="c_full">
    <p class="grid_3 text-right">{$mod->Lang('prompt_force_newpw')}:</p>
    <p class="grid_9">
      {cge_yesno_options prefix=$actionid name=input_force_newpw selected=$origparams.input_force_newpw|default:''}
    </p>
    <div class="clearb"></div>
  </div>
  <div class="c_full">
    <p class="grid_3 text-right">{$prompt_expires}:</p>
    <p class="grid_9">{html_select_date prefix=$expires_dateprefix time=$expiresdate start_year=2000 end_year=2037}</p>
  </div>

{if isset($itemcount) && $itemcount > 0}
<p class="pagetext">{$groupstitle}:</p>
<table cellspacing="0" class="pagetable cms_sortable tablesorter">
	<thead>
		<tr>
			<th width="5%">{$idtext}</th>
			<th>{$nametext}</th>
			<th>{$desctext}</th>
			<th class="pageicon {literal}{sorter: false}{/literal}">&nbsp;</th>
		</tr>
	</thead>
	<tbody>
{foreach from=$items item=entry}
		<tr class="{$entry->rowclass}">
			<td>{$entry->id}</td>
			<td>{$entry->name}</td>
			<td>{$entry->desc}</td>
			<td>{$entry->member}</td>
		</tr>
{/foreach}
	</tbody>
</table>
{/if}
  <div class="pageoverflow">
    <p class="pageinput">{$hidden|default:''}{$submit}{$cancel}</p>
  </div>
{$endform}
