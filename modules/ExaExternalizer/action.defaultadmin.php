<?php
if( !defined('CMS_VERSION') ) exit;
if( !$this->CheckPermission(ExaExternalizer::PERM_USE) ) return;









/* ************************************************************************** *\
    Test
\* ************************************************************************** */

$template_list = \ExaExternalizer\LayoutTemplates::get();









/* ************************************************************************** *\
    Traitement du formulaire du tableau de bord
\* ************************************************************************** */

if (!empty($params['form_dashboard'])):

    \ExaExternalizer\Status::change();

    $status = $this->GetPreference('status');
    if ($status):
        $this->SetMessage($this->Lang('status_changed_activated'));
    else:
        $this->SetMessage($this->Lang('status_changed_deactivated'));
    endif;

    $this->RedirectToAdminTab('dashboard');
   
endif;









/* ************************************************************************** *\
   Traitement du formulaire du tableau de bord - Rafraîchissement
\* ************************************************************************** */

if (!empty($params['form_dashboard_refresh'])):

   \ExaExternalizer\Base::export();
   
   $this->SetMessage($this->Lang('status_changed_refreshed'));
   
   $this->RedirectToAdminTab('dashboard');
   
endif;









/* ************************************************************************** *\
   Traitement du formulaire des paramètres
\* ************************************************************************** */

if (!empty($params['form_setting'])):

   if (isset($params['cache_folder'])):
   
      // Si le chemin commence par un "/" alors l'enlever
      if (startswith($params['cache_folder'], DIRECTORY_SEPARATOR)):
         $params['cache_folder'] = substr($params['cache_folder'], 1);
      endif;
      
      // Si le chemin termine par un "/" alors l'enlever
      if (endswith($params['cache_folder'], DIRECTORY_SEPARATOR)):
         $params['cache_folder'] = substr($params['cache_folder'], -1, 1);
      endif;
   
      $this->SetPreference("cache_folder", $params['cache_folder']);
      
   endif;
   
   if (isset($params['timeout'])):
      $this->SetPreference("timeout", $params['timeout']);
   endif;
   
   if (isset($params['chmod'])):
      $this->SetPreference("chmod", $params['chmod']);
   endif;
   
   if (isset($params['stylesheet_extension'])):
      $this->SetPreference("stylesheet_extension", $params['stylesheet_extension']);
   endif;
   
   if (isset($params['template_extension'])):
      $this->SetPreference("template_extension", $params['template_extension']);
   endif;
   
   if (isset($params['udt_extension'])):
      $this->SetPreference("udt_extension", $params['udt_extension']);
   endif;
   
   $this->SetMessage($this->Lang('setting_changed'));
   $this->RedirectToAdminTab('setting');
   
endif;









/* ************************************************************************** *\
   Récupération des variables
\* ************************************************************************** */

$setting['status'] = $this->GetPreference("status");
$setting['tmp_path'] = \ExaExternalizer\Cache::getTmpPath();
$setting['cache_folder'] = $this->GetPreference("cache_folder");
$setting['timeout'] = $this->GetPreference("timeout");
$setting['chmod'] = $this->GetPreference("chmod");
$setting['stylesheet_extension'] = $this->GetPreference("stylesheet_extension");
$setting['template_extension'] = $this->GetPreference("template_extension");
$setting['udt_extension'] = $this->GetPreference("udt_extension");
$setting['time_left'] = \ExaExternalizer\TimeOut::getTimeLeft();

$smarty->assign('setting', $setting);









/* ************************************************************************** *\
   Affichage du template
\* ************************************************************************** */

$tpl = $smarty->CreateTemplate($this->GetTemplateResource('defaultadmin.tpl'),null,null,$smarty);
$tpl->display();









?>