<?php
$lang['friendlyname'] = "ExaExternalizer";
$lang['admindescription'] = "This module allows you to use a text editor to
    edit templates, stylesheets, UDTs, and some
    SitePrefs (Site down message, Metadata, Default Page Content, Page
    Metadata)! It exports them to temporary files on the server and watches
    these files for modifications which are automatically imported into
    the database and take immediate effect.";
$lang['ask_uninstall'] = "Are you sure you want to uninstall the ExaExternalizer module?";


$lang['dashboard_tab_header'] = "Dashboard";
$lang['status'] = "Status";
$lang['status_activated'] = "Activated";
$lang['status_deactivated'] = "Deactivated";
$lang['time_left'] = "Time left";
$lang['status_activate'] = "Activate";
$lang['status_deactivate'] = "Deactivate";
$lang['status_refresh'] = "Refresh";
$lang['status_changed_activated'] = "ExaExternalizer activated!";
$lang['status_changed_deactivated'] = "ExaExternalizer deactivated!";
$lang['status_changed_refreshed'] = "ExaExternalizer refreshed!";

$lang['setting_tab_header'] = "Settings";
$lang['cache_folder'] = "Cache folder";
$lang['timeout'] = "Timeout";
$lang['minute'] = "minute";
$lang['minutes'] = "minutes";
$lang['chmod'] = "CHMOD";
$lang['stylesheet_extension'] = "CSS extension";
$lang['template_extension'] = "Tempalte extension";
$lang['udt_extension'] = "UDT extension";
$lang['save'] = "Save";
$lang['setting_changed'] = "Settings changed";

$lang['setting_tab_donation'] = "Donate";
$lang['donation_introduction'] = "Ce module est distribué gratuitement et
    continera à l'être.<br/>Cependant, si vous souhaitez effectuer un don du
    montant de votre choix parce que vous estimez que ce module le mérite, je
    vous laisse faire :-)";
    
$lang['sitepref_edited'] = "Preference edited";
$lang['userplugin_edited'] = "UDT edited";

?>