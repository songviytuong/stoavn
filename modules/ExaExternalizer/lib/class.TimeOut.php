<?php

namespace ExaExternalizer;

class TimeOut
{
   
   
   
   
   
   
   
   
   
/* ************************************************************************** *\
    Retourne le temps restant avant désactivation
\* ************************************************************************** */
public static function getTimeLeft()
{
    
    // Variables
    $mod = \cms_utils::get_module('ExaExternalizer');
    $timeout = $mod->GetPreference('timeout');
    $time_left = 0;
    $now_timestamp = time();
    $cache_path = \ExaExternalizer\Cache::getPath();

    // Récupération de la date du dossier de cache
    $cache_path_timestamp = @filemtime($cache_path);

    // Calcul
    $time_left = round($timeout - (($now_timestamp - $cache_path_timestamp) / 60), 0);

    return $time_left;
    
}









/* ************************************************************************** *\
   Remise à zéro du timeout
\* ************************************************************************** */

public static function reset()
{
   
    // Variables
    $cache_path = \ExaExternalizer\Cache::getPath();

    // Réinitialisation
    touch($cache_path);

    return true;
	
}









}?>