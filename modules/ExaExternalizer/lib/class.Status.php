<?php

namespace ExaExternalizer;

class Status
{
   
   
   
   
   
   
   
   
   
/* ************************************************************************** *\
    Changement d'�tat
\* ************************************************************************** */
public static function change()
{
    
    $mod = \cms_utils::get_module('ExaExternalizer');
    $status = $mod->GetPreference('status');

    if (!$status):
        self::activate();
    else:
        self::deactivate();
    endif;
    
}









/* ************************************************************************** *\
   Activation du module
\* ************************************************************************** */
public static function activate()
{
   
    // Activation
    $mod = \cms_utils::get_module('ExaExternalizer');
    $status = $mod->SetPreference('status', true);

    // Cr�ation du dossier de cache
    $cache_folder = $mod->GetPreference('cache_folder');
    \ExaExternalizer\Cache::createFolder();

    // Exportation
    \ExaExternalizer\Base::export();
   
}









/* ************************************************************************** *\
   D�sactivation du module
\* ************************************************************************** */
public static function deactivate()
{

    // D�sactivation
    $mod = \cms_utils::get_module('ExaExternalizer');
    $mod->SetPreference("status", false);

    // Suppression du dossier d'exportation
    $result = \ExaExternalizer\Cache::delete();

    return $result;

}









}?>