<?php

namespace ExaExternalizer;

class LayoutStylesheets
{
   
   
   
   
   
   
   
   
   
/* ************************************************************************** *\
    Variables
\* ************************************************************************** */

public static $fields = [
    'id',
    'content',
    'modified'
    ];









/* ************************************************************************** *\
    Get
\* ************************************************************************** */

public static function get($options=[]) {
    
    $db = \cms_utils::get_db();
    
    $return = new \stdClass();
    $return->result = false;
    $return->message = '';
    $return->count = null;
    $return->record = [];
    
    // Default options
    $default = [
        'fields' => '*',
        'where' => null,
        'order' => null,
        'limit' => null,
        'offset' => null
        ];
      
    // Filter options
    foreach ($options as $key => $val):
        if (!array_key_exists($key, $default)):
            unset($options[$key]);
        endif;
    endforeach;
    $options = array_merge($default, $options);
    
    // Query
    $sql = "SELECT {$options['fields']} FROM " . CMS_DB_PREFIX . "layout_stylesheets";
    $sql .= (!empty($options['where'])) ? " WHERE {$options['where']}" : '';
    $sql .= (!empty($options['order'])) ? " ORDER BY {$options['order']}" : '';
    $sql .= (!empty($options['limit'])) ? " LIMIT {$options['limit']}" : '';
    $sql .= (!empty($options['offset'])) ? " OFFSET {$options['offset']}" : '';
    
    $result = $db->Execute($sql);
    
    if (!$result):
        $return->message = $db->ErrorMsg();
        return $return;
    endif;
    
    $return->count = $result->RecordCount();
    $return->record = $result->GetAll();
    $return->result = true;
    
    return $return;
    
}









/* ************************************************************************** *\
    Set
\* ************************************************************************** */

public static function set($options) {
    
    // Pr�paration des options
    foreach ($options as $key => $val):
        
        // Filtrage
        if (!in_array($key, self::$fields)):
            unset($options[$key]);
            continue;
        endif;

        // Optimisation
        if ($val == ''):
            $options[$key] = NULL;
        endif;
        
        switch ($key):
            case 'id':
                $options[$key] = (int) $val;
                break;
            case 'content':
            case 'modified':
                $options[$key] = trim($val);
                break;
        endswitch;
        
    endforeach;
    
    // Mise � jour
    if (empty($options['id']) OR $options['id'] == 0):
        return false;
    endif;
    
    $return = self::update($options);
    
    return $return;

}









/* ************************************************************************** *\
    Update
\* ************************************************************************** */

protected static function update($options) {
    
    $mod = \cms_utils::get_module('ExaExternalizer');
    $db = \cms_utils::get_db();
    
    $return = new \stdClass();
    $return->result = false;
    $return->message = '';
    
    $timeStamp = time();
    $timeNow = date("Y-m-d H:i:s",$timeStamp);
    
    // Pr�paration des valeurs
    $options['modified'] = (empty($options['modified'])) ? $timeNow : $options['modified'];

    // Pr�paration du SET
    $set = '';
    foreach ($options as $key => $val):
        $set .= $key . ' = ?';
        end($options);
        if ($key != key($options)):
            $set .= ', ';
        endif;
    endforeach;
    
    // Requ�te
    $query = "
        UPDATE " . CMS_DB_PREFIX . "layout_stylesheets
        SET {$set}
        WHERE id = {$options['id']}
        ";
    
    $result = $db->Execute($query, $options);
    
    if (!$result):
        $return->message = $db->ErrorMsg();
        return $return;
    endif;
    
    $return->result = true;
    $return->message = $mod->Lang('stylesheet_edited');
    
    return $return;
    
}









/* ************************************************************************** *\
    Exportation
\* ************************************************************************** */

public static function export() {
    
    // Variables
    $folder = '_CSS';
    $mod = \cms_utils::get_module('ExaExternalizer');
    $stylesheet_extension = $mod->GetPreference('stylesheet_extension');

    // Cr�ation du dossier d'exportation
    \ExaExternalizer\FileSystem::createFolder($folder);
    
    $result = self::get();
    if ($result->count > 0):

        $item_record = $result->record;

        foreach ($item_record as &$item):

            \ExaExternalizer\FileSystem::createFile($folder, $item['name'] . "__" . $item['id'], $stylesheet_extension, $item['content'], $item['modified']);

        endforeach;

    endif;
    
}









/* ************************************************************************** *\
   Importer les feuilles de styles
\* ************************************************************************** */
public static function import()
{
    
    $return = new \stdClass();
    $return->result = false;
    $return->message = '';
    
    // Variables
    $cache_path = \ExaExternalizer\Cache::getPath();
    $mod = \cms_utils::get_module('ExaExternalizer');
    $stylesheet_extension = $mod->GetPreference('stylesheet_extension');
    $folder = '_CSS';
    $folder_path = cms_join_path($cache_path, $folder);


    // Parcourir chaque feuille de styles
    $result = self::get();
    if (count($result->record) > 0):

        $item_record = $result->record;

        foreach ($item_record as $item):

            $filename = \ExaExternalizer\FileSystem::escapeFilename($item['name']);
            $filename = cms_join_path($folder_path, $filename . '__' . $item['id'] . '.' . $stylesheet_extension);

            // Si le fichier n'existe pas
            if (!file_exists($filename)):
                continue;
            endif;

            // Si le fichier n'a pas �t� modifi�
            $filetime = @filemtime($filename);
            if ($filetime <= $item['modified']):
                continue;
            endif;

            // Lecture du contenu
            $content = \ExaExternalizer\FileSystem::readFile($folder, $item['name'] . '__' . $item['id'], $stylesheet_extension);

            // Sauvegarde du contenu
            $result = self::set([
                'id' => $item['id'],
                'content' => $content,
                'modified' => $filetime
                ]);

            // Mise � z�ro du timeout
            \ExaExternalizer\TimeOut::reset();

        endforeach;

    endif;
    
    $return->result = true;
    
    return $return;
   
}









}?>