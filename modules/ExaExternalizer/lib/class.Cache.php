<?php

namespace ExaExternalizer;

class Cache
{
   
   
   
   
   
   
   
   
   
/* ************************************************************************** *\
    Retourne le chemin du dossier tmp
\* ************************************************************************** */
public static function getTmpPath()
{

    $tmp_path = cms_join_path(CMS_ROOT_PATH, 'tmp');
    return $tmp_path;
    
}









/* ************************************************************************** *\
    Retourne le chemin du dossier cache
\* ************************************************************************** */
public static function getPath()
{
    
    $mod = \cms_utils::get_module('ExaExternalizer');
    
    $cache_folder = $mod->GetPreference("cache_folder");
    $cache_path = cms_join_path(self::getTmpPath(), $cache_folder);
    
    return $cache_path;
    
}









/* ************************************************************************** *\
    Crée le dossier de cache
\* ************************************************************************** */
public static function createFolder()
{

    $result = \ExaExternalizer\FileSystem::createFolder();
    
    return $result;
    
}









/* ************************************************************************** *\
    Suppression du cache
\* ************************************************************************** */

public static function delete()
{
    
    $result = \ExaExternalizer\FileSystem::deleteFolder();
    
    return $result;
    
}









}?>