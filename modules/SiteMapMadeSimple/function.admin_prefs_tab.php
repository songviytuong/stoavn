<?php
#-------------------------------------------------------------------------
# Module: SiteMapMadeSimple - A module to easily create a sitemap for
#         google maps, and other search engines.
#
# Version: 1.0, calguy1000 <calguy1000@hotmail.com>
#
#-------------------------------------------------------------------------
# CMS - CMS Made Simple is (c) 2005 by Ted Kulp (wishy@cmsmadesimple.org)
# This project's homepage is: http://www.cmsmadesimple.org
# The module's homepage is: http://dev.cmsmadesimple.org/projects/skeleton/
#
#-------------------------------------------------------------------------
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# However, as a special exception to the GPL, this software is distributed
# as an addon module to CMS Made Simple.  You may not use this software
# in any Non GPL version of CMS Made simple, or in any version of CMS
# Made simple that does not indicate clearly and obviously in its admin 
# section that the site was built with CMS Made simple.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
# Or read it online: http://www.gnu.org/licenses/licenses.html#GPL
#
#-------------------------------------------------------------------------

$smarty->assign('formstart',$this->CreateFormStart($id,'admin_saveprefs',$returnid));
$smarty->assign('formend',$this->CreateFormEnd());
$smarty->assign('submit',$this->CreateInputSubmit($id,'submit',$this->Lang('submit')));

$smarty->assign('prompt_allow_hidden',$this->Lang('allow_hidden_pages'));
$smarty->assign('input_allow_hidden', $this->CreateInputYesNoDropdown($id,'allow_hidden',$this->GetPreference('allow_hidden',0)));

$options[$this->Lang('automatic')] = 'auto';
$options[$this->lang('never')] = 'never';
$options[$this->lang('yearly')] = 'yearly';
$options[$this->lang('monthly')] = 'monthly';
$options[$this->lang('weekly')] = 'weekly';
$options[$this->lang('daily')] = 'daily';
$options[$this->lang('hourly')] = 'hourly';
$options[$this->lang('always')] = 'always';

$smarty->assign('priority_field',$this->GetPreference('priority_field',''));
$smarty->assign('prompt_change_freq',$this->Lang('change_frequency'));
$smarty->assign('input_change_freq',$this->CreateInputDropdown($id,'change_frequency',$options,-1,$this->GetPreference('change_frequency')));

$smarty->assign('prompt_static_sitemap',$this->Lang('static_sitemap'));
$smarty->assign('input_static_sitemap',	$this->CreateInputYesNoDropdown($id,'static_sitemap', $this->GetPreference('static_sitemap',0)));

$smarty->assign('prompt_dynamic_update',$this->Lang('dynamic_update'));
$smarty->assign('input_dynamic_update',	$this->CreateInputYesNoDropdown($id,'dynamic_update',$this->GetPreference('dynamic_update',1)));

echo $this->ProcessTemplate('prefs_tab.tpl');
// EOF
?>
