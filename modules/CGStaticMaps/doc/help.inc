<h3>What does this do?</h3>
<p>This module provides a simple and easy way to create static google maps for CMSMS web pages.</p>
<h3>Features</h3>
<ul>
  <li>Conveniently generate static google maps with a smarty plugin.</li>
  <li>Cache the generated map images on the server.</li>
  <li>Automatically embed smaller maps to redue html requests.</li>
  <li>Supports 3 different marker icon sizes.</li>
  <li>Automatically detects the current language.</li>
  <li>Supports multiple markers per map.</li>
  <li>Supports specifying a center location, or automatically determining one.</li>
  <li>Supports location names or latitude and longitude.</li>
</ul>
<h3>FAQ</h3>
<ul>
  <li>Why cache the map images?
    <p>There are two reasons why the generated map images are cached.  a: performance (it is faster to load a static image than any dynamic one... even if it comes from google).  b: The google static maps API has a quota. Caching the images means that repeated visitors will not effect the quote.</p>
  </li>
  <li>How long is the image cached.
    <p>Images are cached for 30 days, or until deleted from the CMSMS cache directory.</p>
  </li>
  <li>What is the default size?
    <p>The default size of the map is 400x400.</p>
  </li>
  <li>What is the default format?
    <p>The default format is PNG.</p>
  </li>
  <li>There can only be a maximum of 26 markers on each map.</li>
  <li>Polylines are not supported at this time.</li>
  <li>This module does not generate pretty URLS for SEO purposes.</li>
  <li>This module does not have an admin interface.</li>
</ul>
<h3>Parameters</h3>
<ul>
  <li>Map Parameters
    <ul>
    <li>address <em>(optional, string)</em>
        <p>Specify a single marker by it's address (or latitude and longitude).  This parameter will erase any previously set markers.</p>
	<p>Example:  <code>{CGStaticMaps address='Paris FR'}</code>
    </li>
    <li>marker0..25 <em>(optional, string)</em>
        <p>Add up to 26 different markers.</p>
	<p>Example:  <code>{CGStaticMaps marker0='Paris FR' marker1='Amsterdam NL'}</code></p>
    </li>
    <li>maptype <em>(roadmap|satellite|hybrid|terrain)</em>
        <p>Set the map type.  The default is &quot;roadmap&quot;</p>
	<p>Example: {CGStaticMaps address='Paris FR' maptype=satellite}</p>
    </li>
    <li>center <em>(optional, string)</em>
       <p>Set the center location for the map.  Accepts a location or a latitude and logitude.</p>
       <p>Example: {CGStaticMaps center='Paris FR'}</p>
    </li>
    <li>zoom <em>(optional int between 0 and 21)
       <p>Set the zoom level.  If not specified the system will automatically calculate a zoom level that encompasses all markers.</p>
       <p>Example: {CGStaticMaps center='Paris FR' zoom=12}</p>
    </li>
    <li>markersize <em>(small|mid|tiny)</em>
       <p>Allows changing the size of map markers.  Some map marker sizes may not support labels.</p>
       <p>Example: {CGStaticMaps center='Paris FR' zoom=12 markersize=tiny}</p>
    </li>
    <li>markercolor <em>(optional, color string)</em>
       <p>Allows changing the color of all markers for the map.</p>
       <p>Example: {CGStaticMaps center='Paris FR' zoom=12 markercolor='#efefef'}</p>
    </li>
    <li>size <em>(string)
       <p>Specify the map size.  Note, this can also be done with the width and height parameters.</p>
       <p>Example: {CGStaticMaps center='Paris FR' zoom=12 markercolor='#efefef' size='250x250'}</p>
    </li>
    <li>format <em>(PNG|JPG|GIF)</em>
       <p>Specify the image format.  The default value is PNG.</p>
    </li>
    <li>language <em>(string)</em>
       <p>Specify the language for the image labels.  by default the current cmsms language is used.</p>
       <p>Example: {CGStaticMaps center='Rome Italy' language=fr}</p>
    </li>
    </ul>
  </li>

  <li>Img Tag Parameters
    <ul>
    <li>id <em>(optional string)</em> - The id attribute for the img tag.</li>
    <li>class <em>(optional string)</em> - The class attribute for the img tag.</li>
    <li>alt <em>(optional string)</em> - The alt attribute for the img tag.  If not specified one will be genrated by default so that the img tag validates.</li>
    <li>width <em>(optional int)</em> - The width for the image tag.  This parameter can be used to specify the size for the generated image as well.</li>
    <li>height <em>(optional int)</em> - The height for the image tag.  This parameter can be used to specify the size for the generated image as well.</li>
    <li>noembed <em>(optional bool)</em> - Do not embed images.</li>
    </ul>
  </li>
</ul>

<h3>Config Variables</h3>
<p>This module will use the value of the cgsm_noembed variable in the config.php file to determine the default value for the noembed argument.</p>