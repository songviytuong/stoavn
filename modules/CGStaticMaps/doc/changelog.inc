<ul>
	<li>version 1.0.0 - Initial Release</li>
	<li>version 1.0.1 - Minor bug fix
	<ul>
	<li>Register the address parameter, so the data in the help works.</li>
	</ul>
	</li>
	<li>version 1.0.2
	<ul>
	<li>No longer use tmp/cache for file storage.</li>
	</ul>
	</li>
</ul>
