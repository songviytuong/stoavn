<?php

namespace CGStaticMaps;

class map_generator
{
    const STATIC_URL = "https://maps.googleapis.com/maps/api/staticmap";
    const FMT_PNG = 'PNG';
    const FMT_JPG = 'JPG';
    const FMT_GIF = 'GIF';
    const TYPE_ROADMAP = 'roadmap';
    const TYPE_SATELLITE = 'satellite';
    const TYPE_HYBRID = 'hybrid';
    const TYPE_TERRAIN = 'terrain';

    private $_data = array('client'=>null,'signature'=>null,'size'=>'400x400','scale'=>null,'format'=>'PNG','maptype'=>'roadmap','language'=>null,'region'=>null,'center'=>null,'zoom'=>null,'sensor'=>false);
    private $_markers;
    private $_markerstyles;
    private $_visible;
    private $_markersize;
    private $_markercolor;

    public function __construct($opts = null)
    {
        // todo: set default language.
        if( !is_array($opts) || !count($opts) ) return;
        foreach( $opts as $key => $val ) {
            if( !$val ) continue;
            switch( $key ) {
            case 'client':
            case 'signature':
                $this->_data[$key] = trim($val);
                break;

            case 'size':
                if( is_string($val) ) {
                    list($w,$h) = explode('x',$val);
                    $this->set_size($w,$h);
                }
                else if( is_array($val) && count($val) == 2 ) {
                    $this->set_size($val[0],$val[1]);
                }
                else {
                    throw new \RuntimeException("$val is a invalid value for size in ".__CLASS__);
                }
                break;

            case 'markersize':
                $this->set_markersize($val);
                break;

            case 'markercolor':
                $this->set_markercolor($val);
                break;

            case 'scale':
                $this->set_scale($val);
                break;

            case 'format':
                $this->set_format($val);
                break;

            case 'maptype':
                $this->set_maptype($val);
                break;

            case 'language':
                $this->set_language($val);
                break;

            case 'region':
                $this->set_region($val);
                break;

            case 'center':
                $this->set_center($val);
                break;

            case 'zoom':
                $this->set_zoom($val);
                break;

            case 'visible':
                $this->set_visible($val);
                break;

            case 'sensor':
                $this->set_sensor($val);
                break;

            case 'style':
                $this->add_style($val);
                break;

            case 'styles':
                if( !is_array($val) || !count($val) ) throw new \RuntimeException("$val is an invalid value for styles option in ".__CLASS__);
                foreach( $val as $one ) {
                    $this->add_style($one);
                }
                break;

            case 'marker':
                // one marker
                $this->add_marker($val);
                break;

            case 'markers':
                // array of markers
                if( !is_array($val) || !count($val) ) throw new \RuntimeException("$val is an invalid value for markers option in ".__CLASS__);
                foreach( $val as $one ) {
                    $this->add_marker($one);
                }
                break;
            }
        }
    }

    public function set_client($client)
    {
        $client = trim($client);
        $this->_data['client'] = $client;
    }

    public function set_signature($sig)
    {
        $sig = trim($sig);
        $this->_data['signature'] = $sig;
    }

    public function set_size($w,$h)
    {
        $w = (int) $w;
        $h = (int) $h;
        if( $w < 10 || $h < 10 ) throw new \RuntimeException("Invalid values passed to ".__METHOD__);
        $this->_data['size'] = "{$w}x{$h}";
    }

    public function set_scale($scale = 1)
    {
        $scale = (int) $scale;
        if( !in_array($scale,array(1,2,4)) ) throw new \RuntimeException("Invalid value for scale (possible values are 1,2,4)");
        $this->_data['scale'] = $scale;
    }

    public function set_format($fmt)
    {
        switch( $fmt ) {
        case self::FMT_PNG:
        case self::FMT_GIF:
        case self::FMT_JPG:
            $this->_data['format'] = $fmt;
            break;
        default:
            throw new \RuntimeException("$fmt is an invalid format for ".__CLASS__);
        }
    }

    public function set_maptype($type)
    {
        switch( $type ) {
        case self::TYPE_ROADMAP:
        case self::TYPE_SATELLITE:
        case self::TYPE_HYBRID:
        case self::TYPE_TERRAIN:
            $this->_data['maptype'] = $type;
            break;
        default:
            throw  new \RuntimeException("$type is an invalid map type for ".__CLASS__);
        }
    }

    public function set_language($lang)
    {
        $lang = (string) $lang;
        if( strlen($lang) != 2 ) throw new \RuntimeException("$lang is an invalid language for ".__CLASS__);
        $this->_data['language'] = $lang;
    }

    public function set_region($rgn)
    {
        $rgn = (string) $rgn;
        if( strlen($rgn) != 2 ) throw new \RuntimeException("$rgn is an invalid region string");
        $this->_data['region'] = $rgn;
    }

    protected function to_location($loc)
    {
        // could be a place name, or a lat/long array pair.
        if( is_string($loc) ) {
            if( strlen($loc) <= 4 ) throw new \RuntimeException("$loc is not a valid location string");
            $tmp = explode(',',$loc);
            if( count($tmp) === 2 ) {
                if( is_float($tmp[0]) && !is_float($tmp[1]) ) {
                    $loc = $tmp;
                } else {
                    return $loc;
                }
            } else {
                return $loc;
            }
        }

        if( !is_array($loc) || count($loc) != 2 ) throw new \RuntimeException("$loc is not a valid location");
        if( !is_float($loc[0]) || !is_float($loc[1]) ) throw new \RuntimeException("$loc is not a valid location");
        if( $loc[0] < -180 || $loc[0] > 180 ) throw new \RuntimeException("$loc is not a valid location");
        if( $loc[1] < -180 || $loc[1] > 180 ) throw new \RuntimeException("$loc is not a valid location");
        return $location;
    }

    public function set_center($location)
    {
        $this->_data['center'] = $this->to_location($location);
    }

    public function get_center()
    {
        // if center is set, use it... if markers are set, calculate a center
    }

    public function set_zoom($zoom)
    {
        $zoom = (int) $zoom;
        if( $zoom < 0 || $zoom > 21 ) throw new \RuntimeException("$zoom is an invalid zoom value");
        $this->_data['zoom'] = $zoom;
    }

    public function set_markersize($str)
    {
        if( !$str ) return;
        switch( strtolower($str) ) {
        case 'tiny':
        case 't':
            $this->_markersize = 'tiny';
            break;
        case 'mid':
        case 'm':
            $this->_markersize = 'mid';
            break;
        case 'small':
        case 's':
            $this->_markersize = 'small';
            break;
        default:
            throw new \RuntimeException("$str is an invalid marker size value");
        }
    }

    public function set_markercolor($str)
    {
        if( !$str ) return;
        $str = strtolower($str);
        if( startswith($str,'#') && strlen($str) == 7 ) {
            $str = '0x'.substr($str,1);
        }
        if( !startswith($str,'0x') ) throw new \RuntimeException("Invalid value for marker color.  must be a hex value i.e 0xFF99CC or a 7 character css color, i.e: '#ff99cc");
        $this->_markercolor = $str;
    }

    public function set_sensor($sensor = true)
    {
        $sensor = (bool) $sensor;
        $this->_data['sensor'] = $sensor;
    }

    public function set_visible($location)
    {
        $this->_visible = $this->to_location($location);
    }

    public function add_marker($location)
    {
        // allow only 26 markers.
        $location = $this->to_location($location);
        if( count($this->_markers) >= 26 ) throw new \RuntimeException("A static map can have only 30 markers (max)");
        $this->_markers[] = $location;
    }

    protected function find_center()
    {
        if( !count($this->_markers) ) {
            if( $this->_visible ) return $this->_visible;
        }
        if( count($this->_markers) == 1 ) return; // have exactly one marker... no need for center.

        $min_lat = $min_long = 999;
        $max_lat = $max_long = -999;
        $have_ll = false;
        foreach( $this->_markers as $marker ) {
            if( is_array($marker) && count($marker) == 2 ) {
                $have_ll = true;
                $min_lat = min($min_lat,$marker[0]);
                $min_long = min($min_lat,$marker[1]);
                $max_lat = max($max_lat,$marker[0]);
                $max_long = max($min_lat,$marker[1]);
            }
        }
        if( !$have_ll ) return;

        $center_lat = $min_lat + ($max_lat - $min_lat) / 2.0;
        $center_long = $min_long + ($max_long - $min_long) / 2.0;
        return array($center_lat,$center_long);
    }

    protected function encode_location($loc)
    {
        if( is_string($loc) ) return $loc;
        if( is_array($loc) && count($loc) == 2 ) return implode(',',$loc);
    }

    public function encode_marker($marker,$idx) {
        // todo: allow for marker size and color.
        $idx = $idx % 26; // only 26 labels.
        $label = chr(ord('A')+$idx);
        $out = null;
        if( $this->_markersize ) $out .= 'size:'.$this->_markersize.'|';
        if( $this->_markercolor ) $out .= 'color:'.$this->_markercolor.'|';
        if( $this->_markersize == '' || $this->_markersize != 'tiny' ) $out .= "label:$label|";
        $out .= $this->encode_location($marker);
        return $out;
    }

    public function get_url()
    {
        // make sure we ahve at least one marker OR a center
        if( count($this->_markers) == 0 ) {
            if( $this->_data['center'] == null && $this->_visible == null ) throw new \RuntimeException("Please specify a center location, a visible location OR at least one marker.");
        }

        $parms = array();
        foreach( $this->_data as $key => $val ) {
            if( !strlen($val) ) continue;
            $parms[$key] = $val;
        }

        // if we don't have a center, calculate one
        if( !isset($parms['center']) && !$this->_visible ) {
            $tmp = $this->find_center();
            if( $tmp ) $parms['center'] = $this->encode_location($tmp);
        }

        // add visible
        if( $this->_visible ) {
            $parms['visible'] = $this->encode_location($this->_visible);
        }

        // add markers
        if( count($this->_markers) ) {
            $parms['markers'] = array();
            $idx=0;
            foreach( $this->_markers as $marker ) {
                $parms['markers'][] = $this->encode_marker($marker,$idx++);
            }
        }

        // build the url
        $url = self::STATIC_URL;
        $idx = 0;
        foreach( $parms as $key => $val ) {
            $sep = '&';
            if( $idx == 0 ) $sep = '?';
            $idx++;

            if( !$val ) continue;
            if( is_array($val) ) {
                foreach( $val as $key2 => $val2 ) {
                    $url .= $sep.$key.'='.$val2;
                }
            }
            else {
                $url .= $sep.$key.'='.$val;
            }
        }

        $url = str_replace(' ','%20',$url);
        if( strlen($url) > 2048 ) throw new \RuntimeException("Generated URL is too long for the google static maps API");
        return $url;
    }
} // end of class
?>