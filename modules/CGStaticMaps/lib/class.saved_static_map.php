<?php

namespace CGStaticMaps;

class saved_static_map extends map_generator
{
    private $_lifetime;
    private $_crf = null;

    public function __construct($opts)
    {
        $this->_lifetime = 24 * 30;  // 30 days.

        parent::__construct($opts);
        $lifetime = \cge_param::get_int($opts,'lifetime');
        if( $lifetime > 0 ) $this->set_lifetime($lifetime);
    }

    public function set_lifetime($hours)
    {
        $hours = max(1,min(1000,(int)$hours));
        $this->_lifetime = $hours;
    }

    public function get_url()
    {
        $url = parent::get_url();
        $sig = md5(__FILE__.$url);
        $config = \cms_config::get_instance();
        $base_path = TMP_CACHE_LOCATION.'/';
        if( defined(PUBLIC_CACHE_LOCATION) ) $base_path = PUBLIC_CACHE_LOCATION.'/';
        if( !startswith($base_path,$config['root_path']) ) {
            $base_path = $config['uploads_path'].'/_cgsm/';
        }
        $base_path .= $sig;
        $base_url = str_replace($config['root_path'],$config['root_url'],$base_path);
        $dest = $base_path;

        // fix up the url
        $dir = dirname($base_path);
        if( !is_dir($dir) ) @mkdir($dir);
        $crf = new \cge_cached_remote_file($url,$this->_lifetime * 60,$dest);
        if( !$crf->size() ) throw new \RuntimeException('Problem retrieving google map to store statically');

        return $base_url;
    }

    public function get_file()
    {
        $url = parent::get_url();
        $crf = new \cge_cached_remote_file($url,$this->_lifetime * 60);
        return $crf->get_dest();
    }

    public function get_size()
    {
        $url = parent::get_url();
        $crf = new \cge_cached_remote_file($url,$this->_lifetime * 60);
        return $crf->size();
    }

    public function get_contents()
    {
        $url = parent::get_url();
        $crf = new \cge_cached_remote_file($url,$this->_lifetime * 60);
        if( !$crf->size() ) throw new \RuntimeException('Problem retrieving google map to store statically');

        return $crf->file_get_contents();
    }
}
