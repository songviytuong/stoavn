<?php

namespace CGStaticMaps;

class better_url extends \cms_url
{
    public function __toString()
    {
        $url = null;
        $scheme = $this->get_scheme();
        $user = $this->get_user();
        $pass = $this->get_pass();
        $host = $this->get_host();
        $port = $this->get_port();
        $path = $this->get_path();
        $query = $this->get_query();
        $fragment = $this->get_fragment();
        if( $scheme ) $url .= $scheme.'://';
        if( $user ) {
            $url .= $user;
            if( $pass ) $url .= ':' . $pass;
            $url .= '@';
        }
        $url .= $host;
        if( $port && $port != '0' ) $url .= ':'.$port;
        if( $path ) $url .= $path;
        if( $query ) $url .= '?'.urlencode($query);
        if( $fragment ) $url .= '#'.$fragment;
        return $url;
    }
}