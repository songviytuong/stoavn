<?php
if( !isset($gCms) ) exit;

$noembed = cge_utils::to_bool($config['cgsm_noembed']);
$lang = substr(CmsNlsOperations::get_current_language(),0,2);
$tagparms = array('id'=>null,'class'=>null,'alt'=>null,'width'=>null,'height'=>null);
$opts = array('markersize'=>null,'markercolor'=>null,'size'=>null,'scale'=>null,'format'=>null,
              'maptype'=>null,'language'=>$lang,'region'=>null,'center'=>null,'zoom'=>null,'sensor'=>null);
$width = $height = null;
$markers = null;

$markers = array();
$parms = array();
foreach( $params as $key => $val ) {
    switch( $key ) {
    case 'assign':
    case 'action':
    case 'returnid':
    case 'module':
        // ignore these
        break;

    case 'noembed':
        $noembed = \cge_utils::to_bool($val);
        break;

    case 'address':
        // convenience
        $markers[] = $val;
        break;

    default:
        if( in_array($key,array_keys($tagparms)) ) {
            $tagparms[$key] = $val;
        }
        else if( in_array($key,array_keys($opts)) ) {
            $opts[$key] = $val;
        }
        else if( startswith($key,'marker') ) {
            $markers[] = $val;
        }
        else {
            throw new \RuntimeException("$key is not a valid parameter for this action.");
        }
    }
}

// if we have both a height and a width, but no size... we pass that in as an opt
if( $tagparms['width'] && $tagparms['height'] && !$opts['size'] ) {
    $opts['size'] = "{$tagparms['width']}x{$tagparms['height']}";
}
$map = new \CGStaticMaps\saved_static_map($opts);
foreach( $markers as $one ) {
    $map->add_marker($one);
}
$tagparms['src'] = $map->get_url();
if( $map->get_size() < 12000 && !$noembed ) {
    $data = base64_encode($map->get_contents());
    $mimetype = \cge_utils::get_mime_type($map->get_file());
    $tagparms['src'] = 'data:'.$mimetype.';base64,'.$data;
}

if( !$tagparms['alt'] ) {
    if( count($markers) ) {
        $tagparms['alt'] = $markers[0];
    }
    else {
        $tagparms['alt'] = $opts['center'];
    }
}
$out = '<img';
foreach( $tagparms as $key => $val ) {
    $out .= " $key=\"{$val}\"";
}
$out .= '/>';
echo $out;

?>