<?php

final class CGStaticMaps extends CGExtensions
{
    public function InitializeFrontend()
    {
        $this->RegisterModulePlugin();
        $this->SetParameterType('id',CLEAN_STRING);
        $this->SetParameterType('address',CLEAN_STRING);
        $this->SetParameterType('class',CLEAN_STRING);
        $this->SetParameterType('alt',CLEAN_STRING);
        $this->SetParameterType('width',CLEAN_INT);
        $this->SetParameterType('height',CLEAN_INT);
        $this->SetParameterType('markersize',CLEAN_STRING);
        $this->SetParameterType('markercolor',CLEAN_STRING);
        $this->SetParameterType('size',CLEAN_STRING);
        $this->SetParameterType('scale',CLEAN_INT);
        $this->SetParameterType('format',CLEAN_STRING);
        $this->SetParameterType('maptype',CLEAN_STRING);
        $this->SetParameterType('language',CLEAN_STRING);
        $this->SetParameterType('region',CLEAN_STRING);
        $this->SetParameterType('center',CLEAN_STRING);
        $this->SetParameterType('zoom',CLEAN_INT);
        $this->SetParameterType('sensor',CLEAN_INT);
        $this->SetParameterType('noembed',CLEAN_INT);
        $this->SetParameterType(CLEAN_REGEXP.'/marker.*/',CLEAN_STRING);
    }

    public function MinimumCMSVersion() { return '1.12.1'; }
    public function GetVersion() { return '1.0.2'; }
    public function IsPluginModule() { return TRUE; }
    public function HasAdmin() { return FALSE; }
    public function AllowAutoInstall() { return FALSE; }
    public function AllowAutoUpgrade() { return FALSE; }

    public function GetDependencies()
    {
        return array('CGExtensions'=>'1.53.1','CGSimpleSmarty'=>'1.9');
    }
}
