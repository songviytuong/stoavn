<?php
if( !isset($gCms) ) exit;

// map with single marker
$map = new \CGStaticMaps\map_generator(array('size'=>'200x200'));
$map->add_marker('City Hall, Calgary AB');
$url = $map->get_url();
debug_display($url);
echo '<img src="'.$url.'"/><br/>';

// map with no markers, but a center
$map = new \CGStaticMaps\map_generator(array('center'=>'Calgary, AB','size'=>'200x200'));
$url = $map->get_url();
debug_display($url);
echo '<img src="'.$url.'"/><br/>';

// map with no markers, no center, but a 'visible'
$map = new \CGStaticMaps\map_generator(array('visible'=>'Fernie, BC','size'=>'200x200'));
$url = $map->get_url();
debug_display($url);
echo '<img src="'.$url.'"/><br/>';

// map with a center and a visible
$map = new \CGStaticMaps\map_generator(array('visible'=>'Fernie, BC','center'=>'Calgary, AB','size'=>'200x200'));
$url = $map->get_url();
debug_display($url);
echo '<img src="'.$url.'"/><br/>';

// map with multiple markers, and no center
$map = new \CGStaticMaps\map_generator(array('size'=>'200x200'));
$map->add_marker('Calgary, AB');
$map->add_marker('Fernie, BC');
$map->add_marker('Cranbrook, BC');
$url = $map->get_url();
debug_display($url);
echo '<img src="'.$url.'"/><br/>';

// map with multiple markers and a center
$map = new \CGStaticMaps\map_generator(array('size'=>'200x200','center'=>'Sparwood, BC'));
$map->add_marker('Calgary, AB');
$map->add_marker('Fernie, BC');
$map->add_marker('Cranbrook, BC');
$url = $map->get_url();
debug_display($url);
echo '<img src="'.$url.'"/><br/>';

// map with multiple markers, and a visible
$map = new \CGStaticMaps\map_generator(array('size'=>'200x200','visible'=>'Lethbridge AB'));
$map->add_marker('Fernie, BC');
$map->add_marker('Cranbrook, BC');
$url = $map->get_url();
debug_display($url);
echo '<img src="'.$url.'"/><br/>';

// simple map around paris, in french
$map = new \CGStaticMaps\map_generator(array('size'=>'200x200','language'=>'fr','center'=>'paris, fr'));
$url = $map->get_url();
debug_display($url);
echo '<img src="'.$url.'"/><br/>';

// simple map around ghent, in dutch
$map = new \CGStaticMaps\map_generator(array('size'=>'200x200','language'=>'nl','center'=>'brussels, be'));
$url = $map->get_url();
debug_display($url);
echo '<img src="'.$url.'"/><br/>';

// simple map, with one small green marker

$map = new \CGStaticMaps\map_generator(array('size'=>'200x200'));
$map->set_markercolor('#00ff00');
$map->set_markersize('mid');
$map->add_marker('Vancouver BC');
$url = $map->get_url();
debug_display($url);
echo '<img src="'.$url.'"/><br/>';
