<?php
$lang['about_title'] = 'Om %s modulet';
$lang['admin_title'] = 'Modul håndtering adminsitration';
$lang['abouttxt'] = 'Om';
$lang['accessdenied'] = 'Adgang nægtet. Kontrollér venligst tilladelser.';
$lang['action_activated'] = 'Modulet %s er blevet aktiveret.';
$lang['action_installed'] = 'Modulet %s er blevet installeret med følgende besked(er): %s';
$lang['action_upgraded'] = 'Modulet %s er blevet opgraderet';
$lang['action'] = 'Handling';
$lang['active'] = 'Aktivt';
$lang['admindescription'] = 'Et værktøj til at hente og installere moduler direkte fra andre servere';
$lang['advancedsearch_help'] = 'Angiv de ord, som skal være indeholdt eller ikke være indeholdt i søgningen ved at anvende a + eller -. Brug gåseøjne omkring eksakte udtryk f.eks:  +red -apple +"noget tekst"';
$lang['allowuninstall'] = 'Tillad at ModulHånterings-modulet afinstalleres? Vær forsigtig, for dette kan ikke fortrydes.';
$lang['all_modules_up_to_date'] = 'Der er ingen tilgængelige moduler, som er nyere, i modulbiblioteket';
$lang['availablemodules'] = 'Status på modulerne i modul-biblioteket';
$lang['available_updates'] = 'Moduler som er tilgængelige for opdatering. Inden opgradering bedes du venligst læse udgivelsesoplysningerne på Forge og tage backup af hjemmesiden.';
$lang['availmodules'] = 'Tilgængelige moduler';
$lang['back'] = 'Tilbage';
$lang['back_to_module_manager'] = '« Tilbage til Modulhåndtering';
$lang['cancel'] = 'Fortryd';
$lang['cantdownload'] = 'Kan ikke downloade';
$lang['cantremove'] = 'Kan ikke fjerne';
$lang['cantuninstall'] = 'Kan ikke afsinstallere';
$lang['changeperms'] = 'Skift rettigheder';
$lang['confirm_chmod'] = 'Hvis du fortsætter vil modulfilernes rettigheder bliver ændret. Er du sikker på vi skal fortsætte?';
$lang['confirm_resetcache'] = 'Er du sikker på du vil slette den lokale cache?';
$lang['compatibility_disclaimer'] = 'De moduler som vises her, har både  CMS-udviklere og uafhængige udviklere bidraget med  Vi stiller ingen garanti for, at de moduler som er gjort tilgængelige her fungerer, er blevet testet eller er kompatible med dit system.  Du opfordres til at læse de informationer, som findes ved at klikke på hjælp og om linksene for hvert modul, før du forsøger at installere noget.';
$lang['dependstxt'] = 'Modulet er afhængigt af';
$lang['depend_activate'] = 'Modulet %s vil blive aktiveret.';
$lang['depend_install'] = 'Modulet %s (version %s) vil blive installeret.';
$lang['depend_upgrade'] = 'Modulet %s vil blive opgraderet til version %s.';
$lang['depends_upon'] = 'Afhængigt af';
$lang['display_in_english'] = 'Vi på engelsk';
$lang['display_in_mylanguage'] = 'Vis på dansk';
$lang['download'] = 'Hent og installér';
$lang['downloads'] = 'Downloads';
$lang['entersearchterm'] = 'Skriv søgestreng';
$lang['error'] = 'Fejl!';
$lang['error_checksum'] = 'Tjeksum fejl. Dette skyldes formentlig en fejlbehæftet fil, enten forårsaget af fejl ved indsættelse i modul-biblioteket eller ved overførsel til din maskine.';
$lang['error_connectnomodules'] = 'En forbindelse blev oprettet til modul-biblioteket, men der lader ikke til at der er indlagt nogle moduler endnu';
$lang['error_downloadxml'] = 'Der opstod et problem under download af XML-filen: %s';
$lang['error_internal'] = 'Intern fejl... Giv venligst systemets administrator besked';
$lang['error_minimumrepository'] = 'Module-serverens version er ikke kompatibel med denne Module Manager';
$lang['error_moduleinstallfailed'] = 'Installation af modulet mislykkedes';
$lang['error_module_object'] = 'Fejl: kunne ikke få en instans af det %s modul';
$lang['error_nofilename'] = 'Intet fil-navn parameter angivet';
$lang['error_nofilesize'] = 'Intet fil-størrelse parameter angivet';
$lang['error_nomatchingmodules'] = 'Fejl: kunne ikke finde nogen moduler i modulbiblioteket, som matchede';
$lang['error_nomodules'] = 'Fejl: kunne ikke få fat i listen over installerede moduler';
$lang['error_norepositoryurl'] = 'URL\'en til modulebiblioteket ikke angivet';
$lang['error_noresults'] = 'Vi regner med, at nogle resultater er tilgængelige via operationer, som er sat i kø, men der blev ikke fundet nogen. Prøv venligst at reproducere hændelsen og stil tilstrækkelige informationer til rådighed for support-personalet, så problemet kan blive diagnosticeret.';
$lang['error_permissions'] = '<strong><em>ADVARSEL:</em></strong> Du har ikke tilstrækkelig mappe-rettigheder til at installere moduler. En anden mulighed er at der er problemer pga. PHP\'s safemode. Kontrollér at safemode er slået fra og at du har skriverettigheder til modules-mappen.';
$lang['error_request_problem'] = 'Der opstod et problem med kommunikationen med modul-serveren';
$lang['error_search'] = 'Søgefejl';
$lang['error_searchterm'] = 'Angiv venligst nogle gyldige søgekriterier';
$lang['error_skipping'] = 'Dropper installering/opgradering af %s grundet fejl i opsætning af andre nødvendige moduler. Se venligst beskeden ovenfor og prøv igen.';
$lang['error_unsatisfiable_dependency'] = 'Kan ikke finde det krævede modul "%s" (version %s eller nyere) i modulbiblioteket. Modulet kræves direkte af %s; dette kunne tyde på, at der er et problem med den version af dette modul i modulebiblioteket. Kontakt venligst modulets forfatter. Afbryder.';
$lang['error_upgrade'] = 'Opgradering af modulet %s slog fejl!';
$lang['friendlyname'] = 'Modul Håndtering';
$lang['general_notice'] = 'De versioner som vises her udgør de nyeste af de XML-filer, som er blevet uploadet til det valgte modulbibliotek  (normalt CMS %s).  De udgør måske og måske ikke de nyeste af de tilgængelige versioner.';
$lang['help'] = '<h3>What Does This Do?</h3>
<p>A client for the ModuleRepository, this module allows previewing, and installing modules from remote sites without the need for ftping, or unzipping archives.  Module XML files are downloaded using SOAP, integrity verified, and then expanded automatically.</p>
<h3>How Do I Use It</h3>
<p>In order to use this module, you will need the \'Modify Modules\' permission, and you will also need the complete, and full URL to a \'Module Repository\' installation.  You can specify this url in the \'Site Admin\' --> \'Global Settings\' page.</p><br/>
<p>You can find the interface for this module under the \'Extensions\' menu.  When you select this module, the \'Module Repository\' installation will automatically be queried for a list of it\'s available xml modules.  This list will be cross referenced with the list of currently installed modules, and a summary page displayed.  From here, you can view the descriptive information, the help, and the about information for a module without physically installing it.  You can also choose to upgrade or install modules.</p>
<h3>Support</h3>
<p>As per the GPL, this software is provided as-is. Please read the text of the license for the full disclaimer.</p>
<h3>Copyright and License</h3>
<p>Copyright © 2006, calguy1000 <a href="mailto:calguy1000@hotmail.com"><calguy1000@hotmail.com></a>. All Rights Are Reserved.</p>
<p>This module has been released under the <a href="http://www.gnu.org/licenses/licenses.html#GPL">GNU Public License</a>. You must agree to this license before using the module.</p>';
$lang['helptxt'] = 'Hjælp';
$lang['incompatible'] = 'Ikke kompatibel';
$lang['install'] = 'Installér';
$lang['installed'] = 'Installeret';
$lang['install_module'] = 'Installér modul';
$lang['install_procede'] = 'Fortsæt';
$lang['install_submit'] = 'Installér';
$lang['install_with_deps'] = 'Evaluér alle de moduler, som modulet er afhængigt af og intallér';
$lang['instcount'] = 'Moduler der allerede er installeret';
$lang['latestdepends'] = 'Installér altid de nyeste moduler';
$lang['minversion'] = 'Minimum version';
$lang['missingdeps'] = 'Manglende afhængigheder';
$lang['mod_name_ver'] = '%s version %s';
$lang['moddescription'] = 'En klient til ModulBiblioteket, som giver adgang til på forhånd at læse om, og eventuelt installeret moduler direkte fra et andet site uden der er nødvendigt at benytte FTP, unzip osv. XML-filer for modulerne hentes vi SOAP, deres integritet bekræftes og derefter udpakkes og installeres de automatisk.';
$lang['msg_cachecleared'] = 'Cache nulstillet';
$lang['msg_cancelled'] = 'Handlingen blev afbrudt';
$lang['msg_nodependencies'] = 'Filen har ikke oplistet nogen moduler, som det er afhængigt af';
$lang['nametext'] = 'Modul navn';
$lang['newerversion'] = 'Nyere version installeret';
$lang['notice'] = 'Bemærk';
$lang['onlynewesttext'] = 'Vis kun den nyeste version';
$lang['operation_results'] = 'Driftsresultater';
$lang['postinstall'] = 'Modul Håndtering blev korrekt installeret';
$lang['postuninstall'] = 'Modul Håndtering er blevet afinstalleret. Brugere kan ikke længere installere moduler fra en ekstern server. Lokal installation er dog stadig mulig.';
$lang['preferences'] = 'Indstillinger';
$lang['preferencessaved'] = 'Præferencerne blev gemt';
$lang['prompt_advancedsearch'] = 'Avanceret søgning';
$lang['prompt_disable_caching'] = 'Deaktivér caching af forespørgsler fra serveren';
$lang['prompt_dl_chunksize'] = 'Download klump størrelse (Kb)';
$lang['prompt_otheroptions'] = 'Andre valgmuligheder';
$lang['prompt_repository_url'] = 'ModuleBibliotek URL:';
$lang['prompt_settings'] = 'Indstillinger';
$lang['really_uninstall'] = 'Er du sikker på du vil afinstallere dette modul?';
$lang['repositorycount'] = 'Moduler fundet i modul-biblioteket';
$lang['reset'] = 'Nulstil';
$lang['search'] = 'Søg';
$lang['searchterm'] = 'Sørgeterm';
$lang['search_input'] = 'Søgeord';
$lang['search_noresults'] = 'Søgningen lykkedes, men ingen resultater svarede til søgekriterierne';
$lang['search_results'] = 'Søgeresultater';
$lang['sizetext'] = 'Størrelse (bytes)';
$lang['statustext'] = 'Status/Handlinger';
$lang['submit'] = 'Acceptér';
$lang['time_warning'] = 'To eller flere handlinger skal udføres. Vær opmærksom på, at installationen kan tage nogle få minutter. Hav venligst tålmodighed.';
$lang['title_installation_complete'] = 'Installationsprocessen er færdig!';
$lang['uninstalled'] = 'Modulet blev afinstalleret';
$lang['unknown'] = 'Ukendt';
$lang['upgrade'] = 'Opgradér';
$lang['upgraded'] = 'Modulet blev opgraderet til version %s.';
$lang['upgrade_available'] = 'Der findes en nyere version (%s), du har (%s)';
$lang['uptodate'] = 'Installeret';
$lang['use_at_your_own_risk'] = 'Bruges på egen risiko';
$lang['versionsformodule'] = 'Tilgængelige versioner af modulet %s';
$lang['xmltext'] = 'XML-fil';
$lang['yourversion'] = 'Din version';
?>