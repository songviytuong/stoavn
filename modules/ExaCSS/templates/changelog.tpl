<ul>
    <li>
        <strong>Version 2.0.2</strong>
        <ul>
            <li>
                Minor bug fix
            </li>
            <li>
                Fix #10769
            </li>
        </ul>
    </li>
    <li>
        Version 2.0.1
        <ul>
            <li>
                Missing translations
            </li>
            <li>
                Fixed a bug when creating an item and saving it by using the
                "Apply" button
            </li>
            <li>
                Removing the "Change order" button if there is only one item
            </li>
            <li>
                Compatibility with ExaExternalizer
            </li>
        </ul>
    </li>
    <li>
        Version 2.0
        <ul>
            <li>
                Support CMSMS 2.0.1+
            </li>
            <li>
                Replace lessphp (leafo) with Less.php (oyejorge)
            </li>
            <li>
                Remove CSS Crush support
            </li>
            <li>
                Update Less.php (oyejorge) to version 1.7.0.9
            </li>
            <li>
                Update de scssphp (leafo) to version 0.5.1
            </li>
        </ul>
    </li>
</ul>