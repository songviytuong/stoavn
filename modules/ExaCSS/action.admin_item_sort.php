<?php
if( !defined('CMS_VERSION') ) exit;
if( !$this->CheckPermission(ExaCSS::PERM_USE) ) return;









/* ************************************************************************** *\
   Si l'ordre des feuilles de style n'est pas fourni
\* ************************************************************************** */

if (empty($params['item_sort'])):
    $this->RedirectToAdminTab('dashboard', '', 'defaultadmin');
endif;









/* ************************************************************************** *\
   Tri des items
\* ************************************************************************** */

$item_sort = str_replace("item_", "", $params['item_sort']);
$item_sort = explode("|", $item_sort);

foreach ($item_sort as $key => $val):

	if (is_numeric($key) AND is_numeric($val)):
    
		$result = \ExaCSS\Item::sort($val, $key);
        
		if ($result->result == false):
			$this->SetError($result->message);
           $this->RedirectToAdminTab('dashboard', '', 'defaultadmin');
		endif;
	
    endif;

endforeach;

$this->SetMessage($result->message);
$this->RedirectToAdminTab('dashboard', '', 'defaultadmin');









?>