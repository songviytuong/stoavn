<?php
if( !defined('CMS_VERSION') ) exit;









/* ************************************************************************** *\
    Traitement du préprocesseur
\* ************************************************************************** */

if (\ExaCSS\Cache::isCached() == false):
    
    $result = \ExaCSS\Preprocessor::execute();
    
    if (!$result):
        throw new Exception($this->Lang('preprocessor_error'));
    elseif ($result->result == false):
        throw new Exception($result->message);
    endif;
    
    
endif;









/* ************************************************************************** *\
    Affichage de la balise stylesheet
\* ************************************************************************** */

$exacss_url = \ExaCSS\Cache::getUrl();

echo '<link rel="stylesheet" type="text/css" href="' . $exacss_url . '">';









?>