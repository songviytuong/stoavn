<?php
if( !defined('CMS_VERSION') ) exit;









/* ************************************************************************** *\
   Permissions
\* ************************************************************************** */

$this->CreatePermission(ExaCSS::PERM_USE,'ExaCSS - Use');









/* ************************************************************************** *\
   Table Preprocessor
\* ************************************************************************** */

$table['module_exacss_preprocessor'] = "
    id I KEY AUTO,
    name C(255),
    alias C(255),
    url C(255),
    sort I,
    status L
    ";









/* ************************************************************************** *\
   Table Item
\* ************************************************************************** */
    
$table['module_exacss_item'] = "
    id I KEY AUTO,
    name C(255),
    content X2,
    sort I,
    date_creation " . CMS_ADODB_DT . ",
    date_modification " . CMS_ADODB_DT . ",
    status L
    ";
    
    
    






/* ************************************************************************** *\
   Création des tables
\* ************************************************************************** */

$db = $this->GetDb();
$dict = NewDataDictionary($db);

foreach ($table as $key => $val):
    $sqlarray = $dict->CreateTableSQL(CMS_DB_PREFIX.$key,$val,['mysql' => 'TYPE=MyISAM']);
    $dict->ExecuteSQLArray($sqlarray);
endforeach;









/* ************************************************************************** *\
   Table Preprocessor - Insertion
\* ************************************************************************** */

$query = "
	INSERT INTO " . CMS_DB_PREFIX . "module_exacss_preprocessor
	SET
		name = ?,
		alias = ?,
		url = ?,
		sort = ?,
		status = ?
	" ;
	
$values = array('None', '', '', 0, 1) ;
$result = $db->Execute($query, $values);

$values = array('Less.php (oyejorge)', 'lessphp_oyejorge', 'http://lessphp.gpeasy.com/', 1, 1) ;
$result = $db->Execute($query, $values);

$values = array('scssphp (Leafo)', 'scssphp_leafo', 'http://leafo.github.io/scssphp/', 1, 1) ;
$result = $db->Execute($query, $values);









?>