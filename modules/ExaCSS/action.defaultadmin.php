<?php
if( !defined('CMS_VERSION') ) exit;
if( !$this->CheckPermission(ExaCSS::PERM_USE) ) return;









/* ************************************************************************** *\
   Paramètres - Enregistrement du préprocesseur
\* ************************************************************************** */

if (isset($params['submit_preprocessor'])):
	
    $this->SetPreference('preprocessor', $params['preprocessor']);

    $this->SetMessage($this->lang('preprocessor_changed'));
    $this->RedirectToAdminTab('setting');
    
endif;









/* ************************************************************************** *\
   Paramètres - Importation d'un fichier
\* ************************************************************************** */

if (isset($params['submit_import'])):

    if (!isset($_FILES[$id."file"])):
        $this->SetError("No file!");
        $this->RedirectToAdminTab('setting', '', 'defaultadmin');
    endif;
    
    $result = \ExaCSS\Item::import($_FILES[$id."file"]);
    
    if ($result->result == false):
        $this->SetError($result->message);
        $this->RedirectToAdminTab('setting', '', 'defaultadmin');
    else:
        $this->SetMessage($result->message);
        $this->RedirectToAdminTab('setting', '', 'defaultadmin');
    endif;
    
endif;









/* ************************************************************************** *\
   Paramètres - Compatibilité avec ExaExternalizer
\* ************************************************************************** */

if (isset($params['submit_exaexternalizer'])):
	
    $exaexternalizer_compatibility = $this->GetPreference('exaexternalizer_compatibility', false);

    if ($exaexternalizer_compatibility == false):
        $this->AddEventHandler('ExaExternalizer', 'Export');
        $this->AddEventHandler('ExaExternalizer', 'Import');
        $this->SetPreference('exaexternalizer_compatibility', true);
    else:
        $this->RemoveEventHandler('ExaExternalizer', 'Export');
        $this->RemoveEventHandler('ExaExternalizer', 'Import');
        $this->SetPreference('exaexternalizer_compatibility', false);
    endif;
    
    $this->SetMessage($this->lang('setting_saved'));
    $this->RedirectToAdminTab('setting');
    
endif;









/* ************************************************************************** *\
   Récupération des items
\* ************************************************************************** */

$item_list = [];

$result = \ExaCSS\Item::get([
    'order' => "sort ASC, name ASC"
    ]);
    
if ($result->result AND $result->count > 0):
    
    $item_list = $result->record;
    $smarty->assign('item_list', $item_list);
    
endif;









/* ************************************************************************** *\
   Récupération des préprocesseurs
\* ************************************************************************** */

$preprocessor_list = [];

$result = \ExaCSS\Preprocessor::get([
    'order' => "sort ASC, name ASC"
    ]);
    
if ($result->result AND $result->count > 0):
    
    $preprocessor_list = $result->record;
    $smarty->assign('preprocessor_list', $preprocessor_list);
    
endif;









/* ************************************************************************** *\
   Paramètres - Récupération
\* ************************************************************************** */

$setting = [];
$setting['preprocessor'] = $this->GetPreference('preprocessor', '');
$setting['exaexternalizer_available'] = \cms_utils::module_available('ExaExternalizer');
$setting['exaexternalizer_compatibility'] = $this->GetPreference('exaexternalizer_compatibility', false);
$smarty->assign('setting', $setting);









/* ************************************************************************** *\
   Affichage du template
\* ************************************************************************** */

$tpl = $smarty->CreateTemplate($this->GetTemplateResource('defaultadmin.tpl'),null,null,$smarty);
$tpl->display();









?>