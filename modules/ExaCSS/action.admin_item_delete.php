<?php
if( !defined('CMS_VERSION') ) exit;
if( !$this->CheckPermission(ExaCSS::PERM_USE) ) return;









/* ************************************************************************** *\
   Suppression de l'item
\* ************************************************************************** */

$item_id = (empty($params['item_id'])) ? '' : $params['item_id'];

$result = \ExaCSS\Item::delete($item_id);

if ($result->result == false):
    $this->SetError($result->message);
    $this->RedirectToAdminTab('dashboard', '', 'defaultadmin');  
else:
    $this->SetMessage($result->message);
    $this->RedirectToAdminTab('dashboard', '', 'defaultadmin');
endif;









?>