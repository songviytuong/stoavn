<?php
$lang['friendlyname'] = 'CMS Mailer';
$lang['moddescription'] = 'Dette er en enkel wrapper rundt PHPMailer. Den har en tilsvarende API og et enkelt grensesnitt for noen standarder. Denne modulen er utgått siden CMSMS 2.0';
$lang['postinstall'] = 'CMSMailer-modulen har blitt installert';
$lang['postuninstall'] = 'CMSMailer-modulen er avinstallert';
?>