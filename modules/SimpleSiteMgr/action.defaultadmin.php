<?php
#-------------------------------------------------------------------------
# Module: SimpleSiteMgr
# Author: Noel McGran, Rolf Tjassens
#-------------------------------------------------------------------------
# CMS Made Simple is (c) 2004 - 2011 by Ted Kulp (wishy@cmsmadesimple.org)
# CMS Made Simple is (c) 2011 - 2016 by The CMSMS Dev Team
# This project's homepage is: http://www.cmsmadesimple.org
# The module's homepage is: http://dev.cmsmadesimple.org/projects/simplesitemgr
#-------------------------------------------------------------------------
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
# Or read it online: http://www.gnu.org/licenses/licenses.html#GPL
#-------------------------------------------------------------------------

if ( !cmsms() ) exit;

if ( !$this->VisibleToAdminUser() ) return;

// display status messages passed here by other pages as message parameter
if (isset($params['message']))
	echo $this->ShowMessage($params['message']);
	
if (isset($params['errors']) && count($params['errors']))
	echo $this->ShowErrors($params['errors']);

// choose the tab to display. If no tab is set, select 'links' as the default
if (!empty($params['active_tab'])) {
	$tab = $params['active_tab'];
} else {
	$tab = 'listsites';
}	

// and finally, display all the tabs. 
echo $this->StartTabHeaders();
	echo $this->SetTabHeader('listsites', $this->Lang('listsites'), 'listsites' == $tab ? true : false);
	echo $this->SetTabHeader('manage', $this->Lang('manage'), 'manage' == $tab ? true : false);
echo $this->EndTabHeaders();

echo $this->StartTabContent();
	echo $this->StartTab('listsites');
		include 'function.listsites.php';
	echo $this->EndTab();
	echo $this->StartTab('manage');
		include 'function.manage.php';
	echo $this->EndTab();
echo $this->EndTabContent();

?>