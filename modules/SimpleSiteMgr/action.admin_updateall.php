<?php
#-------------------------------------------------------------------------
# Module: SimpleSiteMgr
# Author: Noel McGran, Rolf Tjassens
#-------------------------------------------------------------------------
# CMS Made Simple is (c) 2004 - 2011 by Ted Kulp (wishy@cmsmadesimple.org)
# CMS Made Simple is (c) 2011 - 2016 by The CMSMS Dev Team
# This project's homepage is: http://www.cmsmadesimple.org
# The module's homepage is: http://dev.cmsmadesimple.org/projects/simplesitemgr
#-------------------------------------------------------------------------
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
# Or read it online: http://www.gnu.org/licenses/licenses.html#GPL
#-------------------------------------------------------------------------

if ( !cmsms() ) exit;

if (isset($params['cancel'])) 
	$this->Redirect($id, 'defaultadmin', $returnid, array('active_tab' => 'listsites'));

$site_id = (isset($params['site_id']) ? $params['site_id'] : '');
$errors = array();
$items = array();
$fileparts = array();
$site_modules = array();
$core_modules = $this->core_module_list();
$message = '';
		
$db =& $this->GetDb();

if (isset($params['update'])) {

	$query = 'SELECT * FROM ' . cms_db_prefix() . 'module_simplesitemgr_sites ORDER BY site_name ASC';
	$dbresult =& $db->Execute($query);

	while($dbresult && $row = $dbresult->FetchRow()){
		$i = count($items);
		$items[$i]['site_id']   	= $row['site_id'];
		$items[$i]['site_name']   	= $row['site_name'];
		$items[$i]['site_url']    	= $row['site_url'];
		$items[$i]['site_filepwd'] 	= $row['site_filepwd'];
		
		$local_key = substr( $items[$i]['site_filepwd'], 20 );
		$items[$i]['full_file'] 	= $row['site_url'] . "/tmp/templates_c/SimpleSiteInfo^".md5($local_key).".dat";
		
		@touch( $items[$i]['site_url'] . '/index.php?mact=SimpleSiteInfo,cntnt01,update_info_file,0&cntnt01key=' . $local_key );
	}

	for($j = 0; $j < $i+1; ++$j){
		$filetemp  = @file_get_contents($items[$j]['full_file']);
		
		if ($filetemp === false) {
		
			$message .= $items[$j]['site_name'] . ' - <span style="color:red">' . $this->Lang('no_site_file') . '</span><br />';
			
			$query = 'UPDATE '.cms_db_prefix().'module_simplesitemgr_sites 
				SET
					site_maintenance_mode=?
				WHERE
					site_id=' . $items[$j]['site_id'];

			$result = $db->Execute($query, array('1'));
			
		} else {
		
			$site_filepwd = $items[$j]['site_filepwd'];
			$filedata = $this->NB_Decryption($filetemp,$site_filepwd);
			$fileparts = explode('*', $filedata);
			if($fileparts[0] != 'CMSMS') {
	 			$message .= $items[$j]['site_name'] . ' - <span style="color:red;">' . $this->Lang('wrong_password') . '</span><br />';
			} else {
				$site_id = $items[$j]['site_id'];
				$filetime = date("F d Y H:i:s", $this->GetRemoteFileDate($items[$j]['full_file']));
				$time = date("F d Y H:i:s", time());
				$query = 'UPDATE '.cms_db_prefix().'module_simplesitemgr_sites 
					SET
						site_filedate=?,
						site_fileupdate=?,
						site_cms_version=?,
						site_admin_url=?,
						site_php_version=?,
						site_module_list=?,
						site_ssi_version=?,
						site_config_writable=?,
						site_installer_present=?,
						site_maintenance_mode=?
					WHERE
						site_id=' . $site_id;
				$result = $db->Execute($query, array($filetime, $time, $fileparts[2], $fileparts[1], $fileparts[3], $fileparts[4], $fileparts[5], $fileparts[6], $fileparts[7], $fileparts[8]));
				if(!$result) {
				
					$message .= $items[$j]['site_name'] . ' - <span style="color:red;">SQL ERROR: ' . $db->ErrorMsg() . '(with ' . $db->sql . ')</span><br />';
					
				} else {
				
					$site_modules = $this->GetArrays('|', ',', $fileparts[4]);
					$query = 'DELETE FROM ' . cms_db_prefix() . 'module_simplesitemgr_site_modules WHERE site_id=?';
					$result = $db->Execute($query, array($site_id));				
				
					foreach($site_modules as $module=>$version) {
						if (in_array($module, $core_modules)) {	
							$core = "1"; 
						} else {
							$core = "0";
						}
						$query = 'INSERT INTO '.cms_db_prefix().'module_simplesitemgr_site_modules (
							site_id,
							core,
							module_name,
							module_version)
						VALUES(?,?,?,?)';
						$result = $db->Execute($query, array($site_id, $core, $module, $version));
						if(!$result) { 
							$errors[] = 'SQL ERROR: ' . $db->ErrorMsg() . '(with ' . $db->sql . ')';	
						} 
					}				
					$message .= $items[$j]['site_name'] . ' - ' . $this->Lang('siteupdated') . '<br />';
				}				
			}
		}
	}

	$this->Redirect($id, 'defaultadmin', $returnid, array('active_tab' => 'listsites', 'message' => $message));

} else {

	$query = 'SELECT * FROM ' . cms_db_prefix() . 'module_simplesitemgr_sites ORDER BY site_name ASC';

	$dbresult =& $db->Execute($query);

	$items = Array();

	while($dbresult && $row = $dbresult->FetchRow()){
		$i = count($items);
		$items[$i]['site_name']   	= $row['site_name'];
		$items[$i]['site_url']    	= $row['site_url'];
		$items[$i]['file_url']		= $row['site_url'] . '/tmp/siteinfo.txt';
		$items[$i]['site_filepwd'] 	= $row['site_filepwd'];
		$items[$i]['site_php_version'] 	= $row['site_php_version'];
	}
}

if(isset($id))
	$smarty->assign('idfield', $this->CreateInputHidden($id, 'id', $id));

$smarty->assign('startform', $this->CreateFormStart($id, 'admin_updateall', $returnid));
$smarty->assign('endform', $this->CreateFormEnd());

$smarty->assign('items', $items);

$smarty->assign('label_sites_to_be_updated', $this->lang('sitestobeupdated'));
$smarty->assign('prompt_site_name', $this->lang('prompt_site_name'));
$smarty->assign('prompt_site_url', $this->lang('prompt_site_url'));
$smarty->assign('prompt_site_filepwd', $this->Lang('prompt_site_filepwd'));

$smarty->assign('update', $this->CreateInputSubmit($id, 'update', $this->lang('update')));
$smarty->assign('cancel', $this->CreateInputSubmit($id, 'cancel', lang('cancel')));

echo $this->ProcessTemplate('updateall.tpl');

?>