<?php 
#-------------------------------------------------------------------------
# Module: SimpleSiteMgr
# Author: Noel McGran, Rolf Tjassens
#-------------------------------------------------------------------------
# CMS Made Simple is (c) 2004 - 2011 by Ted Kulp (wishy@cmsmadesimple.org)
# CMS Made Simple is (c) 2011 - 2016 by The CMSMS Dev Team
# This project's homepage is: http://www.cmsmadesimple.org
# The module's homepage is: http://dev.cmsmadesimple.org/projects/simplesitemgr
#-------------------------------------------------------------------------
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
# Or read it online: http://www.gnu.org/licenses/licenses.html#GPL
#-------------------------------------------------------------------------

if ( !cmsms() ) exit;

class SimpleSiteMgr extends CMSModule {

	function GetName(){ return 'SimpleSiteMgr'; }
	function GetFriendlyName(){ return $this->Lang('friendlyname'); }
	function GetVersion(){ return '3.1.1'; }
	function GetHelp() { return file_get_contents(dirname(__FILE__).'/help_text.inc'); }
	function GetAuthor(){ return 'Noel McGran, Rolf Tjassens'; }
	function GetAuthorEmail(){ return 'nmcgran@telus.net, rolf at cmsmadesimple dot org'; }
	function GetChangeLog() { return file_get_contents(dirname(__FILE__).'/changelog.inc'); }
	function SetParameters(){ $this->RestrictUnknownParams(); }
	function IsPluginModule(){ return true; }
	function HasAdmin(){ return true; }
	function GetAdminSection(){ return 'main'; }
	function GetAdminDescription(){ return $this->Lang('moddescription'); }
	function VisibleToAdminUser(){ return ($this->CheckPermission('Modify Site Preferences') || $this->CheckPermission('Modify Modules')); }
	function GetDependencies(){ return array(); }
	function MinimumCMSVersion(){ return "2.0"; }
	function InstallPostMessage(){ return $this->Lang('postinstall'); }
	function UninstallPostMessage(){ return $this->Lang('postuninstall'); }
	
	function core_module_list() { return array('AdminSearch', 'CMSContentManager', 'CMSMailer', 'CMSPrinting', 'DesignManager', 'FileManager', 'MenuManager', 'MicroTiny', 'ModuleManager', 'Navigator', 'News', 'Search', 'ThemeManager'); }
	
	function GetRemoteFileDate($uri)
	{
    	$unixtime = 0;
	    $fp = fopen($uri, "r");
    	if(!$fp) { return; }
    	$MetaData = stream_get_meta_data( $fp );
		
       	foreach ($MetaData['wrapper_data'] as $response)
		{
        	// case: redirection
        	if( substr(strtolower($response), 0, 10) == 'location: ')
			{
            	$newUri = substr($response, 10);
            	fclose( $fp );
            	return $this->GetRemoteFileDate($newUri);
        	}
        	// case: last-modified
        	elseif( substr( strtolower($response), 0, 15 ) == 'last-modified: ' )
			{
            	$unixtime = strtotime(substr($response, 15));
            	break;
        	}
    	}
    	fclose($fp);
    	return $unixtime;
	}
	
	function NB_Decryption($encrypted_data, $key) {
		$TD = mcrypt_module_open('tripledes', '', 'ecb', '');
		$IV = mcrypt_create_iv (mcrypt_enc_get_iv_size($TD), MCRYPT_RAND);
		mcrypt_generic_init($TD, $key, $IV);
		$decrypted_data = mcrypt_ecb (MCRYPT_3DES, $key, $encrypted_data, MCRYPT_DECRYPT,$IV);
		mcrypt_generic_deinit($TD);
		mcrypt_module_close($TD);
		return trim($decrypted_data);
	}
	
	function GetArrays($del1, $del2, $array) {
	
    		$array1 = explode("$del1", $array);
			
    		foreach($array1 as $key=>$value){
        		$array2 = explode("$del2", $value);
        		foreach($array2 as $key2=>$value2){
          			$array3[] = $value2; 
        		}
    		}
			
    		$afinal = array();
			
    		for ($i = 0; $i <= count($array3); $i += 2) {
        		if($array3[$i]!=""){
            		$afinal[trim($array3[$i])] = trim($array3[$i+1]);
        		}
    		}
			
    		return $afinal;
	} 
}

?>