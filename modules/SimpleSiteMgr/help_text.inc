<h3>About</h3>

<p>This module provides a way to monitor the status of external CMSMS sites, showing the currently installed 
version of CMS Made Simple, as well as any installed modules and PHP version.</p>

<h3>Getting Started</h3>

<p>In order to use this module, you need to install the "SimpleSiteInfo" module at each site you wish to monitor. 
Once that is installed, add the remote site to your list of sites to monitor, on the "Manage Sites" tab of this module.</p>

<p><b>NOTE:</b>If you wish to include this site in the list of sites monitored, you must also install the "SimpleSiteInfo" module here.</p>

<h3>Usage</h3>

<p>There is an "Update All" link, which will obtain the site info files from all monitored sites. However, if there are a lot of sites, you may
 experience timeouts if your PHP max_execution_time and/or memory_limit are set too low.</p> 
 
<p>On sites that have out of date modules, the is is highlighted in red.</p>

<p class="warning">Note: The module SimpleSiteMgr can't retrieve the full database of latest module releases from the CMSMS Forge.<br />
Because of that the information in the "Forge Version" column can be empty or wrong... Sadly at this moment I can't do much about that, perhaps later...</p>

<h3>Support</h3>

<p>This module does not contain any commercial support. If you have problems, ask for help in the:</p>
<ul>
	<li><a href="http://forum.cmsmadesimple.org/" target="_blank">CMS Made Simple forums</a>,</li>
	<li><a href="irc://irc.freenode.net/cms" target="_blank"><abbr title="Internet Relay Chat">IRC</abbr> chat</a>,</li>
	<li>or write an email to the author</li>
</ul>

<h3>Copyright</h3>

<p>Copyright &copy; 2008-2016, Noel McGran [nmcgran at telus dot net], Rolf Tjassens [rolf at cmsmadesimple dot org]. All Rights Are Reserved.</p>
<br />
<p>This module has been released under the <a href="http://www.gnu.org/licenses/licenses.html#GPL" target="_blank">GNU Public License v3</a>.<br />
However, as a special exception to the GPL, this software is distributed as an addon module to CMS Made Simple&trade;.<br />
You may only use this software when there is a clear and obvious indication in the admin section that the site was built with <b>CMS Made Simple&trade;</b>.</p>

<br />