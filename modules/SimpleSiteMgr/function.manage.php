<?php
#-------------------------------------------------------------------------
# Module: SimpleSiteMgr
# Author: Noel McGran, Rolf Tjassens
#-------------------------------------------------------------------------
# CMS Made Simple is (c) 2004 - 2011 by Ted Kulp (wishy@cmsmadesimple.org)
# CMS Made Simple is (c) 2011 - 2016 by The CMSMS Dev Team
# This project's homepage is: http://www.cmsmadesimple.org
# The module's homepage is: http://dev.cmsmadesimple.org/projects/simplesitemgr
#-------------------------------------------------------------------------
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
# Or read it online: http://www.gnu.org/licenses/licenses.html#GPL
#-------------------------------------------------------------------------

if ( !cmsms() ) exit;

$query = 'SELECT * FROM ' . cms_db_prefix() . 'module_simplesitemgr_sites ORDER BY site_name ASC';

$dbresult =& $db->Execute($query);

$items = Array();
asort($items);

$admintheme = cms_utils::get_theme_object();

while($dbresult && $row = $dbresult->FetchRow()) {
	$i = count($items);
	$items[$i]['site_name'] = $this->CreateLink($id, 'admin_editsite', $returnid, $row['site_name'], array('site_id' => $row['site_id']));
	$items[$i]['site_url'] = $row['site_url'];
	$items[$i]['site_filedate'] = $row['site_filedate'];
	$items[$i]['site_fileupdate'] = $row['site_fileupdate'];
	
	$items[$i]['editsite'] = $this->CreateLink($id, 'admin_editsite', $returnid,
		$admintheme->DisplayImage('icons/system/edit.gif', $this->Lang('editsite'),'','','systemicon'),
		array('site_id' => $row['site_id'])); 
		
	$items[$i]['delete'] = $this->CreateLink($id, 'admin_deletesite', $returnid,
		$admintheme->DisplayImage('icons/system/delete.gif', $this->Lang('deletesite'),'','','systemicon'),
		array('site_id' => $row['site_id']), $this->Lang('areyousure')); 
}

$smarty->assign('items', $items);
$smarty->assign('header_name', $this->Lang('header_name'));
$smarty->assign('header_url', $this->Lang('header_url'));
$smarty->assign('header_filedate', $this->Lang('header_filedate'));
$smarty->assign('header_fileupdate', $this->Lang('header_fileupdate'));

$smarty->assign('addsite', $this->CreateLink($id, 'admin_addsite', $returnid,
	$admintheme->DisplayImage('icons/system/newobject.gif', $this->Lang('addsite'),'','','systemicon') . ' ' . $this->Lang('addsite')
));

echo $this->ProcessTemplate('manage.tpl');

?>