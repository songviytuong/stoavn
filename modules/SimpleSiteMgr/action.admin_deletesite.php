<?php
#-------------------------------------------------------------------------
# Module: SimpleSiteMgr
# Author: Noel McGran, Rolf Tjassens
#-------------------------------------------------------------------------
# CMS Made Simple is (c) 2004 - 2011 by Ted Kulp (wishy@cmsmadesimple.org)
# CMS Made Simple is (c) 2011 - 2016 by The CMSMS Dev Team
# This project's homepage is: http://www.cmsmadesimple.org
# The module's homepage is: http://dev.cmsmadesimple.org/projects/simplesitemgr
#-------------------------------------------------------------------------
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
# Or read it online: http://www.gnu.org/licenses/licenses.html#GPL
#-------------------------------------------------------------------------

if ( !cmsms() ) exit;

$errors = array();

$db =& $this->GetDb();

$query = 'SELECT 0 FROM ' . cms_db_prefix() . 'module_simplesitemgr_sites WHERE site_id=?';

$result = $db->Execute($query, array($params['site_id']));

if(!$result->FetchRow()) {

	$errors[] = $this->Lang('nosuchid', array($params['site_id']));

} else {

	$query = 'DELETE FROM ' . cms_db_prefix() . 'module_simplesitemgr_sites WHERE site_id=?';
		
	$result = $db->Execute($query, array($params['site_id']));

	if(!$result) {

		$errors[] = 'SQL ERROR: ' . $db->ErrorMsg() . '(with ' . $db->sql . ')';

	} else {

		$message = $this->Lang('sitedeleted');
		$this->Redirect($id, 'defaultadmin', $returnid, array('active_tab' => 'manage', 'message' => $message));

	}

}

$this->Redirect($id, 'defaultadmin', $returnid, array('active_tab' => 'manage', 'errors' => $errors))

?>